package com.funlab.jcchat.main;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.funlab.jcchat.Base.CommonActivity;
import com.funlab.jcchat.R;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;


public class ReadPersonalActivity extends CommonActivity implements OnClickListener{

	private TextView ui_txvContent;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_readpersonal);

		loadLayout();
		setText();
	}

	public void loadLayout() {
		
		TextView txvClose = (TextView) findViewById(R.id.txv_close);
		txvClose.setOnClickListener(this);

		ui_txvContent = (TextView) findViewById(R.id.txv_agreeContent);
	}
	
	public void setText() {
		
		String data = null;
		InputStream inputStream = getResources().openRawResource(R.raw.personanl);
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		 
		int i;
		try {
		    i = inputStream.read();
		    while (i != -1) {
		        byteArrayOutputStream.write(i);
		        i = inputStream.read();
		    }
		     
		    data = new String(byteArrayOutputStream.toByteArray());
		    inputStream.close();
		} catch (IOException e) {
		    e.printStackTrace();
		}
		
		ui_txvContent.setText(data);
	}
	
	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.txv_close) {
			finish();
		}
	}


}
