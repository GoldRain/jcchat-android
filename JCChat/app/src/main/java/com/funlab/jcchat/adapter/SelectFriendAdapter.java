package com.funlab.jcchat.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.funlab.jcchat.JCChatApplication;
import com.funlab.jcchat.R;
import com.funlab.jcchat.Utils.CirculaireNetworkImageView;
import com.funlab.jcchat.Utils.HangulUtils;
import com.funlab.jcchat.model.FriendEntity;

import java.util.ArrayList;

/**
 * Created by HGS on 12/14/2015.
 */
public class SelectFriendAdapter extends BaseAdapter {

    private Context _context = null;
    private ArrayList<FriendEntity> _selectFriDatas = new ArrayList<FriendEntity>();
    private ArrayList<FriendEntity> _selectFriAllDatas = new ArrayList<>();

    private ImageLoader _imageLoader;

    public SelectFriendAdapter(Context _context){

        super();
        this._context = _context;

        _imageLoader = JCChatApplication.getInstance().getImageLoader();
    }

    @Override
    public int getCount(){

        return _selectFriDatas.size();
    }

    @Override
    public Object getItem(int position){

        return _selectFriDatas.get(position);
    }

    @Override
    public long getItemId(int position){

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        SelectFriendHolder selectFriendHolder;

        if (convertView == null) {
            selectFriendHolder = new SelectFriendHolder();

            LayoutInflater inflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.select_friend_list_item, null);

            selectFriendHolder.imvSelectFriPhoto = (CirculaireNetworkImageView)convertView.findViewById(R.id.imv_selectFriPhoto);
            selectFriendHolder.txvSelectFriName = (TextView)convertView.findViewById(R.id.txv_selectFriName);
            selectFriendHolder.imvSelectFriState = (ImageView)convertView.findViewById(R.id.imv_selectFirState);
            selectFriendHolder.imvSelectFriState.setSelected(false);

            convertView.setTag(selectFriendHolder);
        }else {
            selectFriendHolder = (SelectFriendHolder)convertView.getTag();
        }

        final FriendEntity friend = _selectFriDatas.get(position);

        selectFriendHolder.txvSelectFriName.setText(friend.get_name());

        if (friend.get_photoUrl().length() > 0)
            selectFriendHolder.imvSelectFriPhoto.setImageUrl(friend.get_photoUrl(), _imageLoader);
        else
            selectFriendHolder.imvSelectFriPhoto.setImageResource(R.drawable.bg_non_profile1);

        final ImageView imvState = selectFriendHolder.imvSelectFriState;
        imvState.setSelected(friend.is_isSelected());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imvState.setSelected(!imvState.isSelected());
                friend.set_isSelected(imvState.isSelected());
            }
        });

        return convertView;
    }

    public void addItem(FriendEntity entity){
        _selectFriAllDatas.add(entity);
    }

    public void clearData() {
        _selectFriAllDatas.clear();
    }

    public void initSelectFriendDatas(){

        _selectFriDatas.clear();
        _selectFriDatas.addAll(_selectFriAllDatas);
    }

    public void filter(String charText){

        charText = charText.toLowerCase();

        _selectFriDatas.clear();

        if(charText.length() == 0){
            _selectFriDatas.addAll(_selectFriAllDatas);
        }else {

            for (FriendEntity friend : _selectFriAllDatas){

                String value = friend.get_name().toLowerCase();

                if(value.contains(charText)
                        || HangulUtils.isHangulInitialSound(value, charText)){
                    _selectFriDatas.add(friend);
                }
            }
        }
        notifyDataSetChanged();
    }

    public class SelectFriendHolder {

        public CirculaireNetworkImageView imvSelectFriPhoto;
        public TextView txvSelectFriName;
        public ImageView imvSelectFriState;
    }
}
