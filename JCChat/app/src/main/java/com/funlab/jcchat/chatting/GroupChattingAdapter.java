package com.funlab.jcchat.chatting;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.support.v4.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.funlab.jcchat.JCChatApplication;
import com.funlab.jcchat.R;
import com.funlab.jcchat.Utils.BitmapUtils;
import com.funlab.jcchat.Utils.CirculaireNetworkImageView;
import com.funlab.jcchat.Utils.CustomMultiPartEntity;
import com.funlab.jcchat.Utils.CustomNetworkImageView;
import com.funlab.jcchat.commons.Commons;
import com.funlab.jcchat.commons.ReqConst;
import com.funlab.jcchat.logger.Logger;
import com.funlab.jcchat.model.FriendEntity;
import com.funlab.jcchat.model.UserEntity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by HGS on 12/14/2015.
 */
public class GroupChattingAdapter extends BaseAdapter {

    private static final int TYPE_CHAT = 0;
    private static final int TYPE_DATE = 1;

    private ArrayList<Object> _chatList = new ArrayList<Object>();

    private ImageLoader _imageLoader;
    private GroupChattingActivity _context;

    private UserEntity _user = null;

    private LruCache<String, Bitmap> mMemoryCache;

    private boolean _firstNormal = false;

    private Date _lastDate = null;

    public GroupChattingAdapter(Context context){

        super();

        this._context = (GroupChattingActivity) context;
        _user = Commons.g_user;
        _imageLoader = JCChatApplication.getInstance().getImageLoader();


        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

        // Use 1/8th of the available memory for this memory cache.
        final int cacheSize = maxMemory / 8;

        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return bitmap.getByteCount() / 1024;
            }
        };
    }

    public void clearAll() {
        _chatList.clear();
        _lastDate = null;
    }

    @Override
    public int getCount(){

        return _chatList.size();
    }

    @Override
    public Object getItem(int position){

        return _chatList.get(position);
    }

    @Override
    public long getItemId(int position){

        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {

        if (getItem(position) instanceof GroupChatItem)
            return TYPE_CHAT;

        return TYPE_DATE;
    }

    @Override
    public boolean isEnabled(int position) {
        return (getItemViewType(position) == TYPE_CHAT);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){

        int type = getItemViewType(position);

        switch (type) {

            case TYPE_DATE: {

                DateHolder dateHolder;

                if (convertView == null){

                    dateHolder = new GroupChattingAdapter.DateHolder();

                    LayoutInflater inflater = (LayoutInflater)_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.chatting_header, null);

                    dateHolder.txvDate = (TextView)convertView.findViewById(R.id.txv_date);
                    convertView.setTag(dateHolder);

                } else {
                    dateHolder = (DateHolder)convertView.getTag();
                }

                String date = (String) _chatList.get(position);
                dateHolder.txvDate.setText(date);

                return convertView;

            }

            case TYPE_CHAT:
            default: {

                ChattingHolder chattingHolder;

                if (convertView == null){

                    chattingHolder = new GroupChattingAdapter.ChattingHolder();

                    LayoutInflater inflater = (LayoutInflater)_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.chatting_item, null);

                    chattingHolder.imvFriendPhoto = (CirculaireNetworkImageView)convertView.findViewById(R.id.imv_friendPhoto);
                    chattingHolder.txvFriendName = (TextView)convertView.findViewById(R.id.txv_friendName);
                    chattingHolder.txvFriendMessage = (TextView)convertView.findViewById(R.id.txv_message1);
                    chattingHolder.txvTime1 = (TextView) convertView.findViewById(R.id.txv_time1);
                    chattingHolder.imvFriendImage = (CustomNetworkImageView) convertView.findViewById(R.id.imv_imgmsg1);
                    chattingHolder.fltVideoPreview1 = (FrameLayout) convertView.findViewById(R.id.flt_video1);
                    chattingHolder.imvVideoPreview1 = (ImageView) convertView.findViewById(R.id.imv_video1);
                    chattingHolder.imvVideoMarker1 = (ImageView) convertView.findViewById(R.id.imv_videoMark1);
                    chattingHolder.imvVideoProgress1 = (ImageView) convertView.findViewById(R.id.imv_videoprogress1);

                    chattingHolder.txvMyMessage = (TextView) convertView.findViewById(R.id.txv_message2);
                    chattingHolder.txvTime2 = (TextView) convertView.findViewById(R.id.txv_time2);
                    chattingHolder.imvMyImage = (CustomNetworkImageView) convertView.findViewById(R.id.imv_imgmsg2);
                    chattingHolder.imvImageProgress2 = (ImageView) convertView.findViewById(R.id.imv_imgprogress2);
                    chattingHolder.fltVideoPreview2 = (FrameLayout) convertView.findViewById(R.id.flt_video2);
                    chattingHolder.imvVideoPreview2 = (ImageView) convertView.findViewById(R.id.imv_video2);
                    chattingHolder.imvVideoProgress2 = (ImageView) convertView.findViewById(R.id.imv_videoprogress2);
                    chattingHolder.imvVideoMarker2 = (ImageView) convertView.findViewById(R.id.imv_videoMark2);

                    convertView.setTag(chattingHolder);

                } else {
                    chattingHolder = (ChattingHolder)convertView.getTag();
                }

                final GroupChatItem chatItem = (GroupChatItem) _chatList.get(position);

                chattingHolder.imvFriendPhoto.setVisibility(View.GONE);
                chattingHolder.txvFriendName.setVisibility(View.GONE);
                chattingHolder.txvFriendMessage.setVisibility(View.GONE);
                chattingHolder.txvTime1.setVisibility(View.GONE);
                chattingHolder.imvFriendImage.setVisibility(View.GONE);
                chattingHolder.fltVideoPreview1.setVisibility(View.GONE);

                chattingHolder.txvMyMessage.setVisibility(View.GONE);
                chattingHolder.txvTime2.setVisibility(View.GONE);
                chattingHolder.imvMyImage.setVisibility(View.GONE);
                chattingHolder.imvImageProgress2.setVisibility(View.GONE);
                chattingHolder.fltVideoPreview2.setVisibility(View.GONE);

                // my message
                if (chatItem.getSender() == _user.get_idx()) {

                    if (chatItem.getType() == GroupChatItem.ChatType.TEXT) {

                        chattingHolder.txvMyMessage.setVisibility(View.VISIBLE);
                        chattingHolder.txvMyMessage.setText(chatItem.getMessage());

                    } else if (chatItem.getType() == GroupChatItem.ChatType.IMAGE) {

                        chattingHolder.imvMyImage.setVisibility(View.VISIBLE);
                        chattingHolder.imvMyImage.setLocalImageBitmap(null);

                        switch (chatItem.get_status()) {

                            case START_UPLOADING: {

                                String filepath = BitmapUtils.getUploadFolderPath() + chatItem.getFilename();
                                Bitmap bitmap = loadBitmap(filepath);

                                if (bitmap != null) {
                                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(bitmap.getWidth(), bitmap.getHeight());
                                    chattingHolder.imvMyImage.setLayoutParams(layoutParams);
                                    chattingHolder.imvMyImage.setLocalImageBitmap(bitmap);
                                    BitmapUtils.setLocked(chattingHolder.imvMyImage);
                                } else {
                                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                    chattingHolder.imvMyImage.setLayoutParams(layoutParams);
                                    chattingHolder.imvMyImage.setDefaultImageResId(R.drawable.noimg);
                                    BitmapUtils.setUnlocked(chattingHolder.imvMyImage);
                                }

                                chattingHolder.imvImageProgress2.setVisibility(View.VISIBLE);
                                AnimationDrawable frameAni = (AnimationDrawable) chattingHolder.imvImageProgress2.getBackground();
                                frameAni.start();

                                new Uploadtask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, filepath, String.valueOf(GroupChatItem.ChatType.IMAGE.ordinal()), chatItem);
                                chatItem.set_status(GroupChatItem.StatusType.UPLOADING);

                                notifyDataSetChanged();

                            }
                            break;

                            case UPLOADING: {

                                String filepath = BitmapUtils.getUploadFolderPath() + chatItem.getFilename();
                                Bitmap bitmap = loadBitmap(filepath);

                                if (bitmap != null) {
                                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(bitmap.getWidth(), bitmap.getHeight());
                                    chattingHolder.imvMyImage.setLayoutParams(layoutParams);
                                    chattingHolder.imvMyImage.setLocalImageBitmap(bitmap);
                                    BitmapUtils.setLocked(chattingHolder.imvMyImage);
                                } else {
                                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                    chattingHolder.imvMyImage.setLayoutParams(layoutParams);
                                    chattingHolder.imvMyImage.setDefaultImageResId(R.drawable.noimg);
                                    BitmapUtils.setUnlocked(chattingHolder.imvMyImage);
                                }

                                chattingHolder.imvImageProgress2.setVisibility(View.VISIBLE);
                                AnimationDrawable frameAni = (AnimationDrawable) chattingHolder.imvImageProgress2.getBackground();
                                frameAni.start();
                            }
                            break;

                            case NORMAL: {

                                String filepath = BitmapUtils.getUploadFolderPath() + chatItem.getFilename();

                                Bitmap bitmap = loadBitmap(filepath);

                                if (bitmap != null) {
                                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(bitmap.getWidth(), bitmap.getHeight());
                                    chattingHolder.imvMyImage.setLayoutParams(layoutParams);
                                    chattingHolder.imvMyImage.setLocalImageBitmap(bitmap);
                                    BitmapUtils.setUnlocked(chattingHolder.imvMyImage);
                                } else {
                                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                    chattingHolder.imvMyImage.setLayoutParams(layoutParams);
                                    chattingHolder.imvMyImage.setDefaultImageResId(R.drawable.noimg);
                                    chattingHolder.imvMyImage.setImageUrl(chatItem.getFileUrl(), _imageLoader);
                                    BitmapUtils.setUnlocked(chattingHolder.imvMyImage);
                                }
                            }
                            break;
                        }

                    } else if (chatItem.getType() == GroupChatItem.ChatType.VIDEO) {

                        chattingHolder.fltVideoPreview2.setVisibility(View.VISIBLE);

                        String videoFilePath = chatItem.getFileUrl();
                        String thumbPath = BitmapUtils.getVideoThumbFolderPath() + getVideoThumbName(chatItem.getFilename());
                        Bitmap bitmap = loadBitmap(thumbPath);

                        switch (chatItem.get_status()) {

                            case START_UPLOADING: {

                                if (bitmap != null) {

                                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(bitmap.getWidth(), bitmap.getHeight());
                                    chattingHolder.imvVideoPreview2.setLayoutParams(layoutParams);
                                    chattingHolder.imvVideoPreview2.setImageBitmap(bitmap);
                                    BitmapUtils.setLocked(chattingHolder.imvVideoPreview2);
                                    chattingHolder.imvVideoMarker2.setVisibility(View.VISIBLE);

                                } else {
                                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                    chattingHolder.imvVideoPreview2.setLayoutParams(layoutParams);
                                    chattingHolder.imvVideoPreview2.setImageResource(R.drawable.nomov);
                                    BitmapUtils.setUnlocked(chattingHolder.imvVideoPreview2);
                                    chattingHolder.imvVideoMarker2.setVisibility(View.GONE);
                                }

                                new Uploadtask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, videoFilePath, String.valueOf(GroupChatItem.ChatType.VIDEO.ordinal()), chatItem);
                                chatItem.set_status(GroupChatItem.StatusType.UPLOADING);

                                chattingHolder.imvVideoProgress2.setVisibility(View.VISIBLE);
                                AnimationDrawable frameAni = (AnimationDrawable) chattingHolder.imvVideoProgress2.getBackground();
                                frameAni.start();

                                notifyDataSetChanged();

                            }
                            break;

                            case UPLOADING: {

                                if (bitmap != null) {
                                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(bitmap.getWidth(), bitmap.getHeight());
                                    chattingHolder.imvVideoPreview2.setLayoutParams(layoutParams);
                                    chattingHolder.imvVideoPreview2.setImageBitmap(bitmap);
                                    BitmapUtils.setLocked(chattingHolder.imvVideoPreview2);
                                    chattingHolder.imvVideoMarker2.setVisibility(View.VISIBLE);
                                } else {
                                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                    chattingHolder.imvVideoPreview2.setLayoutParams(layoutParams);
                                    chattingHolder.imvVideoPreview2.setImageResource(R.drawable.nomov);
                                    BitmapUtils.setUnlocked(chattingHolder.imvVideoPreview2);
                                    chattingHolder.imvVideoMarker2.setVisibility(View.GONE);
                                }

                                chattingHolder.imvVideoProgress2.setVisibility(View.VISIBLE);
                                AnimationDrawable frameAni = (AnimationDrawable) chattingHolder.imvVideoProgress2.getBackground();
                                frameAni.start();
                            }
                            break;

                            case NORMAL: {

                                if (bitmap != null) {
                                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(bitmap.getWidth(), bitmap.getHeight());
                                    chattingHolder.imvVideoPreview2.setLayoutParams(layoutParams);
                                    chattingHolder.imvVideoPreview2.setImageBitmap(bitmap);
                                    chattingHolder.imvVideoMarker2.setVisibility(View.VISIBLE);

                                }else {

                                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                    chattingHolder.imvVideoPreview2.setLayoutParams(layoutParams);
                                    chattingHolder.imvVideoPreview2.setImageResource(R.drawable.nomov);
                                    chattingHolder.imvVideoMarker2.setVisibility(View.GONE);
                                }

                                chattingHolder.imvVideoProgress2.setVisibility(View.GONE);
                                BitmapUtils.setUnlocked(chattingHolder.imvVideoPreview2);
                            }
                            break;
                        }


                    } else if (chatItem.getType() == GroupChatItem.ChatType.FILE) {

                        chattingHolder.txvMyMessage.setVisibility(View.VISIBLE);

                        switch (chatItem.get_status()) {

                            case START_UPLOADING: {

                                String filepath = chatItem.getFileUrl();

                                chattingHolder.txvMyMessage.setText(chatItem.getUploadFileName() + " : " + chatItem.get_progress() + "%");

                                new Uploadtask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, filepath, String.valueOf(GroupChatItem.ChatType.FILE.ordinal()), chatItem);
                                chatItem.set_status(GroupChatItem.StatusType.UPLOADING);
                                notifyDataSetChanged();
                            }
                            break;

                            case UPLOADING: {
                                chattingHolder.txvMyMessage.setText(chatItem.getUploadFileName() + " : " + chatItem.get_progress() + "%");
                            }
                            break;

                            case NORMAL: {
                                chattingHolder.txvMyMessage.setText(chatItem.getUploadFileName());
                            }
                            break;
                        }
                    }

                    chattingHolder.txvTime2.setVisibility(View.VISIBLE);
                    chattingHolder.txvTime2.setText(chatItem.getDisplayTime());

                } else {    // other message

                    FriendEntity friend = _context.get_roomEntity().getParticipant(chatItem.getSender());

                    if (friend != null) {

                        chattingHolder.imvFriendPhoto.setVisibility(View.VISIBLE);
                        chattingHolder.txvFriendName.setVisibility(View.VISIBLE);
                        chattingHolder.txvTime1.setVisibility(View.VISIBLE);

                        chattingHolder.imvFriendPhoto.setImageUrl(friend.get_photoUrl(), _imageLoader);
                        chattingHolder.txvFriendName.setText(friend.get_name());
                        chattingHolder.txvTime1.setText(chatItem.getDisplayTime());

                        if (chatItem.getType() == GroupChatItem.ChatType.TEXT) {

                            chattingHolder.txvFriendMessage.setVisibility(View.VISIBLE);
                            chattingHolder.txvFriendMessage.setText(chatItem.getMessage());

                        } else if (chatItem.getType() == GroupChatItem.ChatType.IMAGE) {

                            chattingHolder.imvFriendImage.setVisibility(View.VISIBLE);
                            chattingHolder.imvFriendImage.setDefaultImageResId(R.drawable.noimg);
                            chattingHolder.imvFriendImage.setImageUrl(chatItem.getFileUrl(), _imageLoader);

                        } else if (chatItem.getType() == GroupChatItem.ChatType.VIDEO) {

                            chattingHolder.fltVideoPreview1.setVisibility(View.VISIBLE);

                            String videoFileUrl = chatItem.getFileUrl();
                            String thumbPath = BitmapUtils.getVideoThumbFolderPath() + getVideoThumbName(chatItem.getFilename());
                            String videoLocalPath = BitmapUtils.getDownloadFolderPath() + chatItem.getFilename();
                            Bitmap bitmap = loadBitmap(thumbPath);
                            File file = new File(videoLocalPath);

                            switch (chatItem.get_status()) {

                                case NORMAL: {

                                    if (file.exists()) {

                                        if (bitmap == null)
                                            bitmap = _context.saveThumbnail(videoLocalPath);

                                        if (bitmap != null) {
                                            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(bitmap.getWidth(), bitmap.getHeight());
                                            chattingHolder.imvVideoPreview1.setLayoutParams(layoutParams);
                                            chattingHolder.imvVideoPreview1.setImageBitmap(bitmap);
                                            chattingHolder.imvVideoMarker1.setVisibility(View.VISIBLE);
                                        } else {
                                            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                            chattingHolder.imvVideoPreview1.setLayoutParams(layoutParams);
                                            chattingHolder.imvVideoPreview1.setImageResource(R.drawable.nomov);
                                            chattingHolder.imvVideoMarker1.setVisibility(View.GONE);
                                        }

                                        chattingHolder.imvVideoProgress1.setVisibility(View.GONE);
                                        BitmapUtils.setUnlocked(chattingHolder.imvVideoPreview1);

                                        if (_firstNormal) {
                                            notifyDataSetChanged();
                                            _firstNormal = false;
                                        }

                                    } else {
                                        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                        chattingHolder.imvVideoPreview1.setLayoutParams(layoutParams);
                                        chattingHolder.imvVideoPreview1.setImageResource(R.drawable.nomov);
                                        chattingHolder.imvVideoMarker1.setVisibility(View.GONE);

                                        chatItem.set_status(GroupChatItem.StatusType.DOWNLOADING);
                                        new DownloadTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, videoFileUrl, chatItem.getFilename(), chatItem);

                                        chattingHolder.imvVideoProgress1.setVisibility(View.VISIBLE);
                                        AnimationDrawable frameAni = (AnimationDrawable) chattingHolder.imvVideoProgress1.getBackground();
                                        frameAni.start();

                                        notifyDataSetChanged();
                                    }

                                }
                                break;

                                case DOWNLOADING: {
                                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                    chattingHolder.imvVideoPreview1.setLayoutParams(layoutParams);
                                    chattingHolder.imvVideoPreview1.setImageResource(R.drawable.nomov);
                                    chattingHolder.imvVideoMarker1.setVisibility(View.GONE);
                                    chattingHolder.imvVideoProgress1.setVisibility(View.VISIBLE);
                                    AnimationDrawable frameAni = (AnimationDrawable) chattingHolder.imvVideoProgress1.getBackground();
                                    frameAni.start();
                                }

                                break;
                            }

                        } else if (chatItem.getType() == GroupChatItem.ChatType.FILE) {

                            chattingHolder.txvFriendMessage.setVisibility(View.VISIBLE);

                            String fileUrl = chatItem.getFileUrl();

                            switch (chatItem.get_status()) {

                                case START_DOWNLOADING: {
                                    new DownloadTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, fileUrl, chatItem.getFilename(), chatItem);
                                    chatItem.set_status(GroupChatItem.StatusType.DOWNLOADING);
                                    chattingHolder.txvFriendMessage.setText(chatItem.getUploadFileName() + " : " + chatItem.get_progress() + "%");
                                    notifyDataSetChanged();
                                }
                                break;

                                case DOWNLOADING:
                                    chattingHolder.txvFriendMessage.setText(chatItem.getUploadFileName() + " : " + chatItem.get_progress() + "%");
                                    break;

                                case NORMAL:
                                    chattingHolder.txvFriendMessage.setText(chatItem.getUploadFileName());
                                    break;

                            }
                        }
                    }

                }

                return convertView;
            }
        }
    }

    public void addItem(GroupChatItem entity){

        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
        DateFormat outFormat = DateFormat.getDateInstance(DateFormat.LONG);

        String time = entity.getTime().split(",")[0];

        try {

            Date date = df.parse(time);
            if (_lastDate == null || _lastDate.before(date)) {

                String strDate = outFormat.format(date);
                _chatList.add(strDate);
                _lastDate = date;
            }
        } catch (Exception ex) {
        }

        _chatList.add(entity);
    }

    public void addItems(ArrayList<GroupChatItem> items) {

        for (GroupChatItem item : items)
            addItem(item);
    }


    public String getVideoThumbName(String filename) {

        int pos = filename.lastIndexOf(".");
        if (pos > 0) {
            filename = filename.substring(0, pos);
        }

        filename += ".jpg";

        return filename;
    }


    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            mMemoryCache.put(key, bitmap);
        }
    }

    public Bitmap getBitmapFromMemCache(String key) {
        return mMemoryCache.get(key);
    }

    public Bitmap loadBitmap(String filepath) {

        Bitmap bitmap = getBitmapFromMemCache(filepath);

        if (bitmap != null) {
            return bitmap;

        } else {
            bitmap = BitmapUtils.decodeFile(filepath);

            if (bitmap != null)
                addBitmapToMemoryCache(filepath, bitmap);

            return bitmap;
        }
    }

    public void setFileUrl(GroupChatItem chatItem, String fileUrl) {

        String localPath = chatItem.getFileUrl();

        String message = chatItem.getMessage();
        message = message.replace(localPath, fileUrl);

        chatItem.setMessage(message);

    }

    public class ChattingHolder {

        public CirculaireNetworkImageView imvFriendPhoto;
        public TextView txvFriendName;
        public TextView txvFriendMessage;
        public CustomNetworkImageView imvFriendImage;
        public TextView txvTime1;
        public FrameLayout fltVideoPreview1;
        public ImageView imvVideoPreview1;
        public ImageView imvVideoMarker1;
        public ImageView imvVideoProgress1;

        public TextView txvMyMessage;
        public CustomNetworkImageView imvMyImage;
        public ImageView imvImageProgress2;
        public TextView txvTime2;
        public FrameLayout fltVideoPreview2;
        public ImageView imvVideoPreview2;
        public ImageView imvVideoProgress2;
        public ImageView imvVideoMarker2;

    }

    public class DateHolder {

        public TextView txvDate;
    }


    private class Uploadtask extends AsyncTask<Object, Integer, String> {

        long totalSize = 0;
        GroupChatItem _chatItem = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {

            _chatItem.set_progress(progress[0]);

            if (_chatItem.getType() == GroupChatItem.ChatType.FILE)
                notifyDataSetChanged();

        }

        @Override
        protected String doInBackground(Object... params) {

            Logger.d("TEST", "adapter doinbackground:" + params[0]);

            String filename = (String) params[0];
            String type = (String) params[1];
            _chatItem = (GroupChatItem) params[2];
            return upload(filename, type);
        }

        private String upload(String filepath, String type) {

            String responseString = "no";
            String urlString = ReqConst.SERVER_URL + ReqConst.REQ_UPLOADFILE;

            File sourceFile = new File(filepath);
            if (!sourceFile.isFile()) {
                return responseString;
            }

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(urlString);

            try {
                CustomMultiPartEntity entity = new CustomMultiPartEntity(new CustomMultiPartEntity.ProgressListener() {

                    @Override
                    public void transferred(long num) {
                        publishProgress((int) ((num / (float) totalSize) * 100));
                    }
                });

                String filename = Commons.fileNameWithExtFromPath(filepath);
                filename = URLEncoder.encode(filename, "utf-8").replace("+", "%20");

                entity.addPart(ReqConst.PARAM_ID, new StringBody(String.valueOf(_user.get_idx())));
                entity.addPart(ReqConst.PARAM_TYPE, new StringBody(type));
                entity.addPart(ReqConst.PARAM_FILENAME, new StringBody(filename));
                entity.addPart(ReqConst.PARAM_FILE, new FileBody(sourceFile));
                totalSize = entity.getContentLength();
                httppost.setEntity(entity);
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();
                responseString = EntityUtils.toString(r_entity);

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return responseString;

        }

        @Override
        protected void onPostExecute(String result) {

            try {

                JSONObject response = new JSONObject(result);

                int resultCode = response.getInt(ReqConst.RES_CODE);

                if (resultCode == ReqConst.CODE_SUCCESS) {

                    _chatItem.set_status(GroupChatItem.StatusType.NORMAL);

                    String file = response.getString(ReqConst.RES_FILE_URL);
                    String filename = response.getString(ReqConst.RES_FILENAME);
                    setFileUrl(_chatItem, file);
                    _context.onSuccessUpload(file, filename, _chatItem.getType());

                } else {
                    _chatList.remove(_chatItem);
                    _context.onFailUpload();
                }

            } catch (Exception ex) {
                _chatList.remove(_chatItem);
                _context.onFailUpload();
            }

            notifyDataSetChanged();
            super.onPostExecute(result);

        }
    }

    private class DownloadTask extends AsyncTask<Object, Integer, String> {

        GroupChatItem _chatItem = null;

        @Override
        protected void onProgressUpdate(Integer... progress) {

            _chatItem.set_progress(progress[0]);

            if (_chatItem.getType() == GroupChatItem.ChatType.FILE)
                notifyDataSetChanged();
        }

        @Override
        protected String doInBackground(Object... params) {

            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;

            String strUrl = (String) params[0];
            String outfile = BitmapUtils.getDownloadFolderPath() + (String) params[1];
            _chatItem = (GroupChatItem) params[2];

            String returned = "";

            try {

                URL url = new URL(strUrl);

                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                // expect HTTP 200 OK, so we don't mistakenly save error report
                // instead of the file
                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    return "Server returned HTTP " + connection.getResponseCode()
                            + " " + connection.getResponseMessage();
                }

                // this will be useful to display download percentage
                // might be -1: server did not report the length
                int fileLength = connection.getContentLength();

                // download the file
                input = connection.getInputStream();
                output = new FileOutputStream(outfile);

                byte data[] = new byte[4096];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    // allow canceling with back button
                    if (isCancelled()) {
                        input.close();
                        return null;
                    }
                    total += count;
                    // publishing the progress....
                    if (fileLength > 0) // only if total length is known
                        publishProgress((int) (total * 100 / fileLength));

                    output.write(data, 0, count);

                }

                returned = outfile;

            } catch (Exception e) {
                return "";
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                }

                if (connection != null)
                    connection.disconnect();
            }
            return returned;
        }

        @Override
        protected void onPostExecute(String result) {

            _chatItem.set_status(GroupChatItem.StatusType.NORMAL);

            if (result.length() > 0 && _chatItem.getType() == GroupChatItem.ChatType.VIDEO) {
                _context.saveThumbnail(result);
            }

            notifyDataSetChanged();

            _firstNormal = true;

            Logger.d("FILE", "writing file completed");

            super.onPostExecute(result);

        }
    }


}
