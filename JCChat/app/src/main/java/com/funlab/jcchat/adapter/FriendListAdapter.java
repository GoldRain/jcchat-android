package com.funlab.jcchat.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.funlab.jcchat.JCChatApplication;
import com.funlab.jcchat.R;
import com.funlab.jcchat.Utils.CirculaireNetworkImageView;
import com.funlab.jcchat.Utils.HangulUtils;
import com.funlab.jcchat.model.FriendEntity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by HGS on 12/13/2015.
 */
public class FriendListAdapter extends BaseAdapter{

    private Context _context = null;
    private ArrayList<FriendEntity> _friendDatas = new ArrayList<FriendEntity>();
    private ArrayList<FriendEntity> _friendAllDatas = new ArrayList<FriendEntity>();

    private ImageLoader _imageLoader;

    public FriendListAdapter(Context _context){

        super();
        this._context = _context;

        _imageLoader = JCChatApplication.getInstance().getImageLoader();
    }

    @Override
    public int getCount(){

        return _friendDatas.size();
    }

    @Override
    public Object getItem(int position){

        return _friendDatas.get(position);
    }

    @Override
    public long getItemId(int position){

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){

        FriendHolder friendHolder;

        if(convertView == null){
            friendHolder = new FriendHolder();

            LayoutInflater inflater = (LayoutInflater)_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.friend_list_item, null);

            friendHolder.imvFriendPhoto = (CirculaireNetworkImageView)convertView.findViewById(R.id.imv_friendPhoto);
            friendHolder.txvFriendName = (TextView)convertView.findViewById(R.id.txv_friendName);
            friendHolder.txvFriendState = (TextView)convertView.findViewById(R.id.txv_friendState);

            convertView.setTag(friendHolder);

        } else {
            friendHolder = (FriendHolder)convertView.getTag();
        }

        FriendEntity friend = _friendDatas.get(position);

        friendHolder.txvFriendName.setText(friend.get_name());
        friendHolder.txvFriendState.setText(friend.get_label());

        if (friend.get_photoUrl().length() > 0)
            friendHolder.imvFriendPhoto.setImageUrl(friend.get_photoUrl(), _imageLoader);
        else
            friendHolder.imvFriendPhoto.setImageResource(R.drawable.bg_non_profile1);


        return convertView;
    }

    public void clearAll() {

        _friendAllDatas.clear();
    }

    public void addItem(FriendEntity entity){

        _friendAllDatas.add(entity);
    }

    public void initFriendDatas() {

        sortFriendByName();
        _friendDatas.clear();
        _friendDatas.addAll(_friendAllDatas);
    }

    public void sortFriendByName() {

        Collections.sort(_friendAllDatas, new Comparator<FriendEntity>() {

            public int compare(FriendEntity f1, FriendEntity f2) {

                return f1.get_name().compareTo(f2.get_name());
            }
        });
    }

    public void filter(String charText) {

        charText = charText.toLowerCase();

        _friendDatas.clear();

        if (charText.length() == 0) {
            _friendDatas.addAll(_friendAllDatas);

        } else {

            for (FriendEntity friend : _friendAllDatas) {

                String value = friend.get_name().toLowerCase();

                if (value.contains(charText)
                        || HangulUtils.isHangulInitialSound(value, charText)) {
                    _friendDatas.add(friend);
                }
            }
        }
        notifyDataSetChanged();
    }

    public class FriendHolder {

        public CirculaireNetworkImageView imvFriendPhoto;
        public TextView txvFriendName;
        public TextView txvFriendState;
    }

}
