package com.funlab.jcchat.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.funlab.jcchat.JCChatApplication;
import com.funlab.jcchat.R;
import com.funlab.jcchat.Utils.CirculaireNetworkImageView;
import com.funlab.jcchat.Utils.HangulUtils;
import com.funlab.jcchat.chatting.GroupChattingActivity;
import com.funlab.jcchat.commons.Commons;
import com.funlab.jcchat.commons.Constants;
import com.funlab.jcchat.model.FriendEntity;
import com.funlab.jcchat.model.RoomEntity;

import java.util.ArrayList;

/**
 * Created by HGS on 12/14/2015.
 */
public class ChatListAdapter extends BaseAdapter {

    private Context _context;
    private ArrayList<RoomEntity> _roomDatas = new ArrayList<RoomEntity>();
    private ArrayList<RoomEntity> _roomAllDatas = new ArrayList<RoomEntity>();

    ImageLoader _imageLoader;

    public ChatListAdapter(Context _context){

        super();
        this._context = _context;

        _imageLoader = JCChatApplication.getInstance().getImageLoader();

        _roomAllDatas = Commons.g_user.get_roomList();
        _roomDatas.addAll(_roomAllDatas);
    }

    public void refresh() {
        _roomDatas.clear();
        _roomAllDatas = Commons.g_user.get_roomList();
        _roomDatas.addAll(_roomAllDatas);
        notifyDataSetChanged();
    }

    @Override
    public int getCount(){

        return _roomDatas.size();
    }

    @Override
    public Object getItem(int position){

        return _roomDatas.get(position);
    }

    @Override
    public long getItemId(int position){

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){

        ChattingHolder chattingHolder;

        if(convertView == null){
            chattingHolder = new ChattingHolder();

            LayoutInflater inflater = (LayoutInflater)_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.chatting_list_item, null);

            chattingHolder.imvChattingPhoto = (CirculaireNetworkImageView)convertView.findViewById(R.id.imv_ChattingPhoto);
            chattingHolder.txvName = (TextView)convertView.findViewById(R.id.txv_Name);
            chattingHolder.txvChatContent = (TextView)convertView.findViewById(R.id.txv_chattingContent);
            chattingHolder.txvDateHour = (TextView)convertView.findViewById(R.id.txv_date_hour);
            chattingHolder.txvChatNum = (TextView)convertView.findViewById(R.id.txv_chattingNum);

            convertView.setTag(chattingHolder);
        }else {
            chattingHolder = (ChattingHolder)convertView.getTag();
        }

        final RoomEntity room = _roomDatas.get(position);

        chattingHolder.txvName.setText(room.get_displayName() + room.get_displayCount());
        chattingHolder.txvChatContent.setText(room.get_recentContent());
        chattingHolder.txvDateHour.setText(room.get_recentTime());
        chattingHolder.txvChatNum.setText(String.valueOf(room.get_recentCounter()));

        String[] parti = room.get_name().split("_");
        if (parti.length == 2) {

            int otherIdx = Integer.parseInt(parti[0]);
            if (otherIdx == Commons.g_user.get_idx())
                otherIdx = Integer.parseInt(parti[1]);

            FriendEntity friend = room.getParticipant(otherIdx);
            if (friend != null)
                chattingHolder.imvChattingPhoto.setImageUrl(friend.get_photoUrl(), _imageLoader);
        } else {
            chattingHolder.imvChattingPhoto.setImageBitmap(null);
        }

        if(room.get_recentCounter() == 0){
            chattingHolder.txvChatNum.setVisibility(View.INVISIBLE);
        }else {
            chattingHolder.txvChatNum.setVisibility(View.VISIBLE);
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(_context, GroupChattingActivity.class);
                intent.putExtra(Constants.KEY_ROOM, room.get_name());
                _context.startActivity(intent);
            }
        });

        return convertView;
    }

    public void filter(String charText){

        charText = charText.toLowerCase();

        _roomDatas.clear();

        if(charText.length() == 0){
            _roomDatas.addAll(_roomAllDatas);
        }else {

            for (RoomEntity room : _roomAllDatas){

                String value = room.get_displayName();

                if(value.contains(charText)
                        || HangulUtils.isHangulInitialSound(value, charText)){
                    _roomDatas.add(room);
                }
            }
        }
        notifyDataSetChanged();
    }

    public class ChattingHolder {

        public CirculaireNetworkImageView imvChattingPhoto;
        public TextView txvName;
        public TextView txvChatContent;
        public TextView txvDateHour;
        public TextView txvChatNum;
    }
}
