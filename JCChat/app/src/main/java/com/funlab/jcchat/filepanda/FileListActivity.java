package com.funlab.jcchat.filepanda;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.ImageView;

import com.funlab.jcchat.R;
import com.funlab.jcchat.commons.Constants;

import java.io.File;
import java.io.IOException;


public class FileListActivity extends FragmentActivity implements
		FileListFragment.Callbacks, View.OnClickListener  {

	public static final int CODE_LIST = 100;
	/**
	 * Whether or not the activity is in two-pane mode, i.e. running on a tablet
	 * device.
	 */

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_file_list);

		String root = getIntent().getStringExtra("root");

		FileListFragment fragment = (FileListFragment) getSupportFragmentManager()
				.findFragmentById(R.id.file_list);
		fragment.setActivateOnItemClick(true);

		if (root != null) {
			fragment.loadFileDir(root);
		}

		ImageView imvBack = (ImageView) findViewById(R.id.imv_back);
		imvBack.setOnClickListener(this);

	}
	
	/**
	 * Callback method from {@link FileListFragment.Callbacks} indicating that
	 * the item with the given ID was selected.
	 */
	@Override
	public void onItemSelected(File f) {

		openFile(f);
	}

	public void openFile(File f) {

		if (f.isDirectory()) {

			Intent fileList = new Intent(this, FileListActivity.class);

			try {

				fileList.putExtra("root", f.getCanonicalPath());
				startActivityForResult(fileList, CODE_LIST);
			} catch (IOException e) {
				e.printStackTrace();
			}

		} else {

			Intent intent = new Intent();
			intent.putExtra(Constants.KEY_FILE, f.getAbsolutePath());
			setResult(RESULT_OK, intent);
			finish();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		super.onActivityResult(requestCode, resultCode, data);

		switch (requestCode) {

			case CODE_LIST:

				if (resultCode == RESULT_OK) {

					setResult(RESULT_OK, data);
					finish();
				}

				break;
		}
	}

	public void onClick(View view){

		if(view.getId() == R.id.imv_back) {
			finish();
		}
	}

}
