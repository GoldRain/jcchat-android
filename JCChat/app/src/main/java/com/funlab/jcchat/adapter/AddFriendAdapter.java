package com.funlab.jcchat.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.funlab.jcchat.JCChatApplication;
import com.funlab.jcchat.R;
import com.funlab.jcchat.Utils.CirculaireNetworkImageView;
import com.funlab.jcchat.main.AddFriendActivity;
import com.funlab.jcchat.model.FriendEntity;

import java.util.ArrayList;

/**
 * Created by HGS on 12/14/2015.
 */
public class AddFriendAdapter extends BaseAdapter {

    private AddFriendActivity _context =null;
    private ArrayList<FriendEntity> _addFriendDatas = new ArrayList<FriendEntity>();

    private ImageLoader _imageLoader;

    public AddFriendAdapter(AddFriendActivity _context){

        super();
        this._context = _context;

        _imageLoader = JCChatApplication.getInstance().getImageLoader();
    }

    @Override
    public int getCount(){

        return _addFriendDatas.size();
    }

    @Override
    public Object getItem(int position){

        return _addFriendDatas.get(position);
    }

    @Override
    public long getItemId(int position){

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){

        AddFriendHolder addFriendHolder;

        if(convertView == null){
            addFriendHolder = new AddFriendHolder();

            LayoutInflater inflater = (LayoutInflater)_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.add_friend_list_item, null);

            addFriendHolder.imvAddFriendPhoto = (CirculaireNetworkImageView)convertView.findViewById(R.id.imv_addFriendPhoto);
            addFriendHolder.txvFriendId = (TextView)convertView.findViewById(R.id.txv_addFriendId);
            addFriendHolder.txvAddFriend = (TextView)convertView.findViewById(R.id.txv_addFriend_btn);

            convertView.setTag(addFriendHolder);
        }else {
            addFriendHolder = (AddFriendHolder)convertView.getTag();
        }

        final FriendEntity friend = _addFriendDatas.get(position);

        addFriendHolder.txvFriendId.setText(friend.get_name());
        addFriendHolder.txvAddFriend.setText(_context.getString(R.string.add));
        addFriendHolder.txvAddFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 addFriend(friend);
            }
        });

        if (friend.get_photoUrl().length() > 0)
            addFriendHolder.imvAddFriendPhoto.setImageUrl(friend.get_photoUrl(), _imageLoader);
        else
            addFriendHolder.imvAddFriendPhoto.setImageResource(R.drawable.bg_non_profile1);

        return convertView;
    }

    public void addItem(FriendEntity entity){
        _addFriendDatas.add(entity);
    }

    public void removeItem(FriendEntity entity) {
        _addFriendDatas.remove(entity);
    }

    public void clearFriendData(){
        _addFriendDatas.clear();
    }

    public void addFriend(FriendEntity friend) {
        _context.addFriend(friend);
    }

    public class AddFriendHolder {

        public CirculaireNetworkImageView imvAddFriendPhoto;
        public TextView txvFriendId;
        public TextView txvAddFriend;
    }
}
