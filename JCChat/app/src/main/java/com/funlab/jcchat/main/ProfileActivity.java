package com.funlab.jcchat.main;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.funlab.jcchat.Base.CommonActivity;
import com.funlab.jcchat.JCChatApplication;
import com.funlab.jcchat.R;
import com.funlab.jcchat.Utils.BitmapUtils;
import com.funlab.jcchat.Utils.CirculaireNetworkImageView;
import com.funlab.jcchat.Utils.MultiPartRequest;
import com.funlab.jcchat.Utils.PhotoSelectDialog;
import com.funlab.jcchat.Utils.RectNetImageView;
import com.funlab.jcchat.adapter.FriendMomentListAdapter;
import com.funlab.jcchat.commons.Commons;
import com.funlab.jcchat.commons.Constants;
import com.funlab.jcchat.commons.ReqConst;
import com.funlab.jcchat.model.ImageDataEntity;
import com.funlab.jcchat.model.MomentEntity;
import com.funlab.jcchat.model.UserEntity;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ProfileActivity extends CommonActivity implements View.OnClickListener {

    private final int IMGTYPE_PROFILE = 1;
    private final int IMGTYPE_BACKGROUND = 2;

    private Uri _imageCaptureUri;
    private String _photoPath = "";

    private ImageView ui_imvProfileBackground ;
    private CirculaireNetworkImageView ui_imvProfileMan;
    private RectNetImageView ui_imvBg;
    private ImageLoader _imageLoader;

    private int _imgType = 0;

    private UserEntity _user;
    private PhotoSelectDialog _photoSelectDialog;

    ExpandableHeightListView lst_my_profile;
    FriendMomentListAdapter _adapter_profile;

    ArrayList<MomentEntity> _moments = new ArrayList<>();

    ArrayList<ImageDataEntity> _images_data = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        _user = Commons.g_user;

        loadLayout();
    }

    public void loadLayout(){

        ImageView imvProfileClose = (ImageView)findViewById(R.id.imv_profile_close);
        imvProfileClose.setOnClickListener(this);

        ui_imvProfileBackground = (ImageView)findViewById(R.id.imv_profile_bg);
        ui_imvProfileBackground.setOnClickListener(this);

        ui_imvProfileMan = (CirculaireNetworkImageView)findViewById(R.id.imv_profile_man);
        _imageLoader = JCChatApplication.getInstance().getImageLoader();
        ui_imvProfileMan.setImageUrl(_user.get_photoUrl(), _imageLoader);
        ui_imvProfileMan.setOnClickListener(this);

        TextView txvLabel = (TextView) findViewById(R.id.txv_profile_title);
        txvLabel.setText(_user.get_label());

        TextView txvEmail = (TextView)findViewById(R.id.profile_email);
        txvEmail.setText(_user.get_email());

        TextView btnProfileManage = (TextView)findViewById(R.id.btn_profile_manage);
        btnProfileManage.setOnClickListener(this);

        ui_imvBg = (RectNetImageView)findViewById(R.id.imv_bg);
        ui_imvBg.setOnClickListener(this);
        _imageLoader = JCChatApplication.getInstance().getImageLoader();
        ui_imvBg.setImageUrl(_user.get_bgUrl(), _imageLoader);

        lst_my_profile = (ExpandableHeightListView) findViewById(R.id.lst_my_profile);
        lst_my_profile.setExpanded(true);

        getMyMomentInfo();
    }

    public void getMyMomentInfo(){

        String url = "http://35.163.255.175/index.php/Api/getMomentInfo";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                parseMomentInfo(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        })
        {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    //params.put("user_id", String.valueOf(_friend.get_idx()));
                    params.put("user_id", String.valueOf(Commons.g_user.get_idx()));

                } catch (Exception e) {

                    closeProgress();
                    showAlertDialog(getString(R.string.error));
                }
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        JCChatApplication.getInstance().addToRequestQueue(request, url);
    }

    public void parseMomentInfo(String response){

        Log.d("response-->", response);

        closeProgress();

        try {
            JSONObject object = new JSONObject(response);
            int result_code = object.getInt(ReqConst.RES_CODE);

            if (result_code == 0){

                JSONArray moment_info = object.getJSONArray("Moment");

                for(int i = 0; i < moment_info.length(); i++){

                    MomentEntity moment_entity = new MomentEntity();

                    JSONObject jsonObject = moment_info.getJSONObject(i);

                    JSONObject moment_data = jsonObject.getJSONObject("moment_data");

                    moment_entity.set_date(moment_data.getString("post_date"));
                    moment_entity.set_time(moment_data.getString("post_time"));
                    moment_entity.set_comment(moment_data.getString("mo_comment"));

                    JSONArray json_images_data = jsonObject.getJSONArray("images_data");

                    Log.d("===size-->", String.valueOf(json_images_data.length()));
                    Log.d("id===>", String.valueOf(Commons.g_user.get_idx()));

                    ArrayList<ImageDataEntity> imageDataEntities = new ArrayList<>();

                    for (int j = 0; j < json_images_data.length(); j++){

                        JSONObject jsonObject1 = json_images_data.getJSONObject(j);

                        ImageDataEntity images = new ImageDataEntity();

                        images.set_videoUrl(jsonObject1.getString("mo_video"));
                        images.set_photos(jsonObject1.getString("mo_image"));

                        imageDataEntities.add(images);
                    }

                    moment_entity.set_images_data(imageDataEntities);

                    _moments.add(moment_entity);
                }

                _adapter_profile = new FriendMomentListAdapter(this, _moments);
                lst_my_profile.setAdapter(_adapter_profile);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void onClick(View view){

        switch (view.getId()){

            case R.id.imv_profile_close:
                gotoFriendActivity();
                break;

            case R.id.btn_profile_manage:
                gotoMyInfoActivity();
                break;

            case R.id.imv_profile_man:
                showProfilePhoto();
                break;
            case R.id.imv_profile_bg:
                getProfileBackground();
                break;

            case R.id.imv_bg:
                showBackgroundPhoto();
                break;
        }
    }

    public void gotoFriendActivity(){
        finish();
    }

    public void gotoMyInfoActivity(){

        setResult(RESULT_OK);
        finish();
    }

    public void showProfilePhoto(){

        if (_user.get_photoUrl().length() > 0) {

            MimeTypeMap myMime = MimeTypeMap.getSingleton();
            Intent newIntent = new Intent(android.content.Intent.ACTION_VIEW);
            String ext = Commons.fileExtFromUrl(_user.get_photoUrl()).substring(1);
            String mimeType = myMime.getMimeTypeFromExtension(ext);
            newIntent.setDataAndType(Uri.parse(_user.get_photoUrl()), mimeType);

            try {
                startActivity(newIntent);
            } catch (android.content.ActivityNotFoundException e) {
                showToast(getString(R.string.not_support_file));
            }
        }
    }

    public void showBackgroundPhoto(){

        if (_user.get_bgUrl().length() > 0) {


            MimeTypeMap myMime = MimeTypeMap.getSingleton();
            Intent newIntent = new Intent(android.content.Intent.ACTION_VIEW);
            String ext = Commons.fileExtFromUrl(_user.get_bgUrl()).substring(1);
            String mimeType = myMime.getMimeTypeFromExtension(ext);
            newIntent.setDataAndType(Uri.parse(_user.get_bgUrl()), mimeType);

            try {
                startActivity(newIntent);
            } catch (android.content.ActivityNotFoundException e) {
                showToast(getString(R.string.not_support_file));
            }
        }

    }

    public void getProfileBackground(){

        _imgType = IMGTYPE_BACKGROUND;

        View.OnClickListener cameraListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doTakePhotoAction();
                _photoSelectDialog.dismiss();
            }
        };
        View.OnClickListener albumListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doTakeGallery();
                _photoSelectDialog.dismiss();
            }
        };
        View.OnClickListener deleteListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteBackground();
                _photoSelectDialog.dismiss();
            }
        };
        View.OnClickListener cancelListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _photoSelectDialog.dismiss();
            }
        };

        if (_user.get_bgUrl().length() > 0)
            _photoSelectDialog = new PhotoSelectDialog(_context, cameraListener, albumListener, deleteListener, cancelListener);
        else
            _photoSelectDialog = new PhotoSelectDialog(_context, cameraListener, albumListener, cancelListener);

        _photoSelectDialog.show();
    }

    public void deleteBackground() {

        String url = ReqConst.SERVER_URL + ReqConst.REQ_REMOVEBACKGROUND;
        String params = String.format("/%d", _user.get_idx());
        url += params;

        showProgress();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>(){

            @Override
            public void onResponse(String json) {
                parseDeleteResponse(json);
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        JCChatApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    public void parseDeleteResponse(String json){

        closeProgress();

        try{

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if(result_code == ReqConst.CODE_SUCCESS){
                ui_imvBg.setImageDrawable(null);
                _user.set_bgUrl("");
                _photoPath = "";
            } else {
                showAlertDialog(getString(R.string.error));
            }

        }catch (JSONException e){

            e.printStackTrace();
            showAlertDialog(getString(R.string.error));
        }
    }

    public void uploadImage() {

        //if no profile photo
        if (_photoPath.length() == 0) {
            closeProgress();
            return;
        }

        try {

            File file = new File(_photoPath);

            Map<String, String> params = new HashMap<String, String>();
            params.put(ReqConst.PARAM_ID, String.valueOf(_user.get_idx()));

            if (_imgType == IMGTYPE_BACKGROUND){
                params.put(ReqConst.PARAM_IMAGE_TYPE, String.valueOf(1));
            }
            else if (_imgType == IMGTYPE_PROFILE){
                params.put(ReqConst.PARAM_IMAGE_TYPE, String.valueOf(0));
            }

            String url = ReqConst.SERVER_URL + ReqConst.REQ_UPLOADIMAGE;

            MultiPartRequest reqMultiPart = new MultiPartRequest(url, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    closeProgress();
                    showToast(getString(R.string.photo_upload_fail));

                }
            }, new Response.Listener<String>() {

                @Override
                public void onResponse(String json) {

                    ParseUploadImgResponse(json);
                }
            }, file, ReqConst.PARAM_FILE, params);

            reqMultiPart.setRetryPolicy(new DefaultRetryPolicy(
                    Constants.VOLLEY_TIME_OUT, 0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            JCChatApplication.getInstance().addToRequestQueue(reqMultiPart, url);

        } catch (Exception e) {

            e.printStackTrace();
            closeProgress();

            showToast(getString(R.string.photo_upload_fail));

        }
    }

    public void ParseUploadImgResponse(String json){

        closeProgress();

        try{
            JSONObject response = new JSONObject(json);
            int result_code = response.getInt(ReqConst.RES_CODE);


            if (result_code == 0){

                if(_imgType == IMGTYPE_PROFILE){
                    String file = response.getString(ReqConst.RES_FILE_URL);
                    _user.set_photoUrl(file);
                    Commons.g_user = _user;
                    ui_imvProfileMan.setImageUrl(_user.get_photoUrl(), _imageLoader);

                }else if(_imgType == IMGTYPE_BACKGROUND){
                    String file = response.getString(ReqConst.RES_FILE_URL);
                    _user.set_bgUrl(file);
                    Commons.g_user = _user;
                    ui_imvBg.setImageUrl(_user.get_bgUrl(), _imageLoader);
                }

            }
            else if(result_code == 111){
                showToast(getString(R.string.photo_upload_fail));
            }
        }catch (JSONException e){

            e.printStackTrace();

        }
    }

    public void doTakePhotoAction(){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String picturePath = BitmapUtils.getTempFolderPath() + "photo_temp.jpg";
        _imageCaptureUri = Uri.fromFile(new File(picturePath));

        intent.putExtra(MediaStore.EXTRA_OUTPUT, _imageCaptureUri);
        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);

    }

    private void doTakeGallery(){

        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, Constants.PICK_FROM_ALBUM);
    }

    protected void onActivityResult(int requestCOde, int resultCode, Intent data){

        switch (requestCOde){

            case Constants.CROP_FROM_CAMERA: {

                if (resultCode == RESULT_OK){
                    try {

                        File saveFile = BitmapUtils.getOutputMediaFile(this);

                        InputStream in = getContentResolver().openInputStream(Uri.fromFile(saveFile));
                        BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);

                        in.close();

                        //set The bitmap data to image View
                        if (_imgType == IMGTYPE_BACKGROUND){
                            _photoPath = saveFile.getAbsolutePath();
                            uploadImage();
                        }
                        else if (_imgType == IMGTYPE_PROFILE){
                            _photoPath = saveFile.getAbsolutePath();
                            uploadImage();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
            case Constants.PICK_FROM_ALBUM:

                if (resultCode == RESULT_OK){
                    _imageCaptureUri = data.getData();
                }

            case Constants.PICK_FROM_CAMERA:
            {
                try {

                    _photoPath = BitmapUtils.getRealPathFromURI(this, _imageCaptureUri);

                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(_imageCaptureUri, "image/*");

                    intent.putExtra("crop", true);
                    intent.putExtra("scale", true);
                    intent.putExtra("outputX", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("outputY", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("aspectX", 1);
                    intent.putExtra("aspectY", 1);
                    intent.putExtra("noFaceDetection", true);
                    //intent.putExtra("return-data", true);
                    intent.putExtra("output", Uri.fromFile(BitmapUtils.getOutputMediaFile(this)));

                    startActivityForResult(intent, Constants.CROP_FROM_CAMERA);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            }
        }
    }

}
