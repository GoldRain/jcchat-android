package com.funlab.jcchat.chatting;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.funlab.jcchat.JCChatApplication;
import com.funlab.jcchat.R;
import com.funlab.jcchat.RestartActivity;
import com.funlab.jcchat.Utils.Database;
import com.funlab.jcchat.commons.Commons;
import com.funlab.jcchat.commons.Constants;
import com.funlab.jcchat.commons.ReqConst;
import com.funlab.jcchat.logger.Logger;
import com.funlab.jcchat.main.ChatListActivity;
import com.funlab.jcchat.model.FriendEntity;
import com.funlab.jcchat.model.RoomEntity;
import com.funlab.jcchat.preference.PrefConst;
import com.funlab.jcchat.preference.Preference;

import org.jivesoftware.smack.chat.Chat;
import org.jivesoftware.smack.chat.ChatMessageListener;
import org.jivesoftware.smack.packet.Message;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/*
* MyChatMessageListener directs the incoming messages to the appropriate container.
* In this case, messages are contained in the ChatList
* */
public class MyChatMessageListener implements ChatMessageListener {

    private int _senderIdx = 0;

    @Override
    public void processMessage(Chat chat, Message message) {

        String mChatSender = message.getFrom();
        _senderIdx = getIdx(mChatSender);

        String mChatMessage = message.getBody();

        // if sender is block user
        if (Database.isBlocked(_senderIdx))
            return;

        Logger.d("XMPP", "sender = " + _senderIdx +  " msg = " + mChatMessage);

        // write message into db
        GroupChatItem chatItem = new GroupChatItem(_senderIdx, getRoomName(mChatMessage), mChatMessage);
        Database.createMessage(chatItem);

        // if app is running
        if (Commons.g_isAppRunning) {

            RoomEntity room = Commons.g_user.getRoom(getRoomName(mChatMessage));

            // not receive from block friend and not group chatting
            if (Commons.g_user.isBlockFriend(_senderIdx) && !room.isGroup())
                return;

            if (Commons.g_isAppPaused) {
                notifyNewMessage(mChatMessage);

            } else {

                if (Commons.g_currentActivity != null)
                    Commons.g_currentActivity.vibrate();
            }

            // if room not exist create room
            if (room == null || !room.get_participants().equals(getRoomParticipants(mChatMessage))) {
                getRoomInfo(mChatMessage);
            } else {

                room.set_recentContent(getMessage(mChatMessage));
                room.set_recentTime(getDisplayTime(mChatMessage));
                room.set_ownerIdx(_senderIdx);
                room.add_rcentCounter();
                Database.updateRoom(room);

                if (Commons.g_currentActivity != null) {

                    // room recent info update
                    if (Commons.g_currentActivity.getClass().equals(ChatListActivity.class)) {
                        Commons.g_currentActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ((ChatListActivity) Commons.g_currentActivity).refresh();
                            }
                        });
                    }

                    Commons.g_currentActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Commons.g_currentActivity.setUnRead();
                        }
                    });
                }

            }

        } else {
            notifyNewMessage(mChatMessage);
        }

    }

    // ROOM#10_12_13:10_12_13_14:에스오#this is room outside message body#time
    public String getRoomName(String message) {

        if (!message.startsWith(Constants.KEY_ROOM_MARKER))
            return null;

        return message.split(Constants.KEY_SEPERATOR)[1].split(":")[0];

    }

    // ROOM#10_12_13:10_12_13_14:에스오#this is room outside message body#time
    public String getRoomParticipants(String message) {

        if (!message.startsWith(Constants.KEY_ROOM_MARKER))
            return null;

        return message.split(Constants.KEY_SEPERATOR)[1].split(":")[1];

    }

    // ROOM#10_12_13:10_12_13_14:에스오#this is room outside message body#time
    public String getSenderName(String message) {

        if (!message.startsWith(Constants.KEY_ROOM_MARKER))
            return null;

        return message.split(Constants.KEY_SEPERATOR)[1].split(":")[2];

    }

    public String getDisplayTime(String body) {

        String fulldatetime = body.substring(body.lastIndexOf(Constants.KEY_SEPERATOR) + 1);

        String date = fulldatetime.split(",")[0];
        String fulltime = fulldatetime.split(",")[1];

        String time = fulltime.substring(0, fulltime.lastIndexOf(":"));

        int hour = Integer.valueOf(time.split(":")[0]);
        String min = time.split(":")[1];

        if (Commons.g_xmppService != null) {

            if (hour < 12) {
                time = Commons.g_xmppService.getString(R.string.am) + " " + time;
            } else {
                hour -= 12;
                if (hour == 0)
                    hour = 12;
                time = Commons.g_xmppService.getString(R.string.pm) + " " + hour + ":" + min;
            }
        }

        return time;
    }

    // ROOM#1_2#message#time, ROOM#1_2#FILE#message#time
    public String getMessage(String body) {

        String body1 = body.substring(body.indexOf(Constants.KEY_SEPERATOR) + 1, body.lastIndexOf(Constants.KEY_SEPERATOR));
        String message = body1.substring(body1.indexOf(Constants.KEY_SEPERATOR) + 1);

        if (getType(body) != GroupChatItem.ChatType.TEXT) {
            message = Commons.g_xmppService.getString(R.string.transfer_file);
        }

        return message;
    }

    public GroupChatItem.ChatType getType(String body) {

        String body1 = body.substring(body.indexOf(Constants.KEY_SEPERATOR) + 1);
        body1 = body1.substring(body1.indexOf(Constants.KEY_SEPERATOR) + 1);
        GroupChatItem.ChatType type = GroupChatItem.ChatType.TEXT;

        if (body1.startsWith(Constants.KEY_FILE_MARKER))
            type = GroupChatItem.ChatType.FILE;
        else if (body1.startsWith(Constants.KEY_IMAGE_MARKER))
            type = GroupChatItem.ChatType.IMAGE;
        else if (body1.startsWith(Constants.KEY_VIDEO_MARKER))
            type = GroupChatItem.ChatType.VIDEO;

        return type;
    }

    public void getRoomInfo(final String message){

        final String roomName = getRoomParticipants(message);

        String url = ReqConst.SERVER_URL + ReqConst.REQ_GETROOMINFO;

        String params = String.format("/%s", roomName);
        url += params;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                parseRoomInfoResponse(json, message);
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        JCChatApplication.getInstance().addToRequestQueue(stringRequest, url);

    }

    public void parseRoomInfoResponse(String json, String message){

        try {

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                JSONArray friends = response.getJSONArray(ReqConst.RES_USERLIST);

                ArrayList<FriendEntity> participants = new ArrayList<FriendEntity>();

                // get participants data
                for (int i = 0; i < friends.length(); i++) {

                    JSONObject friend = (JSONObject) friends.get(i);
                    FriendEntity entity = new FriendEntity();

                    if (friend.getInt(ReqConst.RES_ID) == Commons.g_user.get_idx())
                        continue;

                    entity.set_idx(friend.getInt(ReqConst.RES_ID));
                    entity.set_name(friend.getString(ReqConst.RES_NAME));
                    entity.set_label(friend.getString(ReqConst.RES_LABEL));
                    entity.set_photoUrl(friend.getString(ReqConst.RES_PHOTO_URL));
                    entity.set_bgUrl(friend.getString(ReqConst.RES_BG_URL));

                    participants.add(entity);
                }

                // make room
                RoomEntity roomEntity = new RoomEntity(getRoomName(message));
                roomEntity.set_participantList(participants);
                roomEntity.set_participants(roomEntity.makeRoomName());
                roomEntity.set_recentContent(getMessage(message));
                roomEntity.set_recentTime(getDisplayTime(message));
                roomEntity.add_rcentCounter();
                roomEntity.set_ownerIdx(_senderIdx);

                if (!Commons.g_user.get_roomList().contains(roomEntity)) {
                    Commons.g_user.get_roomList().add(roomEntity);
                    Database.createRoom(roomEntity);
                }else {
                    RoomEntity oldRoom = Commons.g_user.getRoom(roomEntity.get_name());
                    oldRoom.set_participants(roomEntity.get_participants());
                    oldRoom.set_participantList(roomEntity.get_participantList());
                    oldRoom.set_recentContent(roomEntity.get_recentContent());
                    oldRoom.set_recentTime(roomEntity.get_recentTime());
                    oldRoom.set_ownerIdx(roomEntity.get_ownerIdx());
                    oldRoom.add_rcentCounter();

                    Database.updateRoom(oldRoom);
                }

                if (Commons.g_currentActivity != null) {

                    // refresh chat room list
                    if (Commons.g_currentActivity.getClass().equals(ChatListActivity.class)) {
                        Commons.g_currentActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ((ChatListActivity) Commons.g_currentActivity).refresh();
                            }
                        });
                    }

                    Commons.g_currentActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Commons.g_currentActivity.setUnRead();
                        }
                    });
                }
            }

            if (Commons.g_chattingActivity != null) {
                Commons.g_chattingActivity.updateRoom();
            }

        } catch (JSONException e){
            e.printStackTrace();
        }

    }

    private void notifyNewMessage(String message) {

        boolean pushSetting = Preference.getInstance().getValue(Commons.g_xmppService, PrefConst.PREFKEY_PUSH, true);

        if (!pushSetting)
            return;

        if (Commons.g_xmppService != null)
            Commons.g_xmppService.updateBadgeCount(Commons.g_badgCount + 1);

        // create room
        RoomEntity room = new RoomEntity(getRoomName(message), getRoomParticipants(message), getMessage(message), getDisplayTime(message), Commons.g_badgCount, _senderIdx);

        if (Database.isExistRoom(room))
            Database.updateRoom(room);
        else
            Database.createRoom(room);

        Intent notiIntent = new Intent(Commons.g_xmppService, RestartActivity.class);
        notiIntent.putExtra(Constants.KEY_ROOM, getRoomName(message));
        notiIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(Commons.g_xmppService, 1, notiIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Notification.Builder builder = new Notification.Builder(Commons.g_xmppService);

        // noti small icon
        builder.setSmallIcon(R.drawable.ic_launcher);
        // noti preview
        builder.setTicker(getSenderName(message) + " : " + getMessage(message));
        // noti time
        builder.setWhen(System.currentTimeMillis());
        // noti title
        builder.setContentTitle(getTitleString());
        // content
        builder.setContentText(getSenderName(message) + " : " + getMessage(message));
        // action on touch
        builder.setContentIntent(pendingIntent);
        // auto cancel
        builder.setAutoCancel(true);

        builder.setOngoing(true);

        // large icon
        builder.setLargeIcon(BitmapFactory.decodeResource(Commons.g_xmppService.getResources(),
                R.drawable.ic_launcher));
        // sound
        builder.setDefaults(Notification.DEFAULT_SOUND);
        // vibration
        builder.setDefaults(Notification.DEFAULT_VIBRATE);

        // create noti
        NotificationManager nm = (NotificationManager) Commons.g_xmppService.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(Constants.NORMAL_NOTI_ID, builder.build());

    }

    public String getTitleString() {

        String ret = "[" + Commons.g_xmppService.getString(R.string.app_name) + "]";
        return ret;
    }

    public int getIdx(String name) {
        return Integer.valueOf(name.split("@")[0]);
    }

}
