package com.funlab.jcchat.main;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.funlab.jcchat.Base.CommonActivity;
import com.funlab.jcchat.JCChatApplication;
import com.funlab.jcchat.R;
import com.funlab.jcchat.Utils.BitmapUtils;
import com.funlab.jcchat.Utils.MultiPartRequest;
import com.funlab.jcchat.Utils.PhotoSelectDialog;
import com.funlab.jcchat.Utils.RadiusImageView;
import com.funlab.jcchat.commons.Constants;
import com.funlab.jcchat.commons.ReqConst;
import com.funlab.jcchat.preference.PrefConst;
import com.funlab.jcchat.preference.Preference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends CommonActivity implements View.OnClickListener{

    private int _idx = 0;

    private Uri _imageCaptureUri;
    private String _photoPath = "";

    private boolean _isVerified = false;

    private EditText ui_edtName, ui_edtEmail, ui_edtPwd, ui_edtConfirm;
    private TextView ui_btnVerification;
    private ImageView ui_imvRegPhoto;

    private ImageView ui_imvAgreeEula;
    private ImageView ui_imvAgreePrivacy;

    private PhotoSelectDialog _photoSelectDialog;

    private ArrayList<String> _contacts = new ArrayList<String>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        loadLayout();
    }

    public void loadLayout(){

        ui_edtName = (EditText)findViewById(R.id.edt_name);
        ui_edtEmail = (EditText)findViewById(R.id.edt_email);
        ui_edtPwd = (EditText)findViewById(R.id.edt_password);
        ui_edtConfirm = (EditText)findViewById(R.id.edt_confirm);

        ui_btnVerification = (TextView)findViewById(R.id.btn_verification);
        ui_btnVerification.setOnClickListener(this);

        ui_imvRegPhoto = (RadiusImageView)findViewById(R.id.imv_regPhoto);
        ui_imvRegPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                View.OnClickListener cameraListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        doTakePhotoAction();
                        _photoSelectDialog.dismiss();
                    }
                };
                View.OnClickListener albumListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        doTakeGallery();
                        _photoSelectDialog.dismiss();
                    }
                };

                View.OnClickListener deleteListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deletePhoto();
                        _photoSelectDialog.dismiss();
                    }
                };
                View.OnClickListener cancelListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        _photoSelectDialog.dismiss();
                    }
                };

                if (_photoPath.length() == 0)
                    _photoSelectDialog = new PhotoSelectDialog(_context, cameraListener, albumListener, cancelListener);
                else
                    _photoSelectDialog = new PhotoSelectDialog(_context, cameraListener, albumListener, deleteListener, cancelListener);

                _photoSelectDialog.show();
            }
        });

        Button btnRegister = (Button)findViewById(R.id.btn_register);
        btnRegister.setOnClickListener(this);

        Button btnCancel = (Button)findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(this);

        ui_imvAgreeEula = (ImageView) findViewById(R.id.imv_agree_eula);
        ui_imvAgreeEula.setOnClickListener(this);

        TextView txvViewEula = (TextView) findViewById(R.id.txv_view_eula);
        txvViewEula.setOnClickListener(this);

        ui_imvAgreePrivacy = (ImageView) findViewById(R.id.imv_agree_privacy);
        ui_imvAgreePrivacy.setOnClickListener(this);

        TextView txvViewPrivacy = (TextView) findViewById(R.id.txv_view_privacy);
        txvViewPrivacy.setOnClickListener(this);
    }

    public boolean checkValid(){

        if(ui_edtName.getText().toString().trim().length() == 0){
            showAlertDialog(getString(R.string.inputName));
            return false;
        }else if(ui_edtEmail.getText().toString().trim().length() == 0){
            showAlertDialog(getString(R.string.inputEmail));
            return false;
        }else if(!_isVerified){
            showAlertDialog(getString(R.string.getVerificationNum));
            return false;
        } else if(ui_edtPwd.getText().toString().trim().length() == 0){
            showAlertDialog(getString(R.string.inputPassword));
            return false;
        } else if(ui_edtPwd.getText().toString().trim().length() < 6){
            showAlertDialog(getString(R.string.PwdLength));
            return false;
        } else if(ui_edtConfirm.getText().toString().trim().length() == 0){
            showAlertDialog(getString(R.string.inputPwdConfirm));
            return false;
        }else if(!ui_edtPwd.getText().toString().equals(ui_edtConfirm.getText().toString())){
            showAlertDialog(getString(R.string.checkPwd));
            return false;
        } else if (!ui_imvAgreeEula.isSelected()){
            showAlertDialog(getString(R.string.agree_eula_confirm));
            return false;
        } else if (!ui_imvAgreePrivacy.isSelected()){
            showAlertDialog(getString(R.string.agree_privacy_confirm));
        return false;
    }
        return true;
    }

    public boolean isValidEmail(){

        if(ui_edtName.getText().length() == 0){
            showAlertDialog(getString(R.string.inputName));
            return false;
        }else if(ui_edtEmail.getText().length() == 0){
            showAlertDialog(getString(R.string.inputEmail));
            return false;
        }
        return true;
    }

    public void gotoViewEula() {

        Intent intent = new Intent(RegisterActivity.this, ReadUseActivity.class);
        startActivity(intent);
    }


    public void gotoViewPrivacy() {

        Intent intent = new Intent(RegisterActivity.this, ReadPersonalActivity.class);
        startActivity(intent);

    }

    public void onClick(View view){

        switch (view.getId()){
            case R.id.btn_register:

               if(checkValid()){
                    processRegister();
                }
                break;

            case R.id.btn_cancel:
                gotoCancel();
                break;

            case R.id.btn_verification:

                if (isValidEmail()){
                    createAuthCode();
                }
                break;

            case R.id.imv_agree_eula:
            case R.id.imv_agree_privacy:
                view.setSelected(!view.isSelected());
                break;

            case R.id.txv_view_eula:
                gotoViewEula();
                break;

            case R.id.txv_view_privacy:
                gotoViewPrivacy();
                break;
        }
    }

    public void deletePhoto() {

        _photoPath = "";
        ui_imvRegPhoto.setImageDrawable(null);
    }

 /*Get AuthCode  for user email from server*/
    public void createAuthCode(){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_CREATEAUTHCODE;
        String params = String.format("/%s", ui_edtEmail.getText().toString().trim());
        url += params;

        Preference.getInstance().put(this, PrefConst.PREFKEY_USEREMAIL, ui_edtEmail.getText().toString());

        showProgress();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>(){

            @Override
            public void onResponse(String json) {
                parseCreateAuthCodeResponse(json);
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        JCChatApplication.getInstance().addToRequestQueue(stringRequest, url);

    }

    public void parseCreateAuthCodeResponse(String json){

        closeProgress();

        try{

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if(result_code == ReqConst.CODE_SUCCESS){
                gotoRegVerificationActivity();
            } else if(result_code == ReqConst.CODE_AUTHFAIL){
                showToast(getString(R.string.email_exist));
            }

        }catch (JSONException e){

            e.printStackTrace();
            showAlertDialog(getString(R.string.error));
        }
    }

/*Register the user information to server*/
    public void processRegister() {

        String phoneNumber = getPhoneNumber(this);

        String url = ReqConst.SERVER_URL + ReqConst.REQ_SINGUP;

        try {
            String name = ui_edtName.getText().toString().trim().replace(" ", "%20");
            name = URLEncoder.encode(name, "utf-8");
            String params = String.format("/%s/%s/%s/%s/%d", name, ui_edtEmail.getText().toString().trim(),
                    ui_edtPwd.getText().toString().trim(), phoneNumber, Constants.ANDROID);

            url += params;
        } catch (Exception ex) {
            return;
        }

        showProgress();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url , new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {

                parseRegisterResponse(json);
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        JCChatApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    public void parseRegisterResponse(String json){

        try{
            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if(result_code == ReqConst.CODE_SUCCESS){

                _idx = response.getInt(ReqConst.RES_IDX);

                uploadImage();

            } else if (result_code == ReqConst.CODE_INVALIDEMAIL){

                closeProgress();
                showAlertDialog(getString(R.string.invalidEmail));

            } else if (result_code == ReqConst.CODE_UNAUTHEMAIL){

                closeProgress();
                showAlertDialog(getString(R.string.not_authenticate));
            } else {
                closeProgress();
                showAlertDialog(getString(R.string.register_fail));
            }

        } catch (JSONException e){
            closeProgress();
            showAlertDialog(getString(R.string.register_fail));

            e.printStackTrace();
        }

    }

/*Upload the userPhoto to server*/
    public void uploadImage() {

        //if no profile photo
        if (_photoPath.length() == 0) {

            onSuccessRegister();
            return;
        }

        try {

            File file = new File(_photoPath);

            Map<String, String> params = new HashMap<String, String>();
            params.put(ReqConst.PARAM_ID, String.valueOf(_idx));
            params.put(ReqConst.PARAM_IMAGE_TYPE, String.valueOf(0));

            String url = ReqConst.SERVER_URL + ReqConst.REQ_UPLOADIMAGE;

            MultiPartRequest reqMultiPart = new MultiPartRequest(url, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    showToast(getString(R.string.photo_upload_fail));
                    onSuccessRegister();
                }
            }, new Response.Listener<String>() {

                @Override
                public void onResponse(String json) {

                    ParseUploadImgResponse(json);
                }
            }, file, ReqConst.PARAM_FILE, params);

            reqMultiPart.setRetryPolicy(new DefaultRetryPolicy(
                    Constants.VOLLEY_TIME_OUT, 0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            JCChatApplication.getInstance().addToRequestQueue(reqMultiPart, url);

        } catch (Exception e) {

            e.printStackTrace();

            showToast(getString(R.string.photo_upload_fail));
            onSuccessRegister();
        }
    }

    public void ParseUploadImgResponse(String json){

        try{
            JSONObject response = new JSONObject(json);
            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == 0){

            }
            else if(result_code == 111){
                showToast(getString(R.string.photo_upload_fail));
            }
        }catch (JSONException e){
            e.printStackTrace();
        }

        onSuccessRegister();
    }

    public static String getPhoneNumber(Context p_context) {

        String w_strPhoneNumber = "none";
        TelephonyManager w_tm = (TelephonyManager) p_context.getSystemService(Context.TELEPHONY_SERVICE);
        if (w_tm.getDeviceId() != null && w_tm.getLine1Number() != null) {
            w_strPhoneNumber = w_tm.getLine1Number();
            w_strPhoneNumber = w_strPhoneNumber.replace("+", "0");
        }
        if (w_strPhoneNumber.length() == 0)
            w_strPhoneNumber = "none";

        return w_strPhoneNumber;
    }

    public void gotoCancel() {
        finish();
    }

    public void onSuccessRegister() {
        //addContacts();
        onCompleted();
    }


    public void onCompleted() {

        closeProgress();

        Preference.getInstance().put(this,
                PrefConst.PREFKEY_USEREMAIL, ui_edtEmail.getText().toString().trim());
        Preference.getInstance().put(this,
                PrefConst.PREFKEY_USERPWD, ui_edtPwd.getText().toString().trim());

        setResult(RESULT_OK);
        finish();
    }

    public void addContacts() {

        _contacts = getContactList();

        if (_contacts.size() > 0) {
            addFriendByPhonenumber();
        } else {
            onCompleted();
        }
    }

    public void addFriendByPhonenumber() {

        String url = ReqConst.SERVER_URL + ReqConst.REQ_ADDFRIENDBYPHONENUMBER;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url , new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {

                parseAddFriendResponse(json);
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                    onCompleted();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();

                try {
                    params.put(ReqConst.PARAM_ID, String.valueOf(_idx));

                    JSONArray phonenumbers = new JSONArray();

                    for (String contact : _contacts) {
                        phonenumbers.put(contact);
                    }

                    params.put(ReqConst.PARAM_PHONE_NUMBERS, phonenumbers.toString());

                } catch (Exception e) {
                }

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        JCChatApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    public void parseAddFriendResponse(String json){


        try{
            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if(result_code == ReqConst.CODE_SUCCESS) {

            }

        } catch (JSONException e){
            e.printStackTrace();
        }

        onCompleted();

    }


    public ArrayList<String> getContactList() {

        ArrayList<String> contactList = new ArrayList<String>();

        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;

        String[] projection = new String[] {
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
                ContactsContract.CommonDataKinds.Phone.NUMBER,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME };

        String[] selectionArgs = null;

        String sortOrder = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME
                + " COLLATE LOCALIZED ASC";

        ContentResolver cr = getContentResolver();

        Cursor contactCursor = cr.query(uri, projection, null, selectionArgs,
                sortOrder);

        if (contactCursor.moveToFirst()) {

            do {
                String phonenumber = contactCursor.getString(1).replaceAll("-", "");
                phonenumber = phonenumber.replace("+", "0");

                if (phonenumber.length() > Constants.PHONENUMBER_LENGTH)
                    phonenumber = phonenumber.substring(phonenumber.length() - Constants.PHONENUMBER_LENGTH);

                contactList.add(phonenumber);

            } while (contactCursor.moveToNext());
        }

        contactCursor.close();

        return contactList;
    }


/*Go to the Verification*/
    public void gotoRegVerificationActivity(){

        Intent intent = new Intent(this, RegVerificationActivity.class);
        intent.putExtra(Constants.KEY_EMAIL, ui_edtEmail.getText().toString().trim());
        startActivityForResult(intent, Constants.EMAIL_VERIFICATION);
    }

    public void doTakePhotoAction(){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String picturePath = BitmapUtils.getTempFolderPath() + "photo_temp.jpg";
        _imageCaptureUri = Uri.fromFile(new File(picturePath));

        intent.putExtra(MediaStore.EXTRA_OUTPUT, _imageCaptureUri);
        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);

    }

    private void doTakeGallery(){
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, Constants.PICK_FROM_ALBUM);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){

        switch (requestCode){

            case Constants.CROP_FROM_CAMERA: {

                if (resultCode == RESULT_OK){
                    try {

                        File saveFile = BitmapUtils.getOutputMediaFile(this);

                        InputStream in = getContentResolver().openInputStream(Uri.fromFile(saveFile));
                        BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);

                        in.close();

                        //set The bitmap data to image View
                        ui_imvRegPhoto.setImageBitmap(bitmap);
                        _photoPath = saveFile.getAbsolutePath();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
            case Constants.PICK_FROM_ALBUM:

                if (resultCode == RESULT_OK){
                    _imageCaptureUri = data.getData();
                }

            case Constants.PICK_FROM_CAMERA:
            {
                try {

                    _photoPath = BitmapUtils.getRealPathFromURI(this, _imageCaptureUri);

                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(_imageCaptureUri, "image/*");

                    intent.putExtra("crop", true);
                    intent.putExtra("scale", true);
                    intent.putExtra("outputX", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("outputY", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("aspectX", 1);
                    intent.putExtra("aspectY", 1);
                    intent.putExtra("noFaceDetection", true);
                    //intent.putExtra("return-data", true);
                    intent.putExtra("output", Uri.fromFile(BitmapUtils.getOutputMediaFile(this)));

                    startActivityForResult(intent, Constants.CROP_FROM_CAMERA);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            }

            case Constants.EMAIL_VERIFICATION: {

                if (resultCode == RESULT_OK) {
                    _isVerified = true;
                }
                break;
            }
        }
    }
}
