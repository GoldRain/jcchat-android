package com.funlab.jcchat.main;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.funlab.jcchat.Base.CommonActivity;
import com.funlab.jcchat.JCChatApplication;
import com.funlab.jcchat.R;
import com.funlab.jcchat.Utils.CirculaireNetworkImageView;
import com.funlab.jcchat.Utils.Database;
import com.funlab.jcchat.Utils.RectNetImageView;
import com.funlab.jcchat.adapter.FriendMomentListAdapter;
import com.funlab.jcchat.chatting.GroupChattingActivity;
import com.funlab.jcchat.commons.Commons;
import com.funlab.jcchat.commons.Constants;
import com.funlab.jcchat.commons.ReqConst;
import com.funlab.jcchat.model.FriendEntity;
import com.funlab.jcchat.model.ImageDataEntity;
import com.funlab.jcchat.model.MomentEntity;
import com.funlab.jcchat.model.RoomEntity;
import com.funlab.jcchat.model.UserEntity;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FriendProfileActivity extends CommonActivity implements View.OnClickListener {

    private TextView ui_txvStateMessage;
    RectNetImageView ui_imvBack;
    CirculaireNetworkImageView ui_imvProfile;

    private FriendEntity _friend ;
    UserEntity _user;

    private ImageLoader _imageLoader;

    ExpandableHeightListView listcontainer;
    FriendMomentListAdapter _adapter ;
  //  ArrayList<String>urls=new ArrayList<>();
    ArrayList<MomentEntity>_moment_info = new ArrayList<>();
    ArrayList<ImageDataEntity> _images_data = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_profile);

        _user = Commons.g_user;
        FriendEntity friend = (FriendEntity) getIntent().getSerializableExtra(Constants.KEY_FRIEND);
        _friend = _user.getFriend(friend.get_idx());

        _imageLoader = JCChatApplication.getInstance().getImageLoader();

        loadLayout();

        getFriendInfo();

    }


    public void loadLayout(){

        ImageView imvClose = (ImageView)findViewById(R.id.imv_profile_close);
        imvClose.setOnClickListener(this);

        TextView txvChatting = (TextView)findViewById(R.id.btn_chatting);
        txvChatting.setOnClickListener(this);

        ui_txvStateMessage = (TextView) findViewById(R.id.txv_profile_title);
        ui_txvStateMessage.setText(_friend.get_label());

        ui_imvBack = (RectNetImageView) findViewById(R.id.imv_bg);
        ui_imvBack.setImageUrl(_friend.get_bgUrl(), _imageLoader);
        ui_imvBack.setOnClickListener(this);

        ui_imvProfile = (CirculaireNetworkImageView) findViewById(R.id.imv_friend_proPhoto);
        ui_imvProfile.setImageUrl(_friend.get_photoUrl(), _imageLoader);
        ui_imvProfile.setOnClickListener(this);

        listcontainer = (ExpandableHeightListView)findViewById(R.id.lst_friend);
        listcontainer.setExpanded(true);

        getFriendMomentInfo();
    }

    public void updateFriendInfo() {

        ui_txvStateMessage.setText(_friend.get_label());
        ui_imvBack.setImageUrl(_friend.get_bgUrl(), _imageLoader);
        ui_imvProfile.setImageUrl(_friend.get_photoUrl(), _imageLoader);
    }

    public void getFriendInfo() {

        String url = ReqConst.SERVER_URL + ReqConst.REQ_GETUSERINFO;

        String params = String.format("/%s", _friend.get_idx());
        url += params;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {

                parseFriendResponse(json);
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        JCChatApplication.getInstance().addToRequestQueue(stringRequest, url);
    }


    public void parseFriendResponse(String json){

        try {

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                _friend.set_name(response.getString(ReqConst.RES_NAME));
                _friend.set_label(response.getString(ReqConst.RES_LABEL));
                _friend.set_photoUrl(response.getString(ReqConst.RES_PHOTO_URL));
                _friend.set_bgUrl(response.getString(ReqConst.RES_BG_URL));

                updateFriendInfo();
            }

        }catch (JSONException e){
            e.printStackTrace();
        }

    }

    public void getFriendMomentInfo(){


        String url = "http://35.163.255.175/index.php/Api/getMomentInfo";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                parseMomentInfo(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        })
        {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    //params.put("user_id", String.valueOf(_friend.get_idx()));
                    params.put("user_id", String.valueOf(_friend.get_idx()));

                } catch (Exception e) {

                    closeProgress();
                    showAlertDialog(getString(R.string.error));
                }
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        JCChatApplication.getInstance().addToRequestQueue(request, url);

    }

    public void parseMomentInfo(String response){

        Log.d("response-->", response);

        closeProgress();

        try {
            JSONObject object = new JSONObject(response);
            int result_code = object.getInt(ReqConst.RES_CODE);

            if (result_code == 0){

                JSONArray moment_info = object.getJSONArray("Moment");

                for(int i = 0; i < moment_info.length(); i++){

                    MomentEntity moment_entity = new MomentEntity();

                    JSONObject jsonObject = moment_info.getJSONObject(i);

                    JSONObject moment_data = jsonObject.getJSONObject("moment_data");

                    moment_entity.set_date(moment_data.getString("post_date"));
                    moment_entity.set_time(moment_data.getString("post_time"));
                    moment_entity.set_comment(moment_data.getString("mo_comment"));

                    JSONArray json_images_data = jsonObject.getJSONArray("images_data");


                    ArrayList<ImageDataEntity> imageDataEntities = new ArrayList<>();

                    for (int j = 0; j < json_images_data.length(); j++){

                        JSONObject jsonObject1 = json_images_data.getJSONObject(j);

                        ImageDataEntity images_data = new ImageDataEntity();

                        images_data.set_videoUrl(jsonObject1.getString("mo_video"));
                        images_data.set_photos(jsonObject1.getString("mo_image"));

                        imageDataEntities.add(images_data);
                    }

                    moment_entity.set_images_data(imageDataEntities);

                    _moment_info.add(moment_entity);

                }

                _adapter = new FriendMomentListAdapter(this, _moment_info);
                listcontainer.setAdapter(_adapter);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public RoomEntity makeRoom() {

        ArrayList<FriendEntity> participants = new ArrayList<FriendEntity>();
        participants.add(_friend);

        RoomEntity room = new RoomEntity(participants);
        room.set_ownerIdx(_user.get_idx());

        if (!_user.get_roomList().contains(room)) {
            _user.get_roomList().add(room);
            Database.createRoom(room);
        }

        return room;
    }

    public void showProfilePhoto(){

        if (_friend.get_photoUrl().length() > 0) {

            MimeTypeMap myMime = MimeTypeMap.getSingleton();
            Intent newIntent = new Intent(android.content.Intent.ACTION_VIEW);
            String ext = Commons.fileExtFromUrl(_friend.get_photoUrl()).substring(1);
            String mimeType = myMime.getMimeTypeFromExtension(ext);
            newIntent.setDataAndType(Uri.parse(_friend.get_photoUrl()), mimeType);

            try {
                startActivity(newIntent);
            } catch (android.content.ActivityNotFoundException e) {
                showToast(getString(R.string.not_support_file));
            }
        }
    }

    public void showBackgroundPhoto(){

        if (_friend.get_bgUrl().length() > 0) {

            MimeTypeMap myMime = MimeTypeMap.getSingleton();
            Intent newIntent = new Intent(android.content.Intent.ACTION_VIEW);
            String ext = Commons.fileExtFromUrl(_friend.get_bgUrl()).substring(1);
            String mimeType = myMime.getMimeTypeFromExtension(ext);
            newIntent.setDataAndType(Uri.parse(_friend.get_bgUrl()), mimeType);

            try {
                startActivity(newIntent);
            } catch (android.content.ActivityNotFoundException e) {
                showToast(getString(R.string.not_support_file));
            }
        }

    }

    public void onClick(View view){

        switch (view.getId()){

            case R.id.imv_profile_close:
                finish();
                break;
            case R.id.btn_chatting:
                gotoChattingActivity();
                break;
            case R.id.imv_bg:
                showBackgroundPhoto();
                break;

            case R.id.imv_friend_proPhoto:
                showProfilePhoto();
                break;
        }
    }

    public void gotoChattingActivity(){

        RoomEntity room = makeRoom();

        Intent intent = new Intent(_context, GroupChattingActivity.class);
        intent.putExtra(Constants.KEY_ROOM, room.get_name());
        startActivity(intent);

        finish();
    }
}
