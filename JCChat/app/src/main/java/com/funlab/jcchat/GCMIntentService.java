package com.funlab.jcchat;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gcm.GCMBaseIntentService;
import com.funlab.jcchat.commons.Commons;
import com.funlab.jcchat.commons.Constants;
import com.funlab.jcchat.commons.ReqConst;
import com.funlab.jcchat.preference.PrefConst;
import com.funlab.jcchat.preference.Preference;

import org.json.JSONException;
import org.json.JSONObject;

public class GCMIntentService extends GCMBaseIntentService {

    private final String TAG = "GCMIntentService";

    public static final String GCM_PROJECT_ID = "270060482844";
    public static final int GCM_NOTIFICATION_ID = 1;


    /**
     * public 기본 생성자를 무조건 만들어야 한다.
     */
    public GCMIntentService() {
        this(GCM_PROJECT_ID);
    }

    public GCMIntentService(String projectId) {
        super(projectId);
    }

    /**
     * 푸시로 받은 메시지
     */
    @Override
    protected void onMessage(Context context, Intent intent) {

        if (intent.getAction().equals("com.google.android.c2dm.intent.RECEIVE")) {
            try {
                showMessage(context, intent);
            } catch (NullPointerException e) {

            } catch (Exception e) {

            }
        }
    }

    @Override
    protected void onError(Context context, String msg) {
        Log.w(TAG, "onError!! " + msg);
    }

    /**
     * 단말에서 GCM 서비스 등록 했을 때 등록 id를 받는다
     */
    @Override
    protected void onRegistered(Context context, String regID) {
        Log.d(TAG, "onRegistered. regId : " + regID);

        if (regID != null && !regID.equals("")) {
            JCChatApplication w_app = (JCChatApplication) context.getApplicationContext();
            w_app.setGcmToken(regID);
            registerToken();
        }
    }

    /**
     * 단말에서 GCM 서비스 등록 해지를 하면 해지된 등록 id를 받는다
     */
    @Override
    protected void onUnregistered(Context context, String regID) {
        Log.w(TAG, "onUnregistered!!" + regID);
    }

    @SuppressWarnings("deprecation")
    @SuppressLint("InlinedApi")
    private void showMessage(Context context, Intent intent)
            throws NullPointerException {

        String w_strGCMMessage = intent.getStringExtra("message");
        notifyNewMessage(w_strGCMMessage);

    }

    public void registerToken() {

        String url = ReqConst.SERVER_URL + ReqConst.REQ_REGISTERTOKEN;

        String token = JCChatApplication.getInstance().getGcmToken();

        if (Commons.g_user == null)
            return;

        String params = String.format("/%d/%s", Commons.g_user.get_idx(), token);
        url += params;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {

                parseTokenResponse(json);
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        JCChatApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    public void parseTokenResponse(String json){

        try{

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

            }

        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    private void notifyNewMessage(String message) {

        boolean pushSetting = Preference.getInstance().getValue(this, PrefConst.PREFKEY_PUSH, true);

        if (!pushSetting)
            return;

        Intent notiIntent = new Intent(this, RestartActivity.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 1, notiIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Notification.Builder builder = new Notification.Builder(this);

        // noti small icon
        builder.setSmallIcon(R.drawable.ic_launcher);
        // noti preview
        builder.setTicker(message);
        // noti time
        builder.setWhen(System.currentTimeMillis());
        // noti title
        builder.setContentTitle(getTitleString(message));
        // content
        builder.setContentText(getMessageString(message));
        // action on touch
        builder.setContentIntent(pendingIntent);
        // auto cancel
        builder.setAutoCancel(true);

        builder.setOngoing(true);

        // large icon
        builder.setLargeIcon(BitmapFactory.decodeResource(getResources(),
                R.drawable.ic_launcher));
        // sound
        builder.setDefaults(Notification.DEFAULT_SOUND);
        // vibration
        builder.setDefaults(Notification.DEFAULT_VIBRATE);

        // create noti
        NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(Constants.PUSH_NOTI_ID, builder.build());

    }

    // [D-Chat 공지] content
    public String getTitleString(String message) {

        String ret = message.split("]")[0] + "]";
        return ret;
    }

    public String getMessageString(String message) {

        String ret = message.substring(message.indexOf("]") + 1);
        return ret;
    }

}