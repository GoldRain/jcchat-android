package com.funlab.jcchat.model;

import com.funlab.jcchat.commons.Commons;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by JIS on 12/21/2015.
 */
public class RoomEntity implements Serializable {

    String _name = "";
    String _participants = "";          // contains my id
    ArrayList<FriendEntity> _participantList = new ArrayList<FriendEntity>();       // not contains me

    String _recentContent = "";
    String _recentTime = "";
    int _recentCounter = 0;
    int _ownerIdx = 0;

    private boolean _isSelected = false;

    public RoomEntity(ArrayList<FriendEntity> participants) {

        _participantList = participants;
        _name = makeRoomName();
        _participants = makeRoomName();
    }

    public RoomEntity(String roomname) {

        _name = roomname;
    }

    public RoomEntity(String name, String participants, String recentContent, String recentTime, int recentCounter, int owner) {

        _name = name;
        _recentContent = recentContent;
        _recentTime = recentTime;
        _recentCounter = recentCounter;
        _ownerIdx = owner;
        _participants = participants;
    }


    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public String get_participants() {
        return _participants;
    }

    public void set_participants(String _participants) {
        this._participants = _participants;
    }

    public ArrayList<FriendEntity> get_participantList() {
        return _participantList;
    }

    public void set_participantList(ArrayList<FriendEntity> _participantList) {
        this._participantList = _participantList;
    }

    public String get_recentContent() {
        return _recentContent;
    }

    public void set_recentContent(String _recentContent) {
        this._recentContent = _recentContent;
    }

    public String get_recentTime() {
        return _recentTime;
    }

    public void set_recentTime(String _recentTime) {
        this._recentTime = _recentTime;
    }

    public int get_recentCounter() {
        return _recentCounter;
    }

    public void set_recentCounter(int _recentCounter) {
        this._recentCounter = _recentCounter;
    }

    public void add_rcentCounter() {
        set_recentCounter(_recentCounter + 1);
    }

    public void init_recentCounter() {
        set_recentCounter(0);
    }

    public boolean is_isSelected() {
        return _isSelected;
    }

    public void set_isSelected(boolean _isSelected) {
        this._isSelected = _isSelected;
    }

    public int get_ownerIdx() {
        return _ownerIdx;
    }

    public void set_ownerIdx(int _ownerIdx) {
        this._ownerIdx = _ownerIdx;
    }

    public String makeRoomName() {

        String roomName = "";

        ArrayList<Integer> ids = new ArrayList<Integer>();

        for (FriendEntity entity : _participantList) {
            ids.add(Integer.valueOf(entity.get_idx()));
        }
        ids.add(Integer.valueOf(Commons.g_user.get_idx()));

        Collections.sort(ids);

        for (Integer id : ids) {
            roomName += id + "_";
        }

        roomName = roomName.substring(0, roomName.length() - 1);

        return roomName;

    }


    public String get_displayName() {

        String displayName = "";

        ArrayList<Integer> ids = new ArrayList<Integer>();

        for (FriendEntity entity : _participantList) {
            ids.add(Integer.valueOf(entity.get_idx()));
        }
        Collections.sort(ids);

        for (Integer id : ids) {

            for (FriendEntity entity : _participantList) {

                if (id == entity.get_idx()) {
                    displayName += entity.get_name() + ", ";
                    break;
                }
            }

        }

        if (displayName.length() > 2)
            displayName = displayName.substring(0, displayName.length() - 2);

        return displayName;
    }

    public String get_displayCount() {

        String displayCount = "";

        if (_participantList.size() >= 2)
            displayCount += " (" + String.valueOf(_participantList.size() + 1) + ")";

        return displayCount;
    }

    public FriendEntity getParticipant(int idx) {

        for (FriendEntity friend : _participantList) {

            if (friend.get_idx() == idx)
                return friend;
        }

        return null;
    }


    @Override
    public boolean equals(Object o) {

        RoomEntity other = (RoomEntity) o;
        return (get_name().equalsIgnoreCase(other.get_name()));
    }


    public boolean isGroup() {

        if (get_participantList().size() > 1)
            return true;

        return false;
    }
}
