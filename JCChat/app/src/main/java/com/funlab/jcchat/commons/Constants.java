package com.funlab.jcchat.commons;

/**
 * Created by HGS on 12/11/2015.
 */
public class Constants {

    public static final int VOLLEY_TIME_OUT = 60000;

    public static final int LIMIT_FILE = 25 * 1024 * 1024;

    public static final int PROFILE_IMAGE_SIZE = 256;

    public static final int RECENT_MESSAGE_COUNT = 20;

    public static final int SPLASH_TIME = 2000;
    public static final int PICK_FROM_CAMERA = 100;
    public static final int PICK_FROM_ALBUM = 101;
    public static final int CROP_FROM_CAMERA = 102;
    public static final int EMAIL_VERIFICATION = 103;
    public static final int PICK_FROM_VIDEO = 104;
    public static final int PICK_FROM_FILE = 105;
    public static final int PICK_FROM_INVITE  = 106;

    public static final int MY_PEQUEST_CODE = 107;

    public static final int INPUT_STATE_MESSAGE = 200;
    public static final int INPUT_NAME = 201;

    public static final String KEY_ADDRESS = "address";
    public static final String KEY_ROOM = "room";
    public static final String KEY_ROOM_TITLE = "room_title";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_LOGOUT = "loguut";
    public static final String KEY_FRIEND = "friend";
    public static final String KEY_FILE = "file";
    public static final String KEY_PARTICIPANTS = "participants";
    public static final String KEY_FROMLOGIN = "fromlogin";

    public static final String KEY_ADD_BLOCK = "addOrblock";
    public static final String KEY_ADD = "add";
    public static final String KEY_BLOCK = "block";

    public static final String KEY_SEPERATOR = "#";
    public static final String KEY_FILE_MARKER = "FILE#";
    public static final String KEY_IMAGE_MARKER = "IMAGE#";
    public static final String KEY_VIDEO_MARKER = "VIDEO#";
    public static final String KEY_ROOM_MARKER = "ROOM#";

    public static final String KEY_NOTE = "note";

    public static final String XMPP_START = "xmpp";
    public static final int XMPP_FROMBROADCAST = 0;
    public static final int XMPP_FROMLOGIN = 1;

    public static final int NORMAL_NOTI_ID = 1;
    public static final int PUSH_NOTI_ID = 2;

    public static final String KEY_MESSAGE = "message";

    public static final int PHONENUMBER_LENGTH = 10;

    public static final int ANDROID = 0;

    public static final String KEY_IMAGEPATH = "image_path";
    public static final String KEY_POSITION = "position";
    public static final String KEY_COMMENT = "comment";
    public static final String KEY_VIDEOPATH = "video_path";

    public static final int REQUST_PERMISSION = 150;
    public static final String KEY_COUNT = "count";
    public static final int MAX_IMAGE_COUNT = 9;
    public static final int PICK_FROM_IMAGES = 116;
    public static final String KEY_IMAGES = "images";
    public static final String KEY_TIME = "time";

    public static int TAKE_VIDEO = 0;
    public static String VIDEO_PATH = "";

}
