package com.funlab.jcchat.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.funlab.jcchat.JCChatApplication;
import com.funlab.jcchat.R;
import com.funlab.jcchat.Utils.CirculaireNetworkImageView;
import com.funlab.jcchat.model.FriendEntity;

import java.util.ArrayList;

/**
 * Created by HGS on 12/13/2015.
 */
public class AddOrBlockFriendAdapter extends BaseAdapter{

    private Context _context = null;
    private ArrayList<FriendEntity> _friendDatas = new ArrayList<FriendEntity>();

    private ImageLoader _imageLoader;

    public AddOrBlockFriendAdapter(Context _context){

        super();
        this._context = _context;

        _imageLoader = JCChatApplication.getInstance().getImageLoader();
    }

    public void addItem(FriendEntity item) {
        _friendDatas.add(item);
    }

    public void removeItem(FriendEntity item) {
        _friendDatas.remove(item);
    }

    public void removeAll() {
        _friendDatas.clear();
    }

    public ArrayList<FriendEntity> getSelectedFriends() {

        ArrayList<FriendEntity> selected = new ArrayList<FriendEntity>();

        for (FriendEntity friendEntity : _friendDatas) {

            if (friendEntity.is_isSelected())
                selected.add(friendEntity);
        }

        return selected;

    }

    @Override
    public int getCount(){

        return _friendDatas.size();
    }

    @Override
    public Object getItem(int position){

        return _friendDatas.get(position);
    }

    @Override
    public long getItemId(int position){

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){

        FriendHolder friendHolder;

        if(convertView == null){
            friendHolder = new FriendHolder();

            LayoutInflater inflater = (LayoutInflater)_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.select_friend_list_item, null);

            friendHolder.imvSelectFriPhoto = (CirculaireNetworkImageView)convertView.findViewById(R.id.imv_selectFriPhoto);
            friendHolder.txvSelectFriName = (TextView)convertView.findViewById(R.id.txv_selectFriName);
            friendHolder.imvSelectFriState = (ImageView)convertView.findViewById(R.id.imv_selectFirState);
            friendHolder.imvSelectFriState.setSelected(false);

            convertView.setTag(friendHolder);
        } else {
            friendHolder = (FriendHolder)convertView.getTag();
        }

        final FriendEntity friend = _friendDatas.get(position);

        friendHolder.txvSelectFriName.setText(friend.get_name());

        if (friend.get_photoUrl().length() > 0)
            friendHolder.imvSelectFriPhoto.setImageUrl(friend.get_photoUrl(), _imageLoader);
        else
            friendHolder.imvSelectFriPhoto.setImageResource(R.drawable.bg_non_profile1);

        final ImageView imvState = friendHolder.imvSelectFriState;
        imvState.setSelected(friend.is_isSelected());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imvState.setSelected(!imvState.isSelected());
                friend.set_isSelected(imvState.isSelected());
            }
        });

        return convertView;
    }


    public class FriendHolder {

        public CirculaireNetworkImageView imvSelectFriPhoto;
        public TextView txvSelectFriName;
        public ImageView imvSelectFriState;
    }
}
