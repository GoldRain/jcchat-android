package com.funlab.jcchat.main;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.funlab.jcchat.Base.CommonActivity;
import com.funlab.jcchat.JCChatApplication;
import com.funlab.jcchat.R;
import com.funlab.jcchat.Utils.Database;
import com.funlab.jcchat.adapter.EditFriendAdapter;
import com.funlab.jcchat.commons.Commons;
import com.funlab.jcchat.commons.Constants;
import com.funlab.jcchat.commons.ReqConst;
import com.funlab.jcchat.model.FriendEntity;
import com.funlab.jcchat.model.UserEntity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class FriendEditActivity extends CommonActivity implements View.OnClickListener {

    private TextView ui_txvClose;
    private Button ui_btnBlockFriend;

    private ListView _editFriList = null;
    private EditFriendAdapter _editFriAdapter = null;

    private EditText ui_edtSearch;

    private UserEntity _user;

    private ArrayList<FriendEntity> _toBlockFriends = new ArrayList<FriendEntity>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_edit);

        _user = Commons.g_user;

        loadLayout();
    }

    public void loadLayout(){

        ui_txvClose = (TextView)findViewById(R.id.txv_close);
        ui_txvClose.setOnClickListener(this);

        ui_btnBlockFriend = (Button)findViewById(R.id.btn_block);
        ui_btnBlockFriend.setOnClickListener(this);

        ui_edtSearch = (EditText)findViewById(R.id.edt_search);

        ui_edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = ui_edtSearch.getText().toString().toLowerCase(Locale.getDefault());
                _editFriAdapter.filter(text);
            }
        });


        ui_edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                switch (actionId) {
                    case EditorInfo.IME_ACTION_SEARCH:
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(ui_edtSearch.getWindowToken(), 0);
                        return true;

                    default:
                        return false;
                }
            }
        });

        _editFriList = (ListView)findViewById(R.id.liv_blockFri);
        _editFriAdapter = new EditFriendAdapter(this);

        getEditFriendData();
        _editFriList.setAdapter(_editFriAdapter);
    }

    public void getEditFriendData(){

        _editFriAdapter.clearAll();

        for (int i = 0; i < _user.get_friendList().size(); i++){

            FriendEntity friend = _user.get_friendList().get(i);
            friend.set_isSelected(false);

            if (friend.get_blockStatus() == 1)
                _editFriAdapter.addItem(friend);
        }

        _editFriAdapter.initEditFriendData();
        _editFriAdapter.notifyDataSetChanged();

    }

    public void showBlockPopup() {

        _toBlockFriends.clear();

        for (int i = 0; i < _user.get_friendList().size(); i++) {

            FriendEntity friend = _user.get_friendList().get(i);

            if (friend.is_isSelected())
                _toBlockFriends.add(friend);
        }

        if (_toBlockFriends.size() == 0) {
            return;
        }

        AlertDialog alertDialog = new AlertDialog.Builder(_context).create();

        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(getString(R.string.blcok_confirm));

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,
                _context.getString(R.string.ok),

                new android.content.DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        blockFriends();
                    }
                });

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE,
                getString(R.string.cancel),

                new android.content.DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        alertDialog.show();
    }

    public void blockFriends() {

        showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_BLOCKFRIENDLIST;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url , new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {

                parseBlockFriendResponse(json);
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                showAlertDialog(getString(R.string.error));
            }
        }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();

                try {
                    params.put(ReqConst.PARAM_ID, String.valueOf(_user.get_idx()));

                    JSONArray friendIds = new JSONArray();

                    for (FriendEntity friendEntity : _toBlockFriends) {
                        friendIds.put(String.valueOf(friendEntity.get_idx()));
                    }

                    params.put(ReqConst.PARAM_FRIENDLIST, friendIds.toString());
                    params.put(ReqConst.PARAM_BLCOKSTATUS, "0");

                } catch (Exception e) {
                }

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        JCChatApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    public void parseBlockFriendResponse(String json) {

        try {

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS) {

                for (FriendEntity friendEntity : _toBlockFriends) {
                    friendEntity.set_blockStatus(0);
                    Database.createBlock(friendEntity.get_idx());
                }

                showAlertDialog(getString(R.string.blockfriend_success));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        closeProgress();
        getEditFriendData();
    }

    public void onClick(View view){

        switch (view.getId()){
            case R.id.txv_close:
                finish();
                break;
            case R.id.btn_block:
                showBlockPopup();
                break;
        }
    }
}
