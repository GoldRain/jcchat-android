package com.funlab.jcchat.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.funlab.jcchat.Base.CommonActivity;
import com.funlab.jcchat.JCChatApplication;
import com.funlab.jcchat.R;
import com.funlab.jcchat.adapter.NoteListAdapter;
import com.funlab.jcchat.commons.Constants;
import com.funlab.jcchat.commons.ReqConst;
import com.funlab.jcchat.model.NoteEntity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class NoteListActivity extends CommonActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    private TextView ui_txvClose;

    private ListView ui_lstNote;
    private NoteListAdapter _noteAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_list);

        loadLayout();

    }

    public void loadLayout(){

        ui_txvClose = (TextView)findViewById(R.id.txv_close);
        ui_txvClose.setOnClickListener(this);

        ui_lstNote = (ListView) findViewById(R.id.lst_note);
        _noteAdapter = new NoteListAdapter(this);
        ui_lstNote.setAdapter(_noteAdapter);

        ui_lstNote.setOnItemClickListener(this);

        getNoteList();
    }


    public void getNoteList() {

        showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_GETNOTELIST;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {

                parseNoteResponse(json);
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                showAlertDialog(getString(R.string.error));
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        JCChatApplication.getInstance().addToRequestQueue(stringRequest, url);
    }


    public void parseNoteResponse(String json){

        _noteAdapter.clear();

        try{

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                JSONArray notes = response.getJSONArray(ReqConst.RES_NOTELIST);

                for (int i = 0; i < notes.length(); i++) {

                    JSONObject note = (JSONObject) notes.get(i);
                    NoteEntity entity = new NoteEntity();
                    entity.set_idx(note.getInt(ReqConst.RES_IDX));
                    entity.set_date(note.getString(ReqConst.RES_NOTEDATE));
                    entity.set_title(note.getString(ReqConst.RES_NOTETITLE));

                    _noteAdapter.add(entity);
                }
            }

        }catch (JSONException e){
            e.printStackTrace();
            showAlertDialog(getString(R.string.error));
        }

        closeProgress();
        _noteAdapter.notifyDataSetChanged();
    }

    public void gotoNoteActivity(NoteEntity note) {

        Intent intent = new Intent(NoteListActivity.this, NoteContentActivity.class);
        intent.putExtra(Constants.KEY_NOTE, note);
        startActivity(intent);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        NoteEntity note = (NoteEntity) _noteAdapter.getItem(position);
        gotoNoteActivity(note);
    }

    public void onClick(View view){

        switch (view.getId()){
            case R.id.txv_close:
                finish();
                break;

        }
    }


}
