package com.funlab.jcchat.main;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.funlab.jcchat.Base.CommonActivity;
import com.funlab.jcchat.JCChatApplication;
import com.funlab.jcchat.R;
import com.funlab.jcchat.commons.Commons;
import com.funlab.jcchat.commons.Constants;
import com.funlab.jcchat.commons.ReqConst;
import com.funlab.jcchat.model.UserEntity;
import com.funlab.jcchat.preference.PrefConst;
import com.funlab.jcchat.preference.Preference;

import org.json.JSONException;
import org.json.JSONObject;

public class SettingActivity extends CommonActivity implements View.OnClickListener {

    ImageView ui_imvPushSetting, ui_imvAddFriAllow;

    private UserEntity _user = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        _user = Commons.g_user;
        loadLayout();
    }

    public void loadLayout(){

        TextView txvClose = (TextView)findViewById(R.id.txv_close);
        txvClose.setOnClickListener(this);

        ui_imvAddFriAllow = (ImageView)findViewById(R.id.imv_setting_allow_toggle);
        ui_imvAddFriAllow.setOnClickListener(this);

        ui_imvPushSetting = (ImageView)findViewById(R.id.imv_setting_push_toggle);
        ui_imvPushSetting.setOnClickListener(this);

        loadSetting();
    }

    public void loadSetting() {

        boolean pushSetting = Preference.getInstance().getValue(this, PrefConst.PREFKEY_PUSH, true);
        ui_imvPushSetting.setSelected(pushSetting);

        boolean allowed = (_user.is_allowFriend() == 1 ? true : false);
        ui_imvAddFriAllow.setSelected(allowed);
    }

    public void savePushSetting() {

        Preference.getInstance().put(this, PrefConst.PREFKEY_PUSH, ui_imvPushSetting.isSelected());
    }

    public void saveAllowSetting(){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_SETALLOWFRIEND;

        int status = ui_imvAddFriAllow.isSelected() ? 1 : 0;

        String params = String.format("/%d/%d", _user.get_idx(), status);
        url += params;

        showProgress();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {

                parseSettingResponse(json);
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        JCChatApplication.getInstance().addToRequestQueue(stringRequest, url);

    }

    public void parseSettingResponse(String json){

        try{

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if(result_code == ReqConst.CODE_SUCCESS){
                _user.set_allowFriend(ui_imvAddFriAllow.isSelected() ? 1 : 0);
            } else {
                showAlertDialog(getString(R.string.error));
            }
        }catch (JSONException e){
            e.printStackTrace();
            showAlertDialog(getString(R.string.error));
        }

        loadSetting();

        closeProgress();
    }

    public void onClick(View view){

        switch (view.getId()){

            case R.id.txv_close:
                finish();
                break;
            case R.id.imv_setting_allow_toggle:
                ui_imvAddFriAllow.setSelected(!ui_imvAddFriAllow.isSelected());
                saveAllowSetting();
                break;
            case R.id.imv_setting_push_toggle:
                ui_imvPushSetting.setSelected(!ui_imvPushSetting.isSelected());
                savePushSetting();
                break;
        }
    }


}
