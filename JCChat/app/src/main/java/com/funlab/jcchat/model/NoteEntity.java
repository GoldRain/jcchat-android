package com.funlab.jcchat.model;

import java.io.Serializable;

/**
 * Created by JIS on 12/21/2015.
 */
public class NoteEntity implements Serializable {

    int _idx = 0;
    String _title = "";
    String _date = "";

    public NoteEntity(int idx, String title, String date) {

        _idx = idx;
        _title = title;
        _date = date;
    }

    public NoteEntity() {}

    public int get_idx() {
        return _idx;
    }

    public void set_idx(int _idx) {
        this._idx = _idx;
    }

    public String get_date() {
        return _date;
    }

    public void set_date(String _date) {
        this._date = _date;
    }

    public String get_title() {
        return _title;
    }

    public void set_title(String _title) {
        this._title = _title;
    }
}
