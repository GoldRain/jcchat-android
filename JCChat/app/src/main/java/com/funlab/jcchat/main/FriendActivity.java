package com.funlab.jcchat.main;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.funlab.jcchat.Base.CommonTabActivity;
import com.funlab.jcchat.JCChatApplication;
import com.funlab.jcchat.R;
import com.funlab.jcchat.Utils.CirculaireNetworkImageView;
import com.funlab.jcchat.adapter.FriendListAdapter;
import com.funlab.jcchat.chatting.GroupChattingActivity;
import com.funlab.jcchat.commons.Constants;
import com.funlab.jcchat.commons.ReqConst;
import com.funlab.jcchat.model.FriendEntity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class   FriendActivity extends CommonTabActivity implements View.OnClickListener{

    public static final int PROFILE_CODE = 100;

    private ImageView ui_imvFriend, ui_imvChatting, ui_imvMyInfo, ui_imvAddFriend;
    private TextView ui_txvTitle, ui_txvFriBlock, ui_txvFriendCnt;
    private RelativeLayout ui_relativeLayout;
    private ImageLoader _imageLoader;

    CirculaireNetworkImageView ui_imvProfilePhoto;

    private ListView _friendListView;
    private FriendListAdapter _friendListAdapter = null;

    private EditText ui_edtSearch;

    private int _friendCounter = 0;
    private ArrayList<String> _contacts = new ArrayList<String>();

    LinearLayout lyt_footer_friend, lyt_footer_chat, lyt_footer_addFriend, lyt_footer_info;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend);

        String room = getIntent().getStringExtra(Constants.KEY_ROOM);
        boolean fromlogin = getIntent().getBooleanExtra(Constants.KEY_FROMLOGIN, false);

        _friendCounter = _user.get_friendList().size();

        loadLayout();

        if (room != null) {
            Intent intent = new Intent(FriendActivity.this, GroupChattingActivity.class);
            intent.putExtra(Constants.KEY_ROOM, room);
            startActivity(intent);
        }

        if (!fromlogin) {
            addContacts();
        }
    }

    public void loadLayout(){

        lyt_footer_friend = (LinearLayout)findViewById(R.id.lyt_footer_friend);
        lyt_footer_chat = (LinearLayout)findViewById(R.id.lyt_footer_chat);
        lyt_footer_addFriend = (LinearLayout)findViewById(R.id.lyt_footer_addFriend);
        lyt_footer_info = (LinearLayout)findViewById(R.id.lyt_footer_info);

        /*Changing the letter color in one TextView */
        ui_txvTitle = (TextView)findViewById(R.id.friend_title);

        ui_relativeLayout = (RelativeLayout)findViewById(R.id.layout_friend_profile);
        ui_relativeLayout.setOnClickListener(this);

        TextView txvName = (TextView) findViewById(R.id.my_name);
        txvName.setText(_user.get_name());

        TextView txvState = (TextView) findViewById(R.id.my_state);
        txvState.setText(_user.get_label());

        ui_imvProfilePhoto = (CirculaireNetworkImageView)findViewById(R.id.imv_my_profile_photo);
        _imageLoader = JCChatApplication.getInstance().getImageLoader();

        if (_user.get_photoUrl().length() > 0)
            ui_imvProfilePhoto.setImageUrl(_user.get_photoUrl(), _imageLoader);
        else
            ui_imvProfilePhoto.setImageResource(R.drawable.bg_non_profile1);

        ui_txvFriendCnt = (TextView) findViewById(R.id.txv_friendCnt);

        ImageView imvFriAddFriend = (ImageView)findViewById(R.id.imv_goto_addfriend);
        imvFriAddFriend.setOnClickListener(this);

        ui_edtSearch = (EditText)findViewById(R.id.edt_search);
        ui_edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = ui_edtSearch.getText().toString().toLowerCase(Locale.getDefault());
                _friendListAdapter.filter(text);
            }
        });

        ui_edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                switch (actionId) {
                    case EditorInfo.IME_ACTION_SEARCH:
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(ui_edtSearch.getWindowToken(), 0);
                        return true;

                    default:
                        return false;
                }
            }
        });

        ui_txvFriBlock = (TextView)findViewById(R.id.txv_FriBlock);
        ui_txvFriBlock.setOnClickListener(this);

        ui_imvFriend = (ImageView)findViewById(R.id.imv_friend);
        ui_imvFriend.setOnClickListener(this);
        ui_imvFriend.setSelected(true);

        TextView txvFriend = (TextView) findViewById(R.id.txv_friend);
        txvFriend.setSelected(true);

        ui_imvChatting = (ImageView)findViewById(R.id.imv_chatting);
        ui_imvChatting.setOnClickListener(this);

        ui_imvAddFriend = (ImageView)findViewById(R.id.imv_add_friend);
        ui_imvAddFriend.setOnClickListener(this);

        ui_imvMyInfo = (ImageView)findViewById(R.id.imv_my_info);
        ui_imvMyInfo.setOnClickListener(this);

        ui_txvUnread = (TextView) findViewById(R.id.txv_unread);
        setUnRead();

        /*FriendList*/
        _friendListView = (ListView)findViewById(R.id.friend_lis);
        _friendListAdapter = new FriendListAdapter(this);
        _friendListView.setAdapter(_friendListAdapter);

        _friendListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(FriendActivity.this, FriendProfileActivity.class);
                intent.putExtra(Constants.KEY_FRIEND, (FriendEntity) _friendListAdapter.getItem(position));
                startActivity(intent);
            }
        });

        setFooter(getResources().getColor(R.color.blue), getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.colorPrimary));

    }
    public void setFooter(int a, int b, int c, int d){

        lyt_footer_friend.setBackgroundColor(a);
        lyt_footer_chat.setBackgroundColor(b);
        lyt_footer_addFriend.setBackgroundColor(c);
        lyt_footer_info.setBackgroundColor(d);
    }


    public void addContacts() {

        _contacts = getContactList();

        if (_contacts.size() > 0) {
            addFriendByPhonenumber();
        }
    }

    public void addFriendByPhonenumber() {

        String url = ReqConst.SERVER_URL + ReqConst.REQ_ADDFRIENDBYPHONENUMBER;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url , new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {

                parseAddFriendResponse(json);
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();

                try {
                    params.put(ReqConst.PARAM_ID, String.valueOf(_user.get_idx()));

                    JSONArray phonenumbers = new JSONArray();

                    for (String contact : _contacts) {
                        phonenumbers.put(contact);
                    }

                    params.put(ReqConst.PARAM_PHONE_NUMBERS, phonenumbers.toString());

                } catch (Exception e) {

                }

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        JCChatApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    public void parseAddFriendResponse(String json){
        getFriendList();
    }


    public ArrayList<String> getContactList() {

        ArrayList<String> contactList = new ArrayList<String>();

        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;

        String[] projection = new String[] {
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
                ContactsContract.CommonDataKinds.Phone.NUMBER,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME };

        String[] selectionArgs = null;

        String sortOrder = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME
                + " COLLATE LOCALIZED ASC";

        ContentResolver cr = getContentResolver();

        Cursor contactCursor = cr.query(uri, projection, null, selectionArgs,
                sortOrder);

        if (contactCursor.moveToFirst()) {

            do {
                String phonenumber = contactCursor.getString(1).replaceAll("-", "");
                phonenumber = phonenumber.replace("+", "0");

                if (phonenumber.length() > Constants.PHONENUMBER_LENGTH)
                    phonenumber = phonenumber.substring(phonenumber.length() - Constants.PHONENUMBER_LENGTH);

                contactList.add(phonenumber);

            } while (contactCursor.moveToNext());
        }

        contactCursor.close();

        return contactList;
    }

    public void getFriendList() {

        String url = ReqConst.SERVER_URL + ReqConst.REQ_GETFRIENDLIST;

        String params = String.format("/%s", _user.get_idx());
        url += params;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {

                parseFriendResponse(json);
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        JCChatApplication.getInstance().addToRequestQueue(stringRequest, url);
    }


    public void parseFriendResponse(String json){

        try{

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                JSONArray friends = response.getJSONArray(ReqConst.RES_FRIENDLIST);

                _user.get_friendList().clear();

                for (int i = 0; i < friends.length(); i++) {

                    JSONObject friend = (JSONObject) friends.get(i);
                    FriendEntity entity = new FriendEntity();

                    entity.set_idx(friend.getInt(ReqConst.RES_ID));
                    entity.set_name(friend.getString(ReqConst.RES_NAME));
                    entity.set_label(friend.getString(ReqConst.RES_LABEL));
                    entity.set_photoUrl(friend.getString(ReqConst.RES_PHOTO_URL));
                    entity.set_bgUrl(friend.getString(ReqConst.RES_BG_URL));
                    entity.set_blockStatus(friend.getInt(ReqConst.RES_BLOCK_STATUS));

                    _user.get_friendList().add(entity);
                }

                getFriendData();

            }

        }catch (JSONException e){
            e.printStackTrace();
        }

    }

    public void getFriendData() {

        _friendListAdapter.clearAll();

        _friendCounter = 0;

        for (int i = 0; i < _user.get_friendList().size(); i++){

            FriendEntity friend = _user.get_friendList().get(i);

            if (friend.get_blockStatus() == 1) {
                _friendListAdapter.addItem(friend);
                _friendCounter++;
            }
        }

        _friendListAdapter.initFriendDatas();
        _friendListAdapter.notifyDataSetChanged();

        String friendString = getResources().getString(R.string.friend);
        int start = friendString.length() + 1;

        ui_txvTitle.setText("");

        if (_friendCounter > 0) {
            friendString += " " + _friendCounter;

            int color = getResources().getColor(R.color.friend_title);
            SpannableStringBuilder builder = new SpannableStringBuilder(friendString);

            builder.setSpan(new ForegroundColorSpan(color), start, friendString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            ui_txvTitle.append(builder);
        } else {
            ui_txvTitle.setText(getString(R.string.friend));
        }

        if (_friendCounter > 0)
            ui_txvFriendCnt.setText(getString(R.string.friend) + " " +  _friendCounter);
        else
            ui_txvFriendCnt.setText(getString(R.string.friend));

    }

    public void onClick(View view){

        switch (view.getId()){
            case R.id.imv_add_friend:
                gotoAddFriendActivity();
                break;
            case R.id.imv_friend:
                break;
            case R.id.imv_chatting:
                gotoChatListActivity();
                break;
            case R.id.imv_my_info:
                gotoMyInfoActivity();
                break;
            case R.id.layout_friend_profile:
                gotoProfileActivity();
                break;
            case R.id.imv_goto_addfriend:
                gotoAddFriendActivity();
                break;
            case R.id.txv_FriBlock:
                gotoFriendEditActivity();
        }
    }

    public void gotoAddFriendActivity(){

        Intent intent = new Intent(this, AddFriendActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();
    }

    public void gotoChatListActivity(){

        Intent intent = new Intent(this, ChatListActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(0,0);
        finish();

    }

    public void gotoMyInfoActivity(){

        Intent intent = new Intent(this, MyInformationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(0,0);
        finish();

    }

    public void gotoProfileActivity(){

        Intent intent = new Intent(this,ProfileActivity.class);
        startActivityForResult(intent, PROFILE_CODE);
    }

    public void gotoFriendEditActivity(){

        Intent intent = new Intent(this, FriendEditActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

            case PROFILE_CODE:

                if (_user.get_photoUrl().length() > 0)
                    ui_imvProfilePhoto.setImageUrl(_user.get_photoUrl(), _imageLoader);
                else
                    ui_imvProfilePhoto.setImageResource(R.drawable.bg_non_profile1);

                if (resultCode == RESULT_OK) {
                    gotoMyInfoActivity();
                }
                break;
        }
    }

    @Override
    protected void onResume() {

        super.onResume();

        setUnRead();
        getFriendData();

    }

}
