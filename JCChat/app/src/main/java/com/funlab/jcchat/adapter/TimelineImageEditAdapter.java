package com.funlab.jcchat.adapter;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.funlab.jcchat.R;
import com.funlab.jcchat.commons.Constants;
import com.funlab.jcchat.main.ImagePreviewActivity;
import com.funlab.jcchat.main.TakePhotoActivity;
import com.funlab.jcchat.main.VideoPreviewActivity;

import java.util.ArrayList;

/**
 * Created by Asaf on 1/19/2016.
 */

public class TimelineImageEditAdapter extends RecyclerView.Adapter<TimelineImageEditAdapter.ImageHolder> {

    private ArrayList<String> _imageUrls = new ArrayList<>();
    private TakePhotoActivity _context;

    public TimelineImageEditAdapter(TakePhotoActivity context) {

        this._context = context;
    }


    @Override
    public ImageHolder onCreateViewHolder(ViewGroup viewGroup, final int position) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_takephoto_image_edit, viewGroup, false);

        ImageHolder viewHolder = new ImageHolder(view);
        return viewHolder;
    }


    public void setDatas(ArrayList<String> images) {
        _imageUrls = images;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(ImageHolder viewHolder, int i) {

        String imageUrl = _imageUrls.get(i);
        Bitmap bitmap = BitmapFactory.decodeFile(imageUrl);
//        Bitmap scaledBitmap = BitmapUtils.getSizeLimitedBitmap(bitmap);
//        bitmap.recycle();
        viewHolder.imvPhoto.setImageBitmap(bitmap);

        final int position = i;

        viewHolder.imvPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Constants.TAKE_VIDEO == 5){

                    Intent intent = new Intent(_context, VideoPreviewActivity.class);
                    intent.putExtra(Constants.KEY_VIDEOPATH, Constants.VIDEO_PATH);
                    _context.startActivity(intent);

                } else {

                    Intent intent = new Intent(_context, ImagePreviewActivity.class);
                    intent.putExtra(Constants.KEY_IMAGEPATH, _imageUrls);
                    intent.putExtra(Constants.KEY_POSITION, position);
                    _context.startActivity(intent);

                }
            }
        });

        viewHolder.imvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _context.removeImage(_imageUrls.get(position));
                notifyDataSetChanged();
            }
        });

        if (Constants.TAKE_VIDEO == 5){

            viewHolder.imvVideoPlay.setVisibility(View.VISIBLE);
        } else viewHolder.imvVideoPlay.setVisibility(View.GONE);

    }

    @Override
    public int getItemCount() {
        return (null != _imageUrls ? _imageUrls.size() : 0);
    }


    public class ImageHolder extends RecyclerView.ViewHolder {

        ImageView imvPhoto;
        ImageView imvDelete;
        ImageView imvVideoPlay;

        public ImageHolder(View view) {

            super(view);
            imvPhoto = (ImageView) view.findViewById(R.id.imv_image);
            imvDelete = (ImageView) view.findViewById(R.id.imv_delete);
            imvVideoPlay = (ImageView)view.findViewById(R.id.imv_videoplay);
        }
    }


}
