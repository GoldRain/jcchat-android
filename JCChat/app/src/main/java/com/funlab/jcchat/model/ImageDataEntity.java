package com.funlab.jcchat.model;

import java.io.Serializable;

/**
 * Created by HugeRain on 4/13/2017.
 */

public class ImageDataEntity implements Serializable{

    int _id = 0;
    String _photos = "";
    String _videoUrl = "";

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String get_photos() {
        return _photos;
    }

    public void set_photos(String _photos) {
        this._photos = _photos;
    }

    public String get_videoUrl() {
        return _videoUrl;
    }

    public void set_videoUrl(String _videoUrl) {
        this._videoUrl = _videoUrl;
    }
}
