package com.funlab.jcchat.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.funlab.jcchat.JCChatApplication;
import com.funlab.jcchat.R;
import com.funlab.jcchat.Utils.CirculaireNetworkImageView;
import com.funlab.jcchat.Utils.HangulUtils;
import com.funlab.jcchat.model.FriendEntity;

import java.util.ArrayList;

/**
 * Created by HGS on 12/13/2015.
 */
public class ReleaseBlockFriendAdapter extends BaseAdapter{

    private Context _context = null;
    private ArrayList<FriendEntity> _releaseBlockFriendDatas = new ArrayList<FriendEntity>();
    private ArrayList<FriendEntity> _releaseBlockFriendAllDatas = new ArrayList<>();

    private ImageLoader _imageLoader;

    public ReleaseBlockFriendAdapter(Context _context){

        super();
        this._context = _context;

        _imageLoader = JCChatApplication.getInstance().getImageLoader();
    }

    @Override
    public int getCount(){

        return _releaseBlockFriendDatas.size();
    }

    @Override
    public Object getItem(int position){

        return _releaseBlockFriendDatas.get(position);
    }

    @Override
    public long getItemId(int position){

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){

        ReleaseBlockFriendHolder releaseBlockFriendHolder;

        if(convertView == null){
            releaseBlockFriendHolder = new ReleaseBlockFriendHolder();

            LayoutInflater inflater = (LayoutInflater)_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.release_block_friend_list_item, null);

            releaseBlockFriendHolder.imvReleaseBlockFriendPhoto = (CirculaireNetworkImageView)convertView.findViewById(R.id.imv_releaseBlockFriendPhoto);
            releaseBlockFriendHolder.txvReleaseBlockFriendName = (TextView)convertView.findViewById(R.id.txv_releaseBlockFriendName);
            releaseBlockFriendHolder.imvReleaseBlockFriendState = (ImageView)convertView.findViewById(R.id.imv_releaseBlockFriendState);

            convertView.setTag(releaseBlockFriendHolder);
        }else {
            releaseBlockFriendHolder = (ReleaseBlockFriendHolder)convertView.getTag();
        }

        final FriendEntity friend = _releaseBlockFriendDatas.get(position);

        releaseBlockFriendHolder.txvReleaseBlockFriendName.setText(friend.get_name());

        if (friend.get_photoUrl().length() > 0)
            releaseBlockFriendHolder.imvReleaseBlockFriendPhoto.setImageUrl(friend.get_photoUrl(), _imageLoader);
        else
            releaseBlockFriendHolder.imvReleaseBlockFriendPhoto.setImageResource(R.drawable.bg_non_profile1);

        final ImageView imvState = releaseBlockFriendHolder.imvReleaseBlockFriendState;
        imvState.setSelected(friend.is_isSelected());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imvState.setSelected(!imvState.isSelected());
                friend.set_isSelected(imvState.isSelected());
            }
        });

        return convertView;
    }

    public void addItem(FriendEntity entity){

        _releaseBlockFriendAllDatas.add(entity);
    }

    public void clearAll() {

        _releaseBlockFriendAllDatas.clear();
    }

    public void initReleaseBlockFriendDataes(){

        _releaseBlockFriendDatas.clear();
        _releaseBlockFriendDatas.addAll(_releaseBlockFriendAllDatas);
    }

    public void filter(String charText){

        charText = charText.toLowerCase();

        _releaseBlockFriendDatas.clear();

        if(charText.length() == 0){
            _releaseBlockFriendDatas.addAll(_releaseBlockFriendAllDatas);
        }else {

            for (FriendEntity friend : _releaseBlockFriendAllDatas){

                String value = friend.get_name().toLowerCase();

                if(value.contains(charText)
                        || HangulUtils.isHangulInitialSound(value, charText)){
                    _releaseBlockFriendDatas.add(friend);
                }
            }
        }
        notifyDataSetChanged();
    }

    public class ReleaseBlockFriendHolder {

        public CirculaireNetworkImageView imvReleaseBlockFriendPhoto;
        public TextView txvReleaseBlockFriendName;
        public ImageView imvReleaseBlockFriendState;
    }
}
