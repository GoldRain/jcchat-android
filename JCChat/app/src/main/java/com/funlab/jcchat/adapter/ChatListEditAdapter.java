package com.funlab.jcchat.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.funlab.jcchat.JCChatApplication;
import com.funlab.jcchat.R;
import com.funlab.jcchat.Utils.CirculaireNetworkImageView;
import com.funlab.jcchat.commons.Commons;
import com.funlab.jcchat.model.FriendEntity;
import com.funlab.jcchat.model.RoomEntity;

import java.util.ArrayList;

/**
 * Created by HGS on 12/14/2015.
 */
public class ChatListEditAdapter extends BaseAdapter {

    private Context _context;
    private ArrayList<RoomEntity> _roomDatas = new ArrayList<RoomEntity>();

    ImageLoader _imageLoader;

    public ChatListEditAdapter(Context _context){

        super();
        this._context = _context;

        _imageLoader = JCChatApplication.getInstance().getImageLoader();

        _roomDatas = Commons.g_user.get_roomList();

        for (RoomEntity room :_roomDatas)
            room.set_isSelected(false);
    }

    @Override
    public int getCount(){
        return _roomDatas.size();
    }

    @Override
    public Object getItem(int position){
        return _roomDatas.get(position);
    }

    @Override
    public long getItemId(int position){
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){

        ChatListEditHolder chatListEditHolder;

        if(convertView == null){
            chatListEditHolder = new ChatListEditHolder();

            LayoutInflater inflater = (LayoutInflater)_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.chat_edit_list_item, null);

            chatListEditHolder.imvChatEdittingPhoto = (CirculaireNetworkImageView)convertView.findViewById(R.id.imv_ChatEditPhoto);
            chatListEditHolder.txvChatEdittingName = (TextView)convertView.findViewById(R.id.txv_Name);
            chatListEditHolder.txvChatEdittingContent = (TextView)convertView.findViewById(R.id.txv_chatContent);
            chatListEditHolder.txvDateHOur = (TextView)convertView.findViewById(R.id.txv_date_hour);
            chatListEditHolder.txvChatState = (TextView)convertView.findViewById(R.id.txv_chatState);

            convertView.setTag(chatListEditHolder);
        }else {
            chatListEditHolder = (ChatListEditHolder)convertView.getTag();
        }

        final RoomEntity room = _roomDatas.get(position);

        chatListEditHolder.txvChatEdittingName.setText(room.get_displayName() + room.get_displayCount());
        chatListEditHolder.txvChatEdittingContent.setText(room.get_recentContent());
        chatListEditHolder.txvDateHOur.setText(room.get_recentTime());

        String[] parti = room.get_name().split("_");
        if (parti.length == 2) {

            int otherIdx = Integer.parseInt(parti[0]);
            if (otherIdx == Commons.g_user.get_idx())
                otherIdx = Integer.parseInt(parti[1]);

            FriendEntity friend = room.getParticipant(otherIdx);
            if (friend != null)
                chatListEditHolder.imvChatEdittingPhoto.setImageUrl(friend.get_photoUrl(), _imageLoader);
        } else {
            chatListEditHolder.imvChatEdittingPhoto.setImageBitmap(null);
        }

        final TextView txvChatState = chatListEditHolder.txvChatState;
        txvChatState.setSelected(room.is_isSelected());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txvChatState.setSelected(!txvChatState.isSelected());
                room.set_isSelected(txvChatState.isSelected());
            }
        });

        return convertView;
    }

    public class ChatListEditHolder {

        public CirculaireNetworkImageView imvChatEdittingPhoto;
        public TextView txvChatEdittingName;
        public TextView txvChatEdittingContent;
        public TextView txvDateHOur;
        public TextView txvChatState;
    }
}
