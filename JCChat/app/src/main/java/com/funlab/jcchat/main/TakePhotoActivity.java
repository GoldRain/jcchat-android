package com.funlab.jcchat.main;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.media.MediaRecorder;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.funlab.jcchat.Base.CommonActivity;
import com.funlab.jcchat.JCChatApplication;
import com.funlab.jcchat.R;
import com.funlab.jcchat.Utils.BitmapUtils;
import com.funlab.jcchat.Utils.CustomMultipartRequest;
import com.funlab.jcchat.Utils.MediaRealPathUtil;
import com.funlab.jcchat.Utils.MultiPartRequest;
import com.funlab.jcchat.adapter.TimelineImageEditAdapter;
import com.funlab.jcchat.commons.Commons;
import com.funlab.jcchat.commons.Constants;
import com.funlab.jcchat.commons.ReqConst;
import com.soundcloud.android.crop.Crop;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class TakePhotoActivity extends CommonActivity implements View.OnClickListener{

    public static final int MAX_CHARS = 100;
    public static final int MAX_IMAGES = 9;

    TextView ui_txvLeaveChars, ui_txvSave;
    EditText ui_edtContent;

    RecyclerView ui_recyclerImage;
    TimelineImageEditAdapter _adapter;

    ArrayList<String> _imagePaths = new ArrayList<>();

    private Uri _imageCaptureUri;
    String _photoPath = "";

    boolean _isSaving = false;

    String _thumPath, videoPath = "", _thumbPath = "";
    String _video_thumb = "";

    ArrayList<String> _complextVideo = new ArrayList<>();

    String _current_date = "";
    String _current_time = "";

    int mo_id = 0;

    File filethumb=null;  // thumb


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_photo);

        _isSaving = false;

        loadLayout();
    }


    private void loadLayout() {

        TextView txvCancel = (TextView) findViewById(R.id.txv_cancel);
        txvCancel.setOnClickListener(this);

        ui_txvSave = (TextView) findViewById(R.id.txv_save);
        ui_txvSave.setOnClickListener(this);

        ImageView imvCamera = (ImageView) findViewById(R.id.imv_camera);
        imvCamera.setOnClickListener(this);

        ui_txvLeaveChars = (TextView) findViewById(R.id.txv_leaveChars);

        ui_edtContent = (EditText) findViewById(R.id.edt_content);
        ui_edtContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ui_txvLeaveChars.setText(String.valueOf(MAX_CHARS - s.length()));

                if ((MAX_CHARS - s.length()) == 0){

                    showAlertDialog("You can't input comment anymore");
                }

                if (ui_edtContent.length() > 0 || _imagePaths.size() > 0) {
                    ui_txvSave.setTextColor(0xff5f99fb);
                } else {
                    ui_txvSave.setTextColor(0xff333333);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        ui_recyclerImage = (RecyclerView) findViewById(R.id.recycler_image);
        LinearLayoutManager layoutManager = new LinearLayoutManager(
                this, LinearLayoutManager.HORIZONTAL, false);
        ui_recyclerImage.setLayoutManager(layoutManager);

        _adapter = new TimelineImageEditAdapter(this);
        ui_recyclerImage.setAdapter(_adapter);

        deleteAllTempImages();

        LinearLayout lytContainer = (LinearLayout) findViewById(R.id.lyt_container);
        lytContainer.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(ui_edtContent.getWindowToken(), 0);
                return false;
            }
        });

        cameraSetting();
    }

    private void cameraSetting() {

        MediaRecorder mediaRecorder =  new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
        mediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.DEFAULT);
        mediaRecorder.setVideoSize(640, 480);
        mediaRecorder.setVideoFrameRate(10);
    }


    public void addImage(String path) {

        _imagePaths.add(path);
        _adapter.setDatas(_imagePaths);
        _adapter.notifyDataSetChanged();

        if (ui_edtContent.length() > 0 || _imagePaths.size() > 0 || _thumbPath.length() > 0) {
            ui_txvSave.setTextColor(0xff5f99fb);
        } else {
            ui_txvSave.setTextColor(0xff333333);
        }
    }

    public void addImages(ArrayList<String> paths) {

        _imagePaths.addAll(paths);

        _adapter.setDatas(_imagePaths);

        if (ui_edtContent.length() > 0 || _imagePaths.size() > 0) {
            ui_txvSave.setTextColor(0xff5f99fb);
        } else {
            ui_txvSave.setTextColor(0xff333333);
        }
    }

    public void removeImage(String path) {

        if (Constants.TAKE_VIDEO == 5){

            _thumbPath = "";
            Constants.TAKE_VIDEO = 0;
        }

        _imagePaths.remove(path);
        _adapter.setDatas(_imagePaths);

        if (ui_edtContent.length() > 0 || _imagePaths.size() > 0) {
            ui_txvSave.setTextColor(0xff5f99fb);
        } else {
            ui_txvSave.setTextColor(0xff333333);
        }
    }

    private void onSave() {

        if (ui_edtContent.length() == 0  ) {
            showAlertDialog(getString(R.string.input_timeline_content));
            return;

        } else if (_imagePaths.size() == 0 && _thumbPath.length() == 0){

            showAlertDialog(getString(R.string.input_timeline_image_video));
            return;
        }

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(ui_edtContent.getWindowToken(), 0);
        Log.d("image_size==>", String.valueOf(_imagePaths.size()));

        if(videoPath.length() != 0 && Constants.TAKE_VIDEO == 5){

            uploadThumbnail();
            //uploadImage();

        }else {

            Log.d("image upload", "image");
            uploadImages();
        }
    }

    public void uploadImages() {

        Calendar c = Calendar.getInstance();
        System.out.println("Current time => "+c.getTime());

        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat current_time = new SimpleDateFormat("HH:mm:ss");

        _current_date = date.format(c.getTime());
        _current_time = current_time.format(c.getTime());

        Log.d("data==>", _current_date);
        Log.d("time==>", _current_time);

        if (_isSaving) {
            showToast("uploading...");
            return;
        }

        _isSaving = true;

        showProgress();

        String url = "http://35.163.255.175/index.php/Api/uploadMomentImages";

        Map<String, String> mHeaderPart= new HashMap<>();
        mHeaderPart.put("Content-type", "multipart/form-data;");

        //File part
        Map<String, File> mFilePartData= new HashMap<>();

        for (int i = 0; i < _imagePaths.size(); i++) {

            String path = _imagePaths.get(i);
            if (path.length() > 0)

                mFilePartData.put("file[" + i + "]", new File(path));
        }

        Log.d("User_id", String.valueOf(Commons.g_user.get_idx()));

        //String part
        Map<String, String> mStringPart= new HashMap<>();
        mStringPart.put("user_id", String.valueOf(Commons.g_user.get_idx()));
        mStringPart.put("post_date", _current_date);
        mStringPart.put("post_time", _current_time);
        mStringPart.put("mo_comment",ui_edtContent.getText().toString().trim());

        CustomMultipartRequest mCustomRequest = new CustomMultipartRequest(Request.Method.POST, this, url, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject jsonObject) {

                parseUploadResponse(jsonObject.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                showToast("Images upload failed");
                closeProgress();
                _isSaving = false;
            }
        }, mFilePartData, mStringPart, mHeaderPart);

        mCustomRequest.setRetryPolicy(new DefaultRetryPolicy(600000,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        JCChatApplication.getInstance().addToRequestQueue(mCustomRequest, url);

    }

    public void parseUploadResponse(String json){

        closeProgress();

        _isSaving = false;

        try{

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                Toast.makeText(_context, "Image upload Success!", Toast.LENGTH_SHORT).show();
                finish();
            } else {
                showAlertDialog(getString(R.string.photo_upload_fail));
            }

        } catch (JSONException e){
            showAlertDialog(getString(R.string.photo_upload_fail));
            e.printStackTrace();
        }
    }

    public void uploadThumbnail(){

        Calendar c = Calendar.getInstance();

        SimpleDateFormat datasss = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat current_time = new SimpleDateFormat("HH:mm:ss");

        _current_date = datasss.format(c.getTime());
        _current_time = current_time.format(c.getTime());

        File file_video = new File(videoPath);

        if (!file_video.exists()) return;

        if (file_video.length() > Constants.LIMIT_FILE) {
            showAlertDialog(getString(R.string.file_overflow));
            return;
        }
        showProgress();

        try {

            File file = new File(_thumbPath);

            Map<String, String> params = new HashMap<String, String>();

            params.put("user_id", String.valueOf(Commons.g_user.get_idx()));
            params.put("post_date", _current_date);
            params.put("post_time", _current_time);
            params.put("mo_comment",ui_edtContent.getText().toString().trim());

            String url = "http://35.163.255.175/index.php/Api/uploadVideoThumbnail";

            MultiPartRequest reqMultiPartThnmb = new MultiPartRequest(url, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    closeProgress();

                    showToast(getString(R.string.photo_upload_fail));

                    NetworkResponse networkResponse = error.networkResponse;

                    if (networkResponse != null) {
                        Log.e("Volley", "Error. HTTP Status Code:"+networkResponse.statusCode);
                    }

                    if (error instanceof TimeoutError) {
                        Log.e("Volley", "TimeoutError");
                    }else if(error instanceof NoConnectionError){
                        Log.e("Volley", "NoConnectionError");
                    } else if (error instanceof AuthFailureError) {
                        Log.e("Volley", "AuthFailureError");
                    } else if (error instanceof ServerError) {
                        Log.e("Volley", "ServerError");
                    } else if (error instanceof NetworkError) {
                        Log.e("Volley", "NetworkError");
                    } else if (error instanceof ParseError) {
                        Log.e("Volley", "ParseError");
                    }
                }
            }, new Response.Listener<String>() {

                @Override
                public void onResponse(String json) {

                    ParseUploadThumbNail(json);
                }
                }, file, ReqConst.PARAM_FILE, params);

            reqMultiPartThnmb.setRetryPolicy(new DefaultRetryPolicy(
                    Constants.VOLLEY_TIME_OUT, 0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            JCChatApplication.getInstance().addToRequestQueue(reqMultiPartThnmb, url);

        } catch (Exception e) {

            e.printStackTrace();
            closeProgress();

            showToast(getString(R.string.photo_upload_fail));

        }
    }

    public void ParseUploadThumbNail(String json){

        Log.d("response :", json);

        //closeProgress();

        try{
            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == 0){

                mo_id = response.getInt("mo_id");

                uploadVideo();
            }
            else {
                showToast("Upload Video Failed.!");
            }
        }catch (JSONException e){

            e.printStackTrace();

        }

    }

    public void uploadVideo(){

        //showProgress();

        try {

            File file = new File(videoPath);

            Map<String, String> params = new HashMap<String, String>();

            Log.d("Mo_id", String.valueOf(mo_id));

            params.put("mo_id", String.valueOf(mo_id));

            String url = "http://35.163.255.175/index.php/Api/uploadVideo";

            MultiPartRequest reqMultiPart = new MultiPartRequest(url, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    closeProgress();
                    showToast("Upload Video Failed.!");

                }
            }, new Response.Listener<String>() {

                @Override
                public void onResponse(String json) {

                    parseVideoResponse(json);
                }

            }, file, ReqConst.PARAM_FILE, params);

            reqMultiPart.setRetryPolicy(new DefaultRetryPolicy(
                    60000, 0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            JCChatApplication.getInstance().addToRequestQueue(reqMultiPart, url);

        } catch (Exception e) {

            e.printStackTrace();
            closeProgress();

            showToast(getString(R.string.photo_upload_fail));

        }
    }

    public void parseVideoResponse(String json){

        closeProgress();

        try{

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                Toast.makeText(_context, "Video upload Success!", Toast.LENGTH_SHORT).show();
                finish();

            } else {
                showAlertDialog("Video upload failed");
            }

        }catch (JSONException e){
            showAlertDialog("Video upload failed");
            e.printStackTrace();
        }

    }

    /**
     * ..............photo part
     * @param
     */

    public void onSelectPhoto(){

        if (_imagePaths.size() == MAX_IMAGES) {

            showAlertDialog("You can only select up to 8 photos.");
            return ;

        } else if (_thumbPath.length() != 0){

            showAlertDialog("You can only select up to 1 video");
            return;
        }

        final String[] items = { "Take photo","Choose from Gallery", "Take video","Cancel"};
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);


        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (item == 2 && _imagePaths.size() == 0){
                    Constants.TAKE_VIDEO = 5;
                    doTakeVideo();

                }else if (item == 0){
                    doTakePhoto();

                }else if (item == 1){

                    doTakeGallery();

                }else return;
            }

        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public void doTakeVideo(){

        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT,  BitmapUtils.getVideoThumbFolderPath());
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);
        intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 10);
        startActivityForResult(intent,Constants.PICK_FROM_VIDEO);
    }

    private String getThumbPath(String _videoPath) {

        File file=new File(_videoPath);

        String thumb_path = null;

        Bitmap thumb = null;

        if (!file.exists())

            return "";

        try {
            thumb = ThumbnailUtils.createVideoThumbnail(_videoPath,
                    MediaStore.Video.Thumbnails.MICRO_KIND);

            File outFile = getOutputMediaFile();

            BitmapUtils.saveOutput(outFile , thumb);
            thumb.recycle();
            thumb = null;

            thumb_path = outFile.getAbsolutePath();


            if (thumb != null)
                BitmapUtils.saveOutput(outFile, thumb);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return thumb_path;

    }

    public void doTakePhoto(){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String picturePath = BitmapUtils.getTempFolderPath()  + "photo_temp.jpg" ;
        _imageCaptureUri = Uri.fromFile(new File(picturePath));

        intent.putExtra(MediaStore.EXTRA_OUTPUT, _imageCaptureUri);
        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);

    }

    private void doTakeGallery(){

        Intent intent = new Intent(this, SelectImageActivity.class);
        intent.putExtra(Constants.KEY_COUNT, _imagePaths.size());
        startActivityForResult(intent, Constants.PICK_FROM_IMAGES);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){

        switch (requestCode) {

            case Constants.PICK_FROM_VIDEO:

                if (resultCode == RESULT_OK) {

                    Uri w_uri = data.getData();

                    String w_strPath = MediaRealPathUtil.getPath(this, w_uri);

                    String filename = Commons.fileNameWithExtFromUrl(w_strPath);

                    if (w_strPath == null) {
                        showToast(getString(R.string.getvideo_fail));
                        return;
                    }

                    videoPath = w_strPath;

                    _thumbPath = getThumbPath(w_strPath);

                    Log.d("thumb_path==", _thumbPath);


                    addImage(_thumbPath);

                    Constants.VIDEO_PATH = videoPath;

                }

                break;

            case Constants.CROP_FROM_CAMERA: {

                if (resultCode == RESULT_OK) {

                    try {

                        File outFile = BitmapUtils.getOutputMediaFile(this);

                        InputStream in = getContentResolver().openInputStream(Uri.fromFile(outFile));
                        BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);
                        in.close();

                        ExifInterface ei = new ExifInterface(outFile.getAbsolutePath());
                        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                                ExifInterface.ORIENTATION_NORMAL);

                        Bitmap returnedBitmap = bitmap;

                        switch (orientation) {

                            case ExifInterface.ORIENTATION_ROTATE_90:
                                returnedBitmap = BitmapUtils.rotateImage(bitmap, 90);
                                // Free up the memory
                                bitmap.recycle();
                                bitmap = null;
                                break;
                            case ExifInterface.ORIENTATION_ROTATE_180:
                                returnedBitmap = BitmapUtils.rotateImage(bitmap, 180);
                                // Free up the memory
                                bitmap.recycle();
                                bitmap = null;
                                break;
                            case ExifInterface.ORIENTATION_ROTATE_270:
                                returnedBitmap = BitmapUtils.rotateImage(bitmap, 270);
                                // Free up the memory
                                bitmap.recycle();
                                bitmap = null;
                                break;

                            default:
                                returnedBitmap = bitmap;
                        }

                        Bitmap w_bmpSizeLimited = Bitmap.createScaledBitmap(returnedBitmap, Constants.PROFILE_IMAGE_SIZE, Constants.PROFILE_IMAGE_SIZE, true);

                        BitmapUtils.saveOutput(outFile, w_bmpSizeLimited);

                        _photoPath = outFile.getAbsolutePath();

                        addImage(_photoPath);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }

            case Constants.PICK_FROM_IMAGES:

                if (resultCode == RESULT_OK){
                    ArrayList<String> paths = data.getStringArrayListExtra(Constants.KEY_IMAGES);
                    addImages(paths);
                }

                break;
            case Constants.PICK_FROM_ALBUM:

                if (resultCode == RESULT_OK) {

                    _imageCaptureUri = data.getData();
                }


            case Constants.PICK_FROM_CAMERA:
            {

                if (resultCode == RESULT_OK) {
                    try {

                        _photoPath = BitmapUtils.getRealPathFromURI(this, _imageCaptureUri);


                        new AsyncTask<Void, Void, Void>() {
                            @Override
                            protected Void doInBackground(Void... params) {

                                //We send the message here.
                                //You should also check if the username is valid here.
                                try {

                                    selectPhoto();

                                } catch (Exception e) {

                                }
                                return null;
                            }

                            @Override
                            protected void onPostExecute(Void aVoid) {
                                super.onPostExecute(aVoid);

                                addImage(_photoPath);                                       // add photo  :

                            }

                        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
        }
    }

    private void selectPhoto() {

        try {
            Bitmap returnedBitmap = BitmapUtils.loadOrientationAdjustedBitmap(_photoPath);

            Log.d("=======", _photoPath) ;

            File outFile = getOutputMediaFile();

            BitmapUtils.saveOutput(outFile , returnedBitmap);
            returnedBitmap.recycle();
            returnedBitmap = null;

            _photoPath = outFile.getAbsolutePath();
        }    catch (Exception e) {

            e.printStackTrace();
        }
    }

    public void deleteAllTempImages() {

        File mediaStorageDir = new File(
                Environment.getExternalStorageDirectory() + "/android/data/"
                        + this.getPackageName() + "/myprofile");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return;
            }
        } else {
            String[] children = mediaStorageDir.list();
            for (int i = 0; i < children.length; i++)
            {
                new File(mediaStorageDir, children[i]).delete();
            }
        }
    }

    public File getOutputMediaFile() {

        File mediaStorageDir = new File(
                Environment.getExternalStorageDirectory() + "/android/data/"
                        + this.getPackageName() + "/myprofile");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        long random = new Date().getTime();

        File mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "temp" + random + ".png");

        return mediaFile;
    }


    private void onBack() {
        finish();
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.txv_cancel:
                onBack();
                break;

            case R.id.txv_save:
                onSave();
                break;

            case R.id.imv_camera:
                onSelectPhoto();
                break;
        }
    }
}
