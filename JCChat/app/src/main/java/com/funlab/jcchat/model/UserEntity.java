package com.funlab.jcchat.model;

import java.util.ArrayList;

/**
 * Created by HGS on 12/16/2015.
 */
public class UserEntity {

    private int _idx = 0;
    private String _name = "";
    private String _email = "";
    private String _password = "";
    private String _label = "";
    private String _bgUrl = "";
    private String _photoUrl = "";
    private String _phone_number = "";
    private int _allowFriend = 1;

    private ArrayList<FriendEntity> _friendList = new ArrayList<FriendEntity>();
    private ArrayList<RoomEntity> _roomList = new ArrayList<RoomEntity>();

    ArrayList<MomentEntity> _moment_list = new ArrayList<>();

    public int get_idx() {
        return _idx;
    }

    public void set_idx(int _idx) {
        this._idx = _idx;
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public String get_password() {
        return _password;
    }

    public void set_password(String _password) {
        this._password = _password;
    }

    public String get_label() {
        return _label;
    }

    public void set_label(String _label) {
        this._label = _label;
    }

    public String get_bgUrl() {
        return _bgUrl;
    }

    public void set_bgUrl(String _bgUrl) {
        this._bgUrl = _bgUrl;
    }

    public String get_photoUrl() {
        return _photoUrl;
    }

    public void set_photoUrl(String _photoUrl) {
        this._photoUrl = _photoUrl;
    }

    public String get_phone_number() {
        return _phone_number;
    }

    public void set_phone_number(String _phone_number) {
        this._phone_number = _phone_number;
    }

    public String get_email() {
        return _email;
    }

    public void set_email(String _email) {
        this._email = _email;
    }

    public int is_allowFriend() {
        return _allowFriend;
    }

    public void set_allowFriend(int _allowFriend) {
        this._allowFriend = _allowFriend;
    }

    public ArrayList<FriendEntity> get_friendList() {
        return _friendList;
    }

    public FriendEntity getFriend(int idx) {

        for (FriendEntity friend : _friendList) {

            if (friend.get_idx() == idx)
                return friend;
        }

        return null;
    }

    public boolean isFriend(int idx) {

        for (FriendEntity friend : _friendList) {

            if (friend.get_idx() == idx)
                return true;
        }

        return false;
    }

    public ArrayList<RoomEntity> get_roomList() { return _roomList; }

    public RoomEntity getRoom(String roomName) {

        for (RoomEntity room : _roomList) {
            if (room.get_name().equals(roomName))
                return room;
        }

        return null;
    }

    public boolean isBlockFriend(int friend) {

        for (FriendEntity entity : get_friendList()) {

            if (entity.get_idx() == friend && entity.get_blockStatus() == 0)
                return true;
        }

        return false;
    }

    public ArrayList<MomentEntity> get_moment_list() {return _moment_list;}

    public void set_moment_list(ArrayList<MomentEntity> _moment_list){this._moment_list = _moment_list;}

}
