package com.funlab.jcchat.main;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.funlab.jcchat.Base.CommonActivity;
import com.funlab.jcchat.R;
import com.funlab.jcchat.Utils.Database;
import com.funlab.jcchat.adapter.ChatListEditAdapter;
import com.funlab.jcchat.commons.Commons;
import com.funlab.jcchat.model.RoomEntity;
import com.funlab.jcchat.model.UserEntity;

import java.util.ArrayList;

public class ChatListEditActivity extends CommonActivity implements View.OnClickListener {

    private TextView ui_txvClose;
    private Button ui_btnGoOut;

    private ListView _chatEditList = null;
    private ChatListEditAdapter _chatListEditAdapter = null;

    private UserEntity _user;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_list_edit);

        _user = Commons.g_user;

        loadLayout();
    }

    public void loadLayout(){

        ui_txvClose = (TextView)findViewById(R.id.txv_close);
        ui_txvClose.setOnClickListener(this);

        ui_btnGoOut = (Button)findViewById(R.id.btn_go_out);
        ui_btnGoOut.setOnClickListener(this);

        _chatEditList = (ListView)findViewById(R.id.liv_chat_edit);
        _chatListEditAdapter = new ChatListEditAdapter(this);
        _chatEditList.setAdapter(_chatListEditAdapter);
    }


    public void showGoOutPopup() {

        int counter = 0;
        for (RoomEntity room :_user.get_roomList()) {
            if (room.is_isSelected()) {
                counter++;
            }
        }

        if (counter == 0)
            return;

        AlertDialog alertDialog = new AlertDialog.Builder(_context).create();

        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(getString(R.string.goout_confirm));

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,
                _context.getString(R.string.ok),

                new android.content.DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        gooutRoom();
                    }
                });

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE,
                getString(R.string.cancel),

                new android.content.DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        alertDialog.show();
    }

    public void gooutRoom() {

        ArrayList<RoomEntity> toDelete = new ArrayList<RoomEntity>();

        for (RoomEntity room :_user.get_roomList()) {

            if (room.is_isSelected()) {
                Database.deleteRoom(room);
                toDelete.add(room);
            }
        }

        _user.get_roomList().removeAll(toDelete);

        _chatListEditAdapter.notifyDataSetChanged();
    }

    public void onClick(View view){

        switch (view.getId()){
            case R.id.txv_close:
                finish();
                break;
            case R.id.btn_go_out:
                showGoOutPopup();
                break;
        }
    }


}
