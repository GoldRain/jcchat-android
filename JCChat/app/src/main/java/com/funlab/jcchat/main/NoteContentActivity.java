package com.funlab.jcchat.main;

import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.View;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.funlab.jcchat.Base.CommonActivity;
import com.funlab.jcchat.JCChatApplication;
import com.funlab.jcchat.R;
import com.funlab.jcchat.commons.Constants;
import com.funlab.jcchat.commons.ReqConst;
import com.funlab.jcchat.model.NoteEntity;

import org.json.JSONException;
import org.json.JSONObject;

public class NoteContentActivity extends CommonActivity implements View.OnClickListener {

    private TextView ui_txvNoteTitle;
    private TextView ui_txvNoteContent;

    private NoteEntity _note;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_content);

        _note = (NoteEntity) getIntent().getSerializableExtra(Constants.KEY_NOTE);

        loadLayout();
    }

    public void loadLayout(){

        TextView txvClose = (TextView)findViewById(R.id.txv_close);
        txvClose.setOnClickListener(this);

        ui_txvNoteTitle = (TextView) findViewById(R.id.txv_note_tile);
        ui_txvNoteContent = (TextView) findViewById(R.id.txv_note_content);

        String fullString = _note.get_title() + " " + _note.get_date();
        SpannableStringBuilder builder = new SpannableStringBuilder(fullString);
        int color = _context.getResources().getColor(R.color.colorPrimary);
        builder.setSpan(new ForegroundColorSpan(color), _note.get_title().length() + 1, _note.get_title().length() + 1 + _note.get_date().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.setSpan(new RelativeSizeSpan(0.8f), _note.get_title().length() + 1, _note.get_title().length() + 1 + _note.get_date().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ui_txvNoteTitle.append(builder);

        TextView txvList = (TextView) findViewById(R.id.txv_list);
        txvList.setOnClickListener(this);

        getNoteCotent();

    }

    public void getNoteCotent() {

        showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_GETNOTECONTENT;
        String params = String.format("/%d", _note.get_idx());
        url += params;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {

                parseNoteResponse(json);
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                showAlertDialog(getString(R.string.error));
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        JCChatApplication.getInstance().addToRequestQueue(stringRequest, url);
    }


    public void parseNoteResponse(String json){

        try{

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                String content = response.getString(ReqConst.RES_NOTECONTENT);
                ui_txvNoteContent.setText(content);
            }

        }catch (JSONException e){
            e.printStackTrace();
            showAlertDialog(getString(R.string.error));
        }

        closeProgress();
    }


    public void onClick(View view){

        switch (view.getId()){
            case R.id.txv_close:
            case R.id.txv_list:
                finish();
                break;

        }
    }


}
