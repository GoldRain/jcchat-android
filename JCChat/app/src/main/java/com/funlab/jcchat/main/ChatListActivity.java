package com.funlab.jcchat.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.funlab.jcchat.Base.CommonTabActivity;
import com.funlab.jcchat.R;
import com.funlab.jcchat.adapter.ChatListAdapter;

import java.util.Locale;

public class ChatListActivity extends CommonTabActivity implements View.OnClickListener {

    public static final int CODE_ROOM = 100;
    public static final int CODE_CHAT = 101;

    private ImageView ui_imvFriend, ui_imvChatting, ui_imvAddFriend, ui_imvMyInfo, ui_imvSelectFriend;
    private TextView ui_txvEdit;

    private ListView _chatList = null;
    private ChatListAdapter _chatListAdapter = null;

    private EditText ui_edtSearch;
    LinearLayout lyt_footer_friend, lyt_footer_chat, lyt_footer_addFriend, lyt_footer_info;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatlist);

        loadLayout();
    }

    public void loadLayout(){

        lyt_footer_friend = (LinearLayout)findViewById(R.id.lyt_footer_friend);
        lyt_footer_chat = (LinearLayout)findViewById(R.id.lyt_footer_chat);
        lyt_footer_addFriend = (LinearLayout)findViewById(R.id.lyt_footer_addFriend);
        lyt_footer_info = (LinearLayout)findViewById(R.id.lyt_footer_info);

        ui_imvFriend = (ImageView)findViewById(R.id.imv_friend);
        ui_imvFriend.setOnClickListener(this);

        ui_imvChatting = (ImageView)findViewById(R.id.imv_chatting);
        ui_imvChatting.setOnClickListener(this);
        ui_imvChatting.setSelected(true);

        TextView txvChatting = (TextView) findViewById(R.id.txv_chatting);
        txvChatting.setSelected(true);

        ui_imvAddFriend = (ImageView)findViewById(R.id.imv_add_friend);
        ui_imvAddFriend.setOnClickListener(this);

        ui_imvMyInfo = (ImageView)findViewById(R.id.imv_my_info);
        ui_imvMyInfo.setOnClickListener(this);

        ui_txvEdit = (TextView)findViewById(R.id.txv_Edit);
        ui_txvEdit.setOnClickListener(this);

        ui_imvSelectFriend = (ImageView)findViewById(R.id.imv_add_chatting);
        ui_imvSelectFriend.setOnClickListener(this);

        ui_txvUnread = (TextView) findViewById(R.id.txv_unread);
        setUnRead();

        ui_edtSearch = (EditText)findViewById(R.id.edt_search);
        ui_edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = ui_edtSearch.getText().toString().toLowerCase(Locale.getDefault());
                _chatListAdapter.filter(text);
            }
        });


        ui_edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                switch (actionId) {
                    case EditorInfo.IME_ACTION_SEARCH:
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(ui_edtSearch.getWindowToken(), 0);
                        return true;

                    default:
                        return false;
                }
            }
        });

        _chatList = (ListView)findViewById(R.id.liv_chat);
        _chatListAdapter = new ChatListAdapter(this);

        _chatList.setAdapter(_chatListAdapter);

        setFooter(getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.blue), getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.colorPrimary));
    }

    public void setFooter(int a, int b, int c, int d){

        lyt_footer_friend.setBackgroundColor(a);
        lyt_footer_chat.setBackgroundColor(b);
        lyt_footer_addFriend.setBackgroundColor(c);
        lyt_footer_info.setBackgroundColor(d);
    }



    public void refresh() {
        _chatListAdapter.refresh();
    }


    public void onClick(View view){

        switch (view.getId()){

            case R.id.imv_friend:
                gotoFriendActivity();
                break;
            case R.id.imv_chatting:
                break;
            case R.id.imv_add_friend:
                gotoAddFriendActivity();
                break;
            case R.id.imv_my_info:
                gotoMyInfoActivity();
                break;
            case R.id.txv_Edit:
                gotoChatListEditActivity();
                break;
            case R.id.imv_add_chatting:
                gotoSelectFriendActivity();
                break;
        }
    }

    public void gotoFriendActivity(){

        Intent intent = new Intent(this, FriendActivity.class);
        startActivity(intent);
        overridePendingTransition(0,0);
        finish();
    }

    public void gotoAddFriendActivity(){

        Intent intent = new Intent(this, AddFriendActivity.class);
        startActivity(intent);
        overridePendingTransition(0,0);
        finish();
    }

    public void gotoMyInfoActivity(){

        Intent intent = new Intent(this, MyInformationActivity.class);
        startActivity(intent);
        overridePendingTransition(0,0);
        finish();
    }

    public void gotoChatListEditActivity(){

        Intent intent = new Intent(this, ChatListEditActivity.class);
        startActivityForResult(intent, CODE_ROOM);
    }

    public void gotoSelectFriendActivity(){

        Intent intent = new Intent(this, FriendSelectActivity.class);
        startActivityForResult(intent, CODE_ROOM);

    }

    @Override
    protected void onResume() {

        super.onResume();

        _chatListAdapter.refresh();
        setUnRead();
    }

}
