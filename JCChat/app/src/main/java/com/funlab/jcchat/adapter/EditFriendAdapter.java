package com.funlab.jcchat.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.funlab.jcchat.JCChatApplication;
import com.funlab.jcchat.R;
import com.funlab.jcchat.Utils.CirculaireNetworkImageView;
import com.funlab.jcchat.Utils.HangulUtils;
import com.funlab.jcchat.model.FriendEntity;

import java.util.ArrayList;

/**
 * Created by HGS on 12/14/2015.
 */
public class EditFriendAdapter extends BaseAdapter {

    private Context _context = null;
    private ArrayList<FriendEntity> _editFriDatas = new ArrayList<FriendEntity>();
    private ArrayList<FriendEntity> _editFriAllDatas = new ArrayList<>();

    private ImageLoader _imageLoader;

    public EditFriendAdapter(Context _context){

        super();
        this._context = _context;

        _imageLoader = JCChatApplication.getInstance().getImageLoader();
    }

    @Override
    public int getCount(){

        return _editFriDatas.size();
    }

    @Override
    public Object getItem(int position){

        return _editFriDatas.get(position);
    }

    @Override
    public long getItemId(int position){

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        EditFriendHolder editFriendHolder;

        if (convertView == null) {
            editFriendHolder = new EditFriendHolder();

            LayoutInflater inflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.edit_friend_list_item, null);

            editFriendHolder.imvEditFriPhoto = (CirculaireNetworkImageView)convertView.findViewById(R.id.imv_editFriPhoto);
            editFriendHolder.txvEditFriName = (TextView)convertView.findViewById(R.id.txv_editFriName);
            editFriendHolder.imvEditFriState = (ImageView)convertView.findViewById(R.id.imv_editFirState);
            editFriendHolder.imvEditFriState.setSelected(false);

            convertView.setTag(editFriendHolder);
        }else {
            editFriendHolder = (EditFriendHolder)convertView.getTag();
        }

        final FriendEntity friend = _editFriDatas.get(position);

        editFriendHolder.txvEditFriName.setText(friend.get_name());

        if (friend.get_photoUrl().length() > 0)
            editFriendHolder.imvEditFriPhoto.setImageUrl(friend.get_photoUrl(), _imageLoader);
        else
            editFriendHolder.imvEditFriPhoto.setImageResource(R.drawable.bg_non_profile1);

        final ImageView imvState = editFriendHolder.imvEditFriState;
        imvState.setSelected(friend.is_isSelected());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imvState.setSelected(!imvState.isSelected());
                friend.set_isSelected(imvState.isSelected());
            }
        });

        return convertView;
    }

    public void addItem(FriendEntity entity){

        _editFriAllDatas.add(entity);
    }

    public void clearAll() {

        _editFriAllDatas.clear();
    }

    public void initEditFriendData(){

        _editFriDatas.clear();
        _editFriDatas.addAll(_editFriAllDatas);
    }

    public void filter(String charText){

        charText = charText.toLowerCase();

        _editFriDatas.clear();

        if(charText.length() == 0){
            _editFriDatas.addAll(_editFriAllDatas);
        }else {

            for (FriendEntity friend : _editFriAllDatas){

                String value = friend.get_name().toLowerCase();

                if(value.contains(charText)
                        || HangulUtils.isHangulInitialSound(value, charText)){
                    _editFriDatas.add(friend);
                }
            }
        }
        notifyDataSetChanged();
    }

    public class EditFriendHolder {

        public CirculaireNetworkImageView imvEditFriPhoto;
        public TextView txvEditFriName;
        public ImageView imvEditFriState;
    }

}
