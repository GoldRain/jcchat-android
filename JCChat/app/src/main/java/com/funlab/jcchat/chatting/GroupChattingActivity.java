package com.funlab.jcchat.chatting;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.baoyz.widget.PullRefreshLayout;
import com.funlab.jcchat.Base.CommonActivity;
import com.funlab.jcchat.JCChatApplication;
import com.funlab.jcchat.R;
import com.funlab.jcchat.Utils.BitmapUtils;
import com.funlab.jcchat.Utils.Database;
import com.funlab.jcchat.Utils.MediaRealPathUtil;
import com.funlab.jcchat.commons.Commons;
import com.funlab.jcchat.commons.Constants;
import com.funlab.jcchat.commons.ReqConst;
import com.funlab.jcchat.filepanda.FileListActivity;
import com.funlab.jcchat.logger.Logger;
import com.funlab.jcchat.main.AddOrBlockFriendActivity;
import com.funlab.jcchat.main.FriendInviteActivity;
import com.funlab.jcchat.model.FriendEntity;
import com.funlab.jcchat.model.RoomEntity;
import com.funlab.jcchat.model.UserEntity;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.chat.Chat;
import org.jivesoftware.smack.chat.ChatManager;
import org.jivesoftware.smack.packet.Message;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by HGS on 12/11/2015.
 */
public class GroupChattingActivity extends CommonActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    private final int CODE_ADDBLOCK = 1;

    private TextView ui_txvTitle;

    private LinearLayout ui_lytMorebar;

    private FrameLayout ui_fltFriendBar;
    private LinearLayout ui_lytNoFriend;
    private LinearLayout ui_lytAddFriendOnly;
    private LinearLayout ui_lytReleaseFriendOnly;

    private ListView ui_lstChatting;
    private GroupChattingAdapter _chattingAdapter;

    private PullRefreshLayout ui_pullRefreshLayout;

    private EditText ui_edtMessage;
    private ImageView ui_imvMore;

    private Uri _imageCaptureUri;
    private String _capturePath = "";

    GroupChatManager _groupChat = null;

    RoomEntity _roomEntity = null;
    String _roomName = "";
    String _roomParticiants = "";
    String _roomDisplayName = "";

    int _recentLoadCounter = 1;

    private UserEntity _user;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        _user = Commons.g_user;

        Commons.g_chattingActivity = this;

        setContentView(R.layout.activity_chatting);

        String roomname = getIntent().getStringExtra(Constants.KEY_ROOM);

        _roomEntity = _user.getRoom(roomname);

        _roomName = _roomEntity.get_name();
        _roomDisplayName = _roomEntity.get_displayName();
        _roomParticiants = _roomEntity.makeRoomName();

        updateBadgeCount(Commons.g_badgCount - _roomEntity.get_recentCounter());
        _roomEntity.init_recentCounter();

        _groupChat = new GroupChatManager(this, ConnectionMgrService.mConnection, _roomName);
        enterRoom();

        loadLayout();
    }


    public void enterRoom() {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                // We send the message here.
                // You should also check if the username is valid here.
                try {
                    _groupChat.enterRoom(String.valueOf(_user.get_idx()));
                } catch (Exception e) {
                    showToast(getString(R.string.chatting_error));
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                if (!_groupChat.isJoined) {
                    enterRoomRetry();
                    showToast(getString(R.string.chatting_error));
                } else {
                    showToast(getString(R.string.chatting_success));
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void enterRoomRetry() {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                // We send the message here.
                // You should also check if the username is valid here.
                try {
                    _groupChat.enterRoom(String.valueOf(_user.get_idx()));
                } catch (Exception e) {
                    showToast(getString(R.string.error));
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                if (!_groupChat.isJoined)
                    showToast(getString(R.string.chatting_error));
                else {
                    showToast(getString(R.string.chatting_success));
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }


    public void reenterRoom() {

        try {
            _groupChat.reenterRoom(String.valueOf(_user.get_idx()));
        } catch (Exception e) {
            showToast(getString(R.string.chatting_error));
        }
    }

    public void checkConnection() {

        if (Commons.g_xmppService == null || !Commons.g_xmppService.isConnected || !_groupChat.isJoined) {
            showToast(getString(R.string.chatting_connecting));
        }
    }

    public void loadLayout() {

        ui_txvTitle = (TextView) findViewById(R.id.chatting_title);
        if (_roomDisplayName != null)
            ui_txvTitle.setText(_roomDisplayName);
        else
            ui_txvTitle.setText(getString(R.string.group_chatting));

        ui_fltFriendBar = (FrameLayout) findViewById(R.id.flt_friendbar);

        ui_lytReleaseFriendOnly = (LinearLayout) findViewById(R.id.lyt_releaseFriendOnly);
        ui_lytReleaseFriendOnly.setOnClickListener(this);

        ui_lytAddFriendOnly = (LinearLayout) findViewById(R.id.lyt_addFriendOnly);
        LinearLayout lytAddFriendOnlyButton = (LinearLayout) findViewById(R.id.lyt_addFriendOnlyButton);
        lytAddFriendOnlyButton.setOnClickListener(this);

        ui_lytNoFriend = (LinearLayout) findViewById(R.id.lyt_nofriend);
        LinearLayout lytAddFriend = (LinearLayout) findViewById(R.id.lyt_addFriend);
        lytAddFriend.setOnClickListener(this);

        LinearLayout lytBlockFriend = (LinearLayout) findViewById(R.id.lyt_blockFriend);
        lytBlockFriend.setOnClickListener(this);

        ImageView imvBack = (ImageView) findViewById(R.id.imv_back);
        imvBack.setOnClickListener(this);

        ui_imvMore = (ImageView) findViewById(R.id.imv_more);
        ui_imvMore.setOnClickListener(this);

        ui_edtMessage = (EditText) findViewById(R.id.edt_message);

        TextView imvSend = (TextView) findViewById(R.id.imv_send);
        imvSend.setOnClickListener(this);

        ui_lytMorebar = (LinearLayout) findViewById(R.id.lyt_morebar);
        ui_lytMorebar.setVisibility(View.GONE);

        ImageView imvInvite = (ImageView) findViewById(R.id.imv_invite);
        imvInvite.setOnClickListener(this);

        ImageView imvAlbum = (ImageView) findViewById(R.id.imv_album);
        imvAlbum.setOnClickListener(this);

        ImageView imvVideo = (ImageView) findViewById(R.id.imv_video);
        imvVideo.setOnClickListener(this);

        ImageView imvCamera = (ImageView) findViewById(R.id.imv_camera);
        imvCamera.setOnClickListener(this);

        ImageView imvFile = (ImageView) findViewById(R.id.imv_file);
        imvFile.setOnClickListener(this);

        ui_lstChatting = (ListView) findViewById(R.id.lst_chatting);

        _chattingAdapter = new GroupChattingAdapter(this);
        ui_lstChatting.setAdapter(_chattingAdapter);

        ui_pullRefreshLayout = (PullRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        ui_pullRefreshLayout.setRefreshStyle(PullRefreshLayout.STYLE_RING);
        ui_pullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                _recentLoadCounter++;
                ui_lstChatting.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_NORMAL);
                startRefreshThread();
            }

        });

        ui_lstChatting.setOnItemClickListener(this);

        checkFriendBar();

        new Thread(new Runnable() {
            @Override
            public void run() {
                getFirstChattingList();
            }
        }).start();

        getRoomInfo();

    }

    // add or block or release friend
    public void checkFriendBar() {

        ui_fltFriendBar.setVisibility(View.GONE);
        ui_lytNoFriend.setVisibility(View.GONE);
        ui_lytAddFriendOnly.setVisibility(View.GONE);
        ui_lytReleaseFriendOnly.setVisibility(View.GONE);
        ui_edtMessage.setEnabled(true);
        ui_imvMore.setEnabled(true);

        if (_roomEntity.isGroup()) {

            // if not friend exists, add friend only
            if (checkNotRegisterdFriend(_roomEntity)) {
                ui_fltFriendBar.setVisibility(View.VISIBLE);
                ui_lytAddFriendOnly.setVisibility(View.VISIBLE);
            }

        } else {

            if (checkNotRegisterdFriend(_roomEntity)) {
                ui_fltFriendBar.setVisibility(View.VISIBLE);
                ui_lytNoFriend.setVisibility(View.VISIBLE);
            } else if (isBlockFriend(_roomEntity)){
                ui_fltFriendBar.setVisibility(View.VISIBLE);
                ui_lytReleaseFriendOnly.setVisibility(View.VISIBLE);

                // disable chatting
                ui_edtMessage.setEnabled(false);
                ui_imvMore.setEnabled(false);
            }
        }

    }

    public void getFirstChattingList() {

        final ArrayList<GroupChatItem> recents = Database.getRecentMessage(_roomName, _recentLoadCounter);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                _chattingAdapter.addItems(recents);
                _chattingAdapter.notifyDataSetChanged();
            }
        });
    }

    public void startRefreshThread() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                getChattingList();
            }
        }).start();
    }

    public void getChattingList() {

        final ArrayList<GroupChatItem> recents = Database.getRecentMessage(_roomName, _recentLoadCounter);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                _chattingAdapter.clearAll();
                _chattingAdapter.addItems(recents);
                _chattingAdapter.notifyDataSetChanged();
                ui_pullRefreshLayout.setRefreshing(false);
//                ui_lstChatting.smoothScrollToPosition(0);

            }
        });
    }

    public void updateRoom() {

        _roomParticiants = _roomEntity.get_participants();
        _roomDisplayName = _roomEntity.get_displayName();
        ui_txvTitle.setText(_roomDisplayName);
    }

    public void getRoomInfo(){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_GETROOMINFO;

        String params = String.format("/%s", _roomEntity.get_participants());
        url += params;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                parseRoomInfoResponse(json);
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        JCChatApplication.getInstance().addToRequestQueue(stringRequest, url);

    }

    public void parseRoomInfoResponse(String json){

        try {

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                JSONArray participants = response.getJSONArray(ReqConst.RES_USERLIST);

                // get participants data
                for (int i = 0; i < participants.length(); i++) {

                    JSONObject friend = (JSONObject) participants.get(i);

                    int idx = friend.getInt(ReqConst.RES_ID);

                    FriendEntity participant = _roomEntity.getParticipant(idx);
                    if (participant != null) {
                        participant.set_name(friend.getString(ReqConst.RES_NAME));
                        participant.set_label(friend.getString(ReqConst.RES_LABEL));
                        participant.set_photoUrl(friend.getString(ReqConst.RES_PHOTO_URL));
                        participant.set_bgUrl(friend.getString(ReqConst.RES_BG_URL));
                    }
                }

            }

        } catch (JSONException e){
            e.printStackTrace();
        }

    }


    public void addChat(int sender, String body) {

        _roomEntity.set_ownerIdx(sender);

        checkFriendBar();

        ui_lstChatting.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);

        GroupChatItem chatItem = new GroupChatItem(sender, _roomName, body);

        _chattingAdapter.addItem(chatItem);
        _chattingAdapter.notifyDataSetChanged();

    }

    public void addChat(int sender, String message, GroupChatItem.StatusType type) {

        ui_lstChatting.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);

        GroupChatItem chatItem = new GroupChatItem(sender, _roomName, message, type);

        _chattingAdapter.addItem(chatItem);
        _chattingAdapter.notifyDataSetChanged();
    }

    public RoomEntity get_roomEntity() {

        return _roomEntity;
    }

    public ArrayList<Integer> getParticipants() {

        ArrayList<Integer> returned = new ArrayList<Integer>();

        for (FriendEntity friend : _roomEntity.get_participantList()) {

            int idx = friend.get_idx();

            if (idx != _user.get_idx())
                returned.add(Integer.valueOf(idx));
        }

        return returned;
    }

    // basic send message
    // ROOM#[roomname]:[roomparticipants]:[sendername]#message#time
    // ROOM#1_2:1_2_3:에스오#message#time, ROOM#1_2:1_2_3:에스오#FILE#url#filename#time
    public void sendTextMessage(final String chat_message) {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                // We send the message here.
                // You should also check if the username is valid here.
                try {

                    Logger.d("TEST", "send text doinbackground:" + chat_message);

                    // send both group message and message for outside user
                    String fullMessage = getRoomInfoString() + chat_message + Constants.KEY_SEPERATOR + getTimeString();
                    // send group message for inside user
                    _groupChat.sendMessage(fullMessage);

                    // update room recent conversation
                    _roomEntity.set_recentContent(getMessage(fullMessage));
                    _roomEntity.set_recentTime(getDisplayTime(fullMessage));
                    Database.updateRoom(_roomEntity);

                    // write message into db
                    GroupChatItem chatItem = new GroupChatItem(_user.get_idx(), _roomEntity.get_name(), fullMessage);
                    Database.createMessage(chatItem);

                    // send message for outside user
                    sendTextMessage(getParticipants(), fullMessage);

                } catch (SmackException.NotConnectedException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                String fullMessage = getRoomInfoString() + chat_message + Constants.KEY_SEPERATOR + getTimeString();

                // for image, video, file added already
                if (getType(fullMessage) == GroupChatItem.ChatType.TEXT)
                    addChat(_user.get_idx(), fullMessage);

                super.onPostExecute(aVoid);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    // send message for outside users
    public void sendTextMessage(ArrayList<Integer> participants, String chat_message) {

        for (Integer participant : participants) {

            int idx = participant.intValue();
            sendTextMessage(Commons.idxToAddr(idx), chat_message);
        }
    }

    // send message for outside individual user
    public void sendTextMessage(String address, String chat_message) {
        // Listview is updated with our new message
        ChatManager chatManager = ChatManager.getInstanceFor(ConnectionMgrService.mConnection);

        final Chat newChat = chatManager.createChat(address);

        final Message message = new Message();
        message.setBody(chat_message);

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                // We send the message here.
                // You should also check if the username is valid here.
                try {
                    newChat.sendMessage(message);
                } catch (SmackException.NotConnectedException e) {
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void sendImage(String imgPath, String filename) {

        File file = new File(imgPath);

        if (!file.exists()) return;

        if (file.length() > Constants.LIMIT_FILE) {
            showAlertDialog(getString(R.string.file_overflow));
            return;
        }
        showToast(getString(R.string.file_uploading));

        String chat_message = Constants.KEY_IMAGE_MARKER + imgPath + Constants.KEY_SEPERATOR + filename;
        String fullMessage = getRoomInfoString() + chat_message + Constants.KEY_SEPERATOR + getTimeString();
        addChat(_user.get_idx(), fullMessage, GroupChatItem.StatusType.START_UPLOADING);

    }

    public void sendVideo(String videoPath, String filename) {

        File file = new File(videoPath);

        if (!file.exists()) return;

        if (file.length() > Constants.LIMIT_FILE) {
            showAlertDialog(getString(R.string.file_overflow));
            return;
        }

        showToast(getString(R.string.file_uploading));

        String chat_message = Constants.KEY_VIDEO_MARKER + videoPath + Constants.KEY_SEPERATOR + filename;
        String fullMessage = getRoomInfoString() + chat_message + Constants.KEY_SEPERATOR + getTimeString();
        addChat(_user.get_idx(), fullMessage, GroupChatItem.StatusType.START_UPLOADING);

    }


    public void sendFile(String filePath, String filename) {

        File file = new File(filePath);

        if (!file.exists()) return;

        if (file.length() > Constants.LIMIT_FILE) {
            showAlertDialog(getString(R.string.file_overflow));
            return;
        }

        showToast(getString(R.string.file_uploading));

        String chat_message = Constants.KEY_FILE_MARKER + filePath + Constants.KEY_SEPERATOR + filename;
        String fullMessage = getRoomInfoString() + chat_message + Constants.KEY_SEPERATOR + getTimeString();
        addChat(_user.get_idx(), fullMessage, GroupChatItem.StatusType.START_UPLOADING);
    }


    public void onSuccessUpload(String fileurl, String filename, GroupChatItem.ChatType type) {

        String marker = Constants.KEY_IMAGE_MARKER;

        if (type == GroupChatItem.ChatType.VIDEO)
            marker = Constants.KEY_VIDEO_MARKER;
        else if (type == GroupChatItem.ChatType.FILE)
            marker = Constants.KEY_FILE_MARKER;

        String message = marker + fileurl + Constants.KEY_SEPERATOR + filename;
        sendTextMessage(message);
    }

    public void onFailUpload() {

        sendTextMessage(getString(R.string.transfer_fail));
    }

    public String getRoomInfoString() {

        return Constants.KEY_ROOM_MARKER + _roomName + ":" + _roomParticiants + ":" + _user.get_name() + Constants.KEY_SEPERATOR;
    }

    // 20150101,13:30:26 or 20160103,6:07:06
    public String getTimeString() {

        Calendar now = Calendar.getInstance();

        int year = now.get(Calendar.YEAR);
        int month = now.get(Calendar.MONTH) + 1;
        int date = now.get(Calendar.DATE);

        String time = String.format("%d%02d%02d", year, month, date);

        int hour = now.get(Calendar.HOUR_OF_DAY);
        int min = now.get(Calendar.MINUTE);
        int sec = now.get(Calendar.SECOND);

        time += String.format(",%d:%02d:%02d", hour, min, sec);

        return time;
    }

    public String getDisplayTime(String body) {

        String fulldatetime = body.substring(body.lastIndexOf(Constants.KEY_SEPERATOR) + 1);

        String date = fulldatetime.split(",")[0];
        String fulltime = fulldatetime.split(",")[1];

        String time = fulltime.substring(0, fulltime.lastIndexOf(":"));

        int hour = Integer.valueOf(time.split(":")[0]);
        String min = time.split(":")[1];

        if (hour < 12) {
            time = Commons.g_chattingActivity.getString(R.string.am) + " " + time ;
        } else {
            hour -= 12;
            if (hour == 0)
                hour = 12;
            time = Commons.g_chattingActivity.getString(R.string.pm) + " " + hour + ":" + min;
        }

        return time;
    }

    // ROOM#1_2#message#time, ROOM#1_2#FILE#message#time
    public String getMessage(String body) {

        String body1 = body.substring(body.indexOf(Constants.KEY_SEPERATOR) + 1, body.lastIndexOf(Constants.KEY_SEPERATOR));
        String message = body1.substring(body1.indexOf(Constants.KEY_SEPERATOR) + 1);

        if (getType(body) != GroupChatItem.ChatType.TEXT) {
            message = Commons.g_currentActivity.getString(R.string.transfer_file);
        }

        return message;
    }

    public GroupChatItem.ChatType getType(String body) {

        String body1 = body.substring(body.indexOf(Constants.KEY_SEPERATOR) + 1);
        body1 = body1.substring(body1.indexOf(Constants.KEY_SEPERATOR) + 1);
        GroupChatItem.ChatType type = GroupChatItem.ChatType.TEXT;

        if (body1.startsWith(Constants.KEY_FILE_MARKER))
            type = GroupChatItem.ChatType.FILE;
        else if (body1.startsWith(Constants.KEY_IMAGE_MARKER))
            type = GroupChatItem.ChatType.IMAGE;
        else if (body1.startsWith(Constants.KEY_VIDEO_MARKER))
            type = GroupChatItem.ChatType.VIDEO;

        return type;
    }

    // true if contains not registered friend
    public boolean checkNotRegisterdFriend(RoomEntity roomEntity) {

        for (FriendEntity participant : roomEntity.get_participantList()) {

            if (!_user.isFriend(participant.get_idx())) {
                return true;
            }
        }

        return false;
    }

    public boolean isBlockFriend(RoomEntity roomEntity) {

        for (FriendEntity participant : roomEntity.get_participantList()) {

            if (_user.isFriend(participant.get_idx()) && participant.get_blockStatus() == 0) {
                return true;
            }
        }

        return false;
    }

    public void gotoAddFriend() {

        Intent intent = new Intent(GroupChattingActivity.this, AddOrBlockFriendActivity.class);
        intent.putExtra(Constants.KEY_ADD_BLOCK, Constants.KEY_ADD);
        intent.putExtra(Constants.KEY_ROOM, _roomEntity);
        startActivityForResult(intent, CODE_ADDBLOCK);
    }


    public void gotoBlockFriend() {

        Intent intent = new Intent(GroupChattingActivity.this, AddOrBlockFriendActivity.class);
        intent.putExtra(Constants.KEY_ADD_BLOCK, Constants.KEY_BLOCK);
        intent.putExtra(Constants.KEY_ROOM, _roomEntity);
        startActivityForResult(intent, CODE_ADDBLOCK);
    }


    public void gotoInviteFriend() {

        Intent intent = new Intent(GroupChattingActivity.this, FriendInviteActivity.class);
        intent.putExtra(Constants.KEY_ROOM, _roomEntity.get_name());
        startActivityForResult(intent, Constants.PICK_FROM_INVITE);

    }

    public void doTakeGallery(){

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, Constants.PICK_FROM_ALBUM);
    }

    public void doTakePhotoAction(){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        _capturePath = BitmapUtils.getTempFolderPath() + "photo_temp.jpg";
        _imageCaptureUri = Uri.fromFile(new File(_capturePath));

        intent.putExtra(MediaStore.EXTRA_OUTPUT, _imageCaptureUri);
        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);

    }

    public void doTakeVideo() {

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("video/*");
        startActivityForResult(intent, Constants.PICK_FROM_VIDEO);
    }

    public void doTakeFile() {

        Intent intent = new Intent(GroupChattingActivity.this, FileListActivity.class);
        startActivityForResult(intent, Constants.PICK_FROM_FILE);
    }

    public void showImage(String url, boolean mine) {

        MimeTypeMap myMime = MimeTypeMap.getSingleton();
        Intent newIntent = new Intent(android.content.Intent.ACTION_VIEW);
        String ext = Commons.fileExtFromUrl(url).substring(1);
        String mimeType = myMime.getMimeTypeFromExtension(ext);
        newIntent.setDataAndType(Uri.parse(url), mimeType);

        try {
            startActivity(newIntent);
        } catch (android.content.ActivityNotFoundException e) {
            showToast(getString(R.string.not_support_file));
        }
    }

    public void showFile(String filename) {

        String filepath = BitmapUtils.getDownloadFolderPath() + filename;
        File file = new File(filepath);

        if (!file.exists())
            return;

        MimeTypeMap myMime = MimeTypeMap.getSingleton();
        Intent newIntent = new Intent(android.content.Intent.ACTION_VIEW);
        String ext = Commons.fileExtFromUrl(filepath).substring(1);
        String mimeType = myMime.getMimeTypeFromExtension(ext);
        newIntent.setDataAndType(Uri.parse(filepath), mimeType);

        try {
            startActivity(newIntent);
        } catch (android.content.ActivityNotFoundException e) {
            showToast(getString(R.string.not_support_file));
        }
    }

    public void showDownloadDialog(final GroupChatItem chatItem) {

        final String url = chatItem.getFileUrl();
        final String filename = chatItem.getFilename();

        String filepath = BitmapUtils.getDownloadFolderPath() + filename;
        File file = new File(filepath);

        // already save to file
        if (file.exists()) {
            showFile(filename);
            return;
        }

        AlertDialog alertDialog = new AlertDialog.Builder(_context).create();

        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(filename + " " + getString(R.string.download_confirm));

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,
                _context.getString(R.string.ok),
                new android.content.DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        chatItem.set_status(GroupChatItem.StatusType.START_DOWNLOADING);
                        _chattingAdapter.notifyDataSetChanged();
                    }
                });

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE,
                getString(R.string.cancel),
                new android.content.DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        alertDialog.show();
    }

    public Bitmap saveThumbnail(String filepath) {

        Bitmap thumb = null;

        File file = new File(filepath);

        if (!file.exists())
            return thumb;

        try {
            thumb = ThumbnailUtils.createVideoThumbnail(filepath,
                    MediaStore.Images.Thumbnails.MINI_KIND);

            String filename = BitmapUtils.getVideoThumbFolderPath() + Commons.fileNameWithoutExtFromPath(filepath) + ".jpg";
            File thumbFile = new File(filename);

            if (thumb != null)
                BitmapUtils.saveOutput(thumbFile, thumb);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return thumb;
    }



    public void showAddPopup() {

        AlertDialog alertDialog = new AlertDialog.Builder(_context).create();

        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(getString(R.string.add_confirm));

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,
                _context.getString(R.string.ok),

                new android.content.DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        addFriend();
                    }
                });

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE,
                getString(R.string.cancel),

                new android.content.DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        alertDialog.show();
    }

    public void addFriend() {

        showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_ADDFRIENDLIST;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url , new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {

                parseAddFriendResponse(json);
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                showAlertDialog(getString(R.string.error));
            }
        }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();

                try {
                    params.put(ReqConst.PARAM_ID, String.valueOf(_user.get_idx()));

                    JSONArray friendIds = new JSONArray();

                    for (FriendEntity friendEntity : _roomEntity.get_participantList()) {
                        friendIds.put(String.valueOf(friendEntity.get_idx()));
                    }

                    params.put(ReqConst.PARAM_FRIENDLIST, friendIds.toString());

                } catch (Exception e) {
                }

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        JCChatApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    public void parseAddFriendResponse(String json){

        try{

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                for (FriendEntity friendEntity : _roomEntity.get_participantList()) {
                    _user.get_friendList().add(friendEntity);
                }

                showAlertDialog(getString(R.string.addfriend_success));
            }

        } catch (JSONException e){
            e.printStackTrace();
            showAlertDialog(getString(R.string.error));
        }

        closeProgress();

        checkFriendBar();
    }

    public void showBlockPopup() {

        AlertDialog alertDialog = new AlertDialog.Builder(_context).create();

        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(getString(R.string.blcok_confirm));

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,
                _context.getString(R.string.ok),

                new android.content.DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        blockFriend();
                    }
                });

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE,
                getString(R.string.cancel),

                new android.content.DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        alertDialog.show();
    }

    public void blockFriend() {

        String url = ReqConst.SERVER_URL + ReqConst.REQ_BLOCKFRIENDLIST;

        showProgress();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url , new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {

                parseBlockFriendResponse(json);
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                showAlertDialog(getString(R.string.error));
            }
        }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();

                try {
                    params.put(ReqConst.PARAM_ID, String.valueOf(_user.get_idx()));

                    JSONArray friendIds = new JSONArray();

                    for (FriendEntity friendEntity : _roomEntity.get_participantList()) {
                        friendIds.put(String.valueOf(friendEntity.get_idx()));
                    }

                    params.put(ReqConst.PARAM_FRIENDLIST, friendIds.toString());
                    params.put(ReqConst.PARAM_BLCOKSTATUS, "0");

                } catch (Exception e) {
                }

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        JCChatApplication.getInstance().addToRequestQueue(stringRequest, url);

    }

    public void parseBlockFriendResponse(String json) {

        try{

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                for (FriendEntity friendEntity : _roomEntity.get_participantList()) {
                    friendEntity.set_blockStatus(0);
                    Database.createBlock(friendEntity.get_idx());
                    _user.get_friendList().add(friendEntity);
                }

                showAlertDialog(getString(R.string.blockfriend_success));
            }

        } catch (JSONException e){
            e.printStackTrace();
            showAlertDialog(getString(R.string.error));
        }

        closeProgress();
        checkFriendBar();
    }

    public void showReleasePopup() {

        AlertDialog alertDialog = new AlertDialog.Builder(_context).create();

        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(getString(R.string.release_confirm));

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,
                _context.getString(R.string.ok),

                new android.content.DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        releaseFriend();
                    }
                });

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE,
                getString(R.string.cancel),

                new android.content.DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        alertDialog.show();
    }

    public void releaseFriend() {

        String url = ReqConst.SERVER_URL + ReqConst.REQ_BLOCKFRIENDLIST;

        showProgress();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url , new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {

                parseReleaseFriendResponse(json);
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                showAlertDialog(getString(R.string.error));
            }
        }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();

                try {
                    params.put(ReqConst.PARAM_ID, String.valueOf(_user.get_idx()));

                    JSONArray friendIds = new JSONArray();

                    for (FriendEntity friendEntity : _roomEntity.get_participantList()) {
                        friendIds.put(String.valueOf(friendEntity.get_idx()));
                    }

                    params.put(ReqConst.PARAM_FRIENDLIST, friendIds.toString());
                    params.put(ReqConst.PARAM_BLCOKSTATUS, "1");

                } catch (Exception e) {
                }

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        JCChatApplication.getInstance().addToRequestQueue(stringRequest, url);

    }

    public void parseReleaseFriendResponse(String json) {

        try{

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                for (FriendEntity friendEntity : _roomEntity.get_participantList()) {
                    friendEntity.set_blockStatus(1);
                    Database.deleteBlock(friendEntity.get_idx());
                }
            }

        } catch (JSONException e){
            e.printStackTrace();
            showAlertDialog(getString(R.string.error));
        }

        closeProgress();

        checkFriendBar();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == Constants.PICK_FROM_ALBUM) {

            if (resultCode == RESULT_OK) {

                Uri w_uri = data.getData();

                String w_strPath = MediaRealPathUtil.getPath(this, w_uri);

                if (w_strPath == null) {
                    showToast(getString(R.string.getimage_fail));
                    return;
                }

                String filename = Commons.fileNameWithoutExtFromUrl(w_strPath) + ".jpg";

                Bitmap w_bmpGallery = BitmapUtils.loadOrientationAdjustedBitmap(w_strPath);

                String w_strLimitedImageFilePath = BitmapUtils.getUploadImageFilePath(w_bmpGallery, filename);
                if (w_strLimitedImageFilePath != null) {
                    w_strPath = w_strLimitedImageFilePath;
                }

                sendImage(w_strPath, filename);
            }

        } else if (requestCode == Constants.PICK_FROM_CAMERA) {

            if (resultCode == RESULT_OK) {

                String filename = "IMAGE_" + System.currentTimeMillis() + ".jpg";

                Bitmap w_bmpGallery = BitmapUtils.loadOrientationAdjustedBitmap(_capturePath);

                String w_strFilePath = "";
                String w_strLimitedImageFilePath = BitmapUtils.getUploadImageFilePath(w_bmpGallery, filename);
                if (w_strLimitedImageFilePath != null) {
                    w_strFilePath = w_strLimitedImageFilePath;
                }

                sendImage(w_strFilePath, filename);
            }

        } else if (requestCode == Constants.PICK_FROM_VIDEO) {

            if (resultCode == RESULT_OK) {

                Uri w_uri = data.getData();

                String w_strPath = MediaRealPathUtil.getPath(this, w_uri);

                String filename = Commons.fileNameWithExtFromUrl(w_strPath);

                if (w_strPath == null) {
                    showToast(getString(R.string.getvideo_fail));
                    return;
                }

                saveThumbnail(w_strPath);

                sendVideo(w_strPath, filename);
            }

        } else if (requestCode == Constants.PICK_FROM_FILE) {

            if (resultCode == RESULT_OK) {

                String path = data.getStringExtra(Constants.KEY_FILE);

                String filename = Commons.fileNameWithExtFromUrl(path);

                if (path != null) {
                    sendFile(path, filename);
                } else {
                    showToast(getString(R.string.getfile_fail));
                }
            }
        } else if (requestCode == Constants.PICK_FROM_INVITE) {

            if (resultCode == RESULT_OK) {

                ArrayList<Integer> participants = data.getIntegerArrayListExtra(Constants.KEY_PARTICIPANTS);

                for (Integer participant :participants) {

                    FriendEntity newFriend = _user.getFriend(participant.intValue());
                    _roomEntity.get_participantList().add(newFriend);
                }

                _roomEntity.set_participants(_roomEntity.makeRoomName());
                updateRoom();

            }
        } else if (requestCode == CODE_ADDBLOCK) {

            checkFriendBar();
        }
    }

    public void onExit() {

        if (_chattingAdapter.getCount() > 0) {

            GroupChatItem lastItem = (GroupChatItem) _chattingAdapter.getItem(_chattingAdapter.getCount() - 1);

            String recentMsg = lastItem.getMessage();
            String recentTime = lastItem.getDisplayTime();

            if (lastItem.getType() != GroupChatItem.ChatType.TEXT) {
                recentMsg = getString(R.string.transfer_file);
            }

            _roomEntity.init_recentCounter();
            _roomEntity.set_recentContent(recentMsg);
            _roomEntity.set_recentTime(recentTime);
            Database.updateRoom(_roomEntity);
        }

        finish();
    }

    @Override
    public void onClick(View view){

        if (view.getId() == R.id.imv_more) {

            if (_groupChat.isJoined) {

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(ui_edtMessage.getWindowToken(), 0);

                if (ui_lytMorebar.getVisibility() == View.VISIBLE)
                    ui_lytMorebar.setVisibility(View.GONE);
                else
                    ui_lytMorebar.setVisibility(View.VISIBLE);
            }

        } else if(view.getId() == R.id.imv_back) {
            onExit();
        } else if (view.getId() == R.id.imv_send) {
            if (ui_edtMessage.length() > 0) {

                if (_groupChat.isJoined) {
                    sendTextMessage(ui_edtMessage.getText().toString());
                    ui_edtMessage.setText("");
                } else {
                    showToast(getString(R.string.error));
                }
            }
        } else if (view.getId() == R.id.imv_invite) {
            gotoInviteFriend();
        } else if (view.getId() == R.id.imv_album) {
            doTakeGallery();
        } else if (view.getId() == R.id.imv_camera) {
            doTakePhotoAction();
        } else if (view.getId() == R.id.imv_video) {
            doTakeVideo();
        } else if (view.getId() == R.id.imv_file) {
            doTakeFile();
        } else if (view.getId() == R.id.lyt_addFriend) {
            showAddPopup();
        } else if (view.getId() == R.id.lyt_blockFriend) {
            showBlockPopup();
        } else if (view.getId() == R.id.lyt_addFriendOnlyButton) {
            gotoAddFriend();
        } else if (view.getId() == R.id.lyt_releaseFriendOnly) {
            showReleasePopup();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        GroupChatItem chatItem = (GroupChatItem) _chattingAdapter.getItem(position);

        switch (chatItem.getType()) {

            case TEXT:
                break;

            case IMAGE:
                if (chatItem.getSender() == _user.get_idx())
                    showImage(chatItem.getFileUrl(), true);
                else
                    showImage(chatItem.getFileUrl(), false);
                break;

            case VIDEO:
                if (chatItem.getSender() != _user.get_idx())
                    showFile(chatItem.getFilename());
                break;

            case FILE:

                if (chatItem.getSender() != _user.get_idx())
                    showDownloadDialog(chatItem);
                break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onExit();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        _groupChat.leaveRoom();
        _groupChat = null;
        Commons.g_chattingActivity = null;
    }


    @Override
    protected void onResume() {
        super.onResume();
        checkConnection();
    }
}
