package com.funlab.jcchat.main;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.funlab.jcchat.Base.CommonActivity;
import com.funlab.jcchat.JCChatApplication;
import com.funlab.jcchat.R;
import com.funlab.jcchat.commons.Commons;
import com.funlab.jcchat.commons.Constants;
import com.funlab.jcchat.commons.ReqConst;
import com.funlab.jcchat.model.UserEntity;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;

public class StateMessageActivity extends CommonActivity implements View.OnClickListener {

    private TextView ui_txvClose;
    private EditText ui_edtStateMsg;
    private ImageView ui_imvTextDel;
    private Button ui_btnConfirm;

    private UserEntity _user;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_state_message);

        _user = Commons.g_user;

        loadLayout();
    }

    public void loadLayout(){

        ui_txvClose = (TextView)findViewById(R.id.txv_close);
        ui_txvClose.setOnClickListener(this);

        ui_edtStateMsg = (EditText)findViewById(R.id.edt_state);
        ui_edtStateMsg.setText(_user.get_label());

        ui_imvTextDel = (ImageView)findViewById(R.id.imv_textDel);
        ui_imvTextDel.setOnClickListener(this);

        ui_btnConfirm = (Button)findViewById(R.id.btn_confirm);
        ui_btnConfirm.setOnClickListener(this);
    }

    public void onClick(View view){

        switch (view.getId()){

            case R.id.txv_close:
                finish();
                break;

            case R.id.btn_confirm:

                if(isValid()){
                    processConfirm();
                }
                break;

            case R.id.imv_textDel:
                ui_edtStateMsg.setText("");
                break;
        }
    }

    public boolean isValid(){
        if(ui_edtStateMsg.length() == 0){

            showToast(getString(R.string.inputState));
            return false;
        }

        return true;
    }

    public void processConfirm(){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_UPDATELABEL;

        try {
            String stateMessage = ui_edtStateMsg.getText().toString().trim().replace(" ", "%20");
            stateMessage = URLEncoder.encode(stateMessage, "utf-8");

            String params = String.format("/%d/%s", _user.get_idx(), stateMessage.trim());

            url += params;
        }catch (Exception e){

        }

        showProgress();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String json) {
                parseStateMessage(json);
            }
        }, new Response.ErrorListener(){

            @Override
            public void onErrorResponse(VolleyError error) {

                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        JCChatApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    public void parseStateMessage(String json){

        closeProgress();
        try{

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if(result_code == ReqConst.CODE_SUCCESS){

                _user.set_label(ui_edtStateMsg.getText().toString());

                Commons.g_user = _user;

                gotoMyInfoActivity();

            }else if(result_code == ReqConst.CODE_UNREGUSER){
                closeProgress();
                showAlertDialog(getString(R.string.unregistered_user));
            }
        }catch (JSONException e){
            e.printStackTrace();
            showAlertDialog(getString(R.string.error));
        }
    }

    public void gotoMyInfoActivity(){

        setResult(Constants.INPUT_STATE_MESSAGE);
        finish();
    }

}
