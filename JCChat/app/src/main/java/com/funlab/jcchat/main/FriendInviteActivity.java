package com.funlab.jcchat.main;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.funlab.jcchat.Base.CommonActivity;
import com.funlab.jcchat.R;
import com.funlab.jcchat.adapter.SelectFriendAdapter;
import com.funlab.jcchat.commons.Commons;
import com.funlab.jcchat.commons.Constants;
import com.funlab.jcchat.model.FriendEntity;
import com.funlab.jcchat.model.RoomEntity;
import com.funlab.jcchat.model.UserEntity;

import java.util.ArrayList;
import java.util.Locale;

public class FriendInviteActivity extends CommonActivity implements View.OnClickListener {

    private ListView _selectFriList = null;
    private SelectFriendAdapter _selectFriAdapter = null;

    private EditText ui_selectSearch;

    private UserEntity _user;
    private RoomEntity _room;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_invite);

        _user = Commons.g_user;

        String roomname = getIntent().getStringExtra(Constants.KEY_ROOM);
        _room = _user.getRoom(roomname);

        loadLayout();
    }

    public void loadLayout(){

        TextView txvClose = (TextView)findViewById(R.id.txv_close);
        txvClose.setOnClickListener(this);

        Button btnMakeRoom = (Button)findViewById(R.id.btn_invite);
        btnMakeRoom.setOnClickListener(this);

        ui_selectSearch = (EditText)findViewById(R.id.edt_search);
        ui_selectSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = ui_selectSearch.getText().toString().toLowerCase(Locale.getDefault());
                _selectFriAdapter.filter(text);
            }
        });


        ui_selectSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                switch (actionId) {
                    case EditorInfo.IME_ACTION_SEARCH:
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(ui_selectSearch.getWindowToken(), 0);
                        return true;

                    default:
                        return false;
                }
            }
        });

        _selectFriList = (ListView)findViewById(R.id.liv_selectFri);
        _selectFriAdapter = new SelectFriendAdapter(this);

        getSelectFriendData();
        _selectFriList.setAdapter(_selectFriAdapter);
    }

    public void getSelectFriendData(){

        _selectFriAdapter.clearData();

        for (int i = 0; i < _user.get_friendList().size(); i++){

            FriendEntity friend = _user.get_friendList().get(i);
            friend.set_isSelected(false);

            if (friend.get_blockStatus() == 1 && !_room.get_participantList().contains(friend))
                _selectFriAdapter.addItem(friend);
        }

        _selectFriAdapter.initSelectFriendDatas();
        _selectFriAdapter.notifyDataSetChanged();
    }


    public void showInvitePopup() {

        final ArrayList<Integer> participants = new ArrayList<Integer>();

        for (FriendEntity friend : _user.get_friendList()) {

            if (friend.is_isSelected()) {
                participants.add(Integer.valueOf(friend.get_idx()));
            }
        }

        if (participants.size() == 0)
            return;

         AlertDialog alertDialog = new AlertDialog.Builder(_context).create();

        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(getString(R.string.invite_confirm));

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,
                _context.getString(R.string.ok),

                new android.content.DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Intent intent = new Intent();
                        intent.putExtra(Constants.KEY_PARTICIPANTS, participants);
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                });

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE,
                getString(R.string.cancel),

                new android.content.DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        alertDialog.show();
    }

    public void onClick(View view){

        switch (view.getId()){
            case R.id.txv_close:
                finish();
                break;
            case R.id.btn_invite:
                showInvitePopup();
                break;
        }
    }
}
