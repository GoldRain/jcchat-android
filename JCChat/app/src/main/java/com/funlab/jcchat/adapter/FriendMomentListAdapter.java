package com.funlab.jcchat.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.funlab.jcchat.R;
import com.funlab.jcchat.commons.Constants;
import com.funlab.jcchat.main.ImagePreviewActivity;
import com.funlab.jcchat.main.SelectImageActivity;
import com.funlab.jcchat.main.VideoPreviewActivity;
import com.funlab.jcchat.model.ImageDataEntity;
import com.funlab.jcchat.model.MomentEntity;

import java.util.ArrayList;

/**
 * Created by HugeRain on 2/15/2017.
 */

public class FriendMomentListAdapter extends BaseAdapter {

    String[] _months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

    Context _activity;
    ArrayList<MomentEntity> _moment_list = new ArrayList<>();

    public FriendMomentListAdapter(Context activity, ArrayList<MomentEntity> moments){

        _activity = activity;
        _moment_list.clear();
        _moment_list.addAll(moments);

    }

    @Override
    public int getCount() {
        return _moment_list.size();
        //return 9;
    }

    @Override
    public Object getItem(int position) {
        return _moment_list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        MomentHolder momentHolder;

        if (convertView == null){

            momentHolder = new MomentHolder();

            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.friend_moment_item, parent, false);

            momentHolder.imv_photo = (ImageView)convertView.findViewById(R.id.imv_photos);
            momentHolder.txv_comment = (TextView)convertView.findViewById(R.id.txv_comment);
            momentHolder.txv_item = (TextView)convertView.findViewById(R.id.txv_item_conut);
            momentHolder.txv_date = (TextView)convertView.findViewById(R.id.txv_date);
            momentHolder.txv_month = (TextView)convertView.findViewById(R.id.txv_month);

            momentHolder.imv_video = (ImageView)convertView.findViewById(R.id.imv_video);
            momentHolder.imv_video.setVisibility(View.GONE);

            convertView.setTag(momentHolder);

        } else {

            momentHolder = (MomentHolder)convertView.getTag();
        }

        final MomentEntity moment_list = (MomentEntity) _moment_list.get(position);

        Log.d("id===>", String.valueOf(moment_list.get_id()));
        Log.d("comment====>", moment_list.get_comment());
        Log.d("images_data_size", String.valueOf(moment_list.get_images_data().size()));

        if (moment_list.get_images_data().get(0).get_videoUrl().length() == 0){/////images

            Glide.with(_activity).load(moment_list.get_images_data().get(0).get_photos()).placeholder(R.color.background).into(momentHolder.imv_photo);

            momentHolder.imv_video.setVisibility(View.GONE);

            momentHolder.txv_item.setText(String.valueOf(moment_list.get_images_data().size() + "items"));
            momentHolder.txv_comment.setText(moment_list.get_comment());

            if(position != 0 && (moment_list.get_date().contains(_moment_list.get(position-1).get_date()))){

                momentHolder.txv_month.setText("");
                momentHolder.txv_date.setText("");

            }else{

                String monthdate = moment_list.get_date();
                String[] separated = monthdate.split("-");
                momentHolder.txv_date.setText(separated[2]);
                momentHolder.txv_month.setText(_months[Integer.parseInt(separated[1])-1]);

            }

            momentHolder.imv_photo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ArrayList<String>imagepaths=new ArrayList<String>();
                    for(int i=0; i<moment_list.get_images_data().size(); i++){
                        imagepaths.add(moment_list.get_images_data().get(i).get_photos());
                    }

                    Intent intent = new Intent(_activity, ImagePreviewActivity.class);
                    intent.putExtra(Constants.KEY_IMAGEPATH, imagepaths);
                    intent.putExtra(Constants.KEY_POSITION, 0);
                    intent.putExtra(Constants.KEY_COMMENT, moment_list.get_comment());
                    intent.putExtra(Constants.KEY_TIME, moment_list.get_time());

                    _activity.startActivity(intent);
                }
            });

        } else{/////////video

            Glide.with(_activity).load(moment_list.get_images_data().get(0).get_photos()).placeholder(R.color.contentText).into(momentHolder.imv_photo);
            momentHolder.txv_item.setText("");
            momentHolder.txv_comment.setText(moment_list.get_comment());
            momentHolder.imv_video.setVisibility(View.VISIBLE);

            if(position != 0 && (moment_list.get_date().contains(_moment_list.get(position-1).get_date()))){

                momentHolder.txv_month.setText("");
                momentHolder.txv_date.setText("");
            }else{

                String monthdate = moment_list.get_date();
                String[] separated = monthdate.split("-");
                momentHolder.txv_date.setText(separated[2]);
                momentHolder.txv_month.setText(_months[Integer.parseInt(separated[1])-1]);

            }

            momentHolder.imv_photo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String path = moment_list.get_images_data().get(0).get_videoUrl();

                    Intent intent = new Intent(_activity, VideoPreviewActivity.class);
                    intent.putExtra(Constants.KEY_VIDEOPATH, path);
                    intent.putExtra(Constants.KEY_COMMENT, moment_list.get_comment());
                    intent.putExtra(Constants.KEY_TIME, moment_list.get_time());

                    Log.d("===video==path==>", path);

                    _activity.startActivity(intent);
                }
            });

        }

        return convertView;
    }

    public class MomentHolder {

        ImageView imv_photo, imv_video;
        TextView txv_comment, txv_item, txv_date, txv_month;

    }
}
