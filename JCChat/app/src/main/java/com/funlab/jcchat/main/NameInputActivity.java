package com.funlab.jcchat.main;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.funlab.jcchat.Base.CommonActivity;
import com.funlab.jcchat.JCChatApplication;
import com.funlab.jcchat.R;
import com.funlab.jcchat.commons.Commons;
import com.funlab.jcchat.commons.Constants;
import com.funlab.jcchat.commons.ReqConst;
import com.funlab.jcchat.model.UserEntity;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;

public class NameInputActivity extends CommonActivity implements View.OnClickListener {

    private TextView ui_txvClose;
    private EditText ui_edtName;
    private Button ui_btnConfirm;
    private ImageView ui_imvTextDel;

    private UserEntity _user;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name_input);

        _user = Commons.g_user;

        loadLayout();
    }

    public void loadLayout(){

        ui_txvClose = (TextView)findViewById(R.id.txv_close);
        ui_txvClose.setOnClickListener(this);

        ui_edtName = (EditText)findViewById(R.id.edt_name);
        ui_edtName.setText(_user.get_name());

        ui_imvTextDel = (ImageView)findViewById(R.id.imv_textDel);
        ui_imvTextDel.setOnClickListener(this);

        ui_btnConfirm = (Button)findViewById(R.id.btn_confirm);
        ui_btnConfirm.setOnClickListener(this);
    }

    public void onClick(View view){

        switch (view.getId()){

            case R.id.txv_close:
                finish();
                break;

            case R.id.btn_confirm:
                if(isValid()){
                    processConfirm();
                }
                break;

            case R.id.imv_textDel:
                ui_edtName.setText("");
               break;
        }
    }

    public boolean isValid(){

        if(ui_edtName.length() == 0){
            showToast(getString(R.string.inputName));
            return false;
        }

        return true;
    }

    public void processConfirm(){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_UPDATENAME;

        try{
            String name = ui_edtName.getText().toString().trim().replace(" ", "%20");
            name = URLEncoder.encode(name, "utf-8");
            String params = String.format("/%d/%s", _user.get_idx(), name);

            url += params;
        }catch (Exception e){

        }

        showProgress();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String json) {
                parseName(json);
            }
        }, new Response.ErrorListener(){

            @Override
            public void onErrorResponse(VolleyError error) {

                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        JCChatApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    public void parseName(String json){

        closeProgress();
        try{

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if(result_code == ReqConst.CODE_SUCCESS){

                _user.set_name(ui_edtName.getText().toString());

                Commons.g_user = _user;

                gotoMyInfoActivity();

            }else if(result_code == ReqConst.CODE_UNREGUSER){
                closeProgress();
                showAlertDialog(getString(R.string.unregistered_user));
            }
        }catch (JSONException e){
            e.printStackTrace();
            showAlertDialog(getString(R.string.error));
        }
    }

    public void gotoMyInfoActivity(){

        setResult(Constants.INPUT_NAME);
        finish();
    }
}
