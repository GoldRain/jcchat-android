package com.funlab.jcchat.main;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.funlab.jcchat.R;
import com.funlab.jcchat.adapter.ImagePreviewAdapter;
import com.funlab.jcchat.commons.Constants;

import java.util.ArrayList;

public class ImagePreviewActivity extends AppCompatActivity implements View.OnClickListener {

    ViewPager ui_viewPager;
    ImagePreviewAdapter _adapter;
    TextView ui_txvImageNo, txv_comment_pre, txv_time;

    ArrayList<String> _imagePaths = new ArrayList<>();
    int _position = 0;
    String _comment = "";
    String _time = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_preview);

        _imagePaths = getIntent().getStringArrayListExtra(Constants.KEY_IMAGEPATH);
        _position = getIntent().getIntExtra(Constants.KEY_POSITION,0);
        _comment = getIntent().getStringExtra(Constants.KEY_COMMENT);
        _time = getIntent().getStringExtra(Constants.KEY_TIME);

        loadLayout();
    }

    private void loadLayout(){

        ui_txvImageNo = (TextView)findViewById(R.id.txv_timeline_no);

        txv_comment_pre = (TextView)findViewById(R.id.txv_comment_pre);
        txv_comment_pre.setText(_comment);
        txv_time = (TextView)findViewById(R.id.txv_time);
        txv_time.setText(_time);

        ImageView imvBack = (ImageView)findViewById(R.id.imv_back);
        imvBack.setOnClickListener(this);

        ui_viewPager = (ViewPager)findViewById(R.id.viewpager);
        _adapter = new ImagePreviewAdapter(this);
        ui_viewPager.setAdapter(_adapter);
        _adapter.setDatas(_imagePaths);
        ui_viewPager.setCurrentItem(_position);
        ui_txvImageNo.setText((_position + 1) + " / " + _imagePaths.size());

        ui_viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                ui_txvImageNo.setText((position + 1) + " / " + _imagePaths.size());
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_back:
                finish();
                break;
        }
    }
}
