package com.funlab.jcchat.main;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.funlab.jcchat.Base.CommonActivity;
import com.funlab.jcchat.R;
import com.funlab.jcchat.Utils.Database;
import com.funlab.jcchat.adapter.SelectFriendAdapter;
import com.funlab.jcchat.chatting.GroupChattingActivity;
import com.funlab.jcchat.commons.Constants;
import com.funlab.jcchat.model.RoomEntity;
import com.funlab.jcchat.commons.Commons;
import com.funlab.jcchat.model.FriendEntity;
import com.funlab.jcchat.model.UserEntity;

import java.util.ArrayList;
import java.util.Locale;

public class FriendSelectActivity extends CommonActivity implements View.OnClickListener {

    private TextView ui_txvClose;
    private Button ui_btnMakeRoom;
    private ListView _selectFriList = null;
    private SelectFriendAdapter _selectFriAdapter = null;

    private EditText ui_selectSearch;

    private UserEntity _user;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_select);

        _user = Commons.g_user;

        loadLayout();
    }

    public void loadLayout(){

        ui_txvClose = (TextView)findViewById(R.id.txv_close);
        ui_txvClose.setOnClickListener(this);

        ui_btnMakeRoom = (Button)findViewById(R.id.btn_makeRoom);
        ui_btnMakeRoom.setOnClickListener(this);

        ui_selectSearch = (EditText)findViewById(R.id.edt_search);
        ui_selectSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = ui_selectSearch.getText().toString().toLowerCase(Locale.getDefault());
                _selectFriAdapter.filter(text);
            }
        });

        ui_selectSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                switch (actionId) {
                    case EditorInfo.IME_ACTION_SEARCH:
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(ui_selectSearch.getWindowToken(), 0);
                        return true;

                    default:
                        return false;
                }
            }
        });

        _selectFriList = (ListView)findViewById(R.id.liv_selectFri);
        _selectFriAdapter = new SelectFriendAdapter(this);

        getSelectFriendData();
        _selectFriList.setAdapter(_selectFriAdapter);
    }

    public void getSelectFriendData(){

        _selectFriAdapter.clearData();

        for (int i = 0; i < _user.get_friendList().size(); i++){

            FriendEntity friend = _user.get_friendList().get(i);
            friend.set_isSelected(false);

            if (friend.get_blockStatus() == 1)
                _selectFriAdapter.addItem(friend);
        }

        _selectFriAdapter.initSelectFriendDatas();
        _selectFriAdapter.notifyDataSetChanged();
    }

    public void makeRoom() {

        ArrayList<FriendEntity> participants = new ArrayList<FriendEntity>();

        for (FriendEntity friend : _user.get_friendList()) {

            if (friend.is_isSelected()) {
                participants.add(friend);
            }
        }

        if (participants.size() > 0) {

            RoomEntity room = new RoomEntity(participants);
            room.set_ownerIdx(_user.get_idx());

            if (!_user.get_roomList().contains(room)) {
                _user.get_roomList().add(room);
                Database.createRoom(room);
                showSuccessDialog(room);
            } else {
                showAlertDialog(getString(R.string.makeroom_fail));
            }
        }
    }

    public void showSuccessDialog(final RoomEntity room) {

        AlertDialog alertDialog = new AlertDialog.Builder(_context).create();

        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(getString(R.string.makeroom_success));

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, _context.getString(R.string.ok),

                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        gotoChattingRoom(room);
                    }
                });

        alertDialog.show();
    }

    public void gotoChattingRoom(RoomEntity room) {

        Intent intent = new Intent(FriendSelectActivity.this, GroupChattingActivity.class);
        intent.putExtra(Constants.KEY_ROOM, room.get_name());
        startActivity(intent);
        finish();
    }

    public void onClick(View view){

        switch (view.getId()){
            case R.id.txv_close:
                finish();
                break;
            case R.id.btn_makeRoom:
                makeRoom();
                break;
        }
    }
}
