package com.funlab.jcchat.main;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.funlab.jcchat.Base.CommonActivity;
import com.funlab.jcchat.JCChatApplication;
import com.funlab.jcchat.R;
import com.funlab.jcchat.Utils.CirculaireNetworkImageView;
import com.funlab.jcchat.Utils.Database;
import com.funlab.jcchat.Utils.RectNetImageView;
import com.funlab.jcchat.commons.Commons;
import com.funlab.jcchat.commons.Constants;
import com.funlab.jcchat.commons.ReqConst;
import com.funlab.jcchat.model.FriendEntity;
import com.funlab.jcchat.model.UserEntity;

import org.json.JSONException;
import org.json.JSONObject;

public class RecommendProfileActivity extends CommonActivity implements View.OnClickListener {

    private FriendEntity _friend ;
    private UserEntity _user;

    private ImageLoader _imageLoader;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recommend_profile);

        _user = Commons.g_user;
        _friend = (FriendEntity) getIntent().getSerializableExtra(Constants.KEY_FRIEND);

        _imageLoader = JCChatApplication.getInstance().getImageLoader();

        loadLayout();
    }

    public void loadLayout(){

        ImageView imvClose = (ImageView)findViewById(R.id.imv_profile_close);
        imvClose.setOnClickListener(this);

        TextView ui_txvAddFriend = (TextView)findViewById(R.id.txv_addFriend);
        ui_txvAddFriend.setOnClickListener(this);

        TextView ui_txvBlockFriend = (TextView)findViewById(R.id.txv_blockFriend);
        ui_txvBlockFriend.setOnClickListener(this);

        TextView txvStateMessage = (TextView) findViewById(R.id.txv_profile_title);
        txvStateMessage.setText(_friend.get_label());

        RectNetImageView imvBack = (RectNetImageView) findViewById(R.id.imv_bg);
        imvBack.setImageUrl(_friend.get_bgUrl(), _imageLoader);

        CirculaireNetworkImageView imvProfile = (CirculaireNetworkImageView) findViewById(R.id.imv_friend_proPhoto);
        imvProfile.setImageUrl(_friend.get_photoUrl(), _imageLoader);
    }

    public void showAddPopup() {

        AlertDialog alertDialog = new AlertDialog.Builder(_context).create();

        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(getString(R.string.blcok_confirm));

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,
                _context.getString(R.string.ok),

                new android.content.DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        addFriend();
                    }
                });

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE,
                getString(R.string.cancel),

                new android.content.DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        alertDialog.show();
    }


    public void addFriend() {

        String url = ReqConst.SERVER_URL + ReqConst.REQ_ADDFRIENDBYID;

        String params = String.format("/%d/%d", _user.get_idx(), _friend.get_idx());
        url += params;

        showProgress();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                parseAddFriendResponse(json);
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        JCChatApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    public void parseAddFriendResponse(String json){

        closeProgress();

        try{

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){
                _user.get_friendList().add(_friend);
                showAlertDialog(getString(R.string.addfriend_success));
            }

        } catch (JSONException e){
            e.printStackTrace();
        }
    }

    public void showBlockPopup() {

        AlertDialog alertDialog = new AlertDialog.Builder(_context).create();

        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(getString(R.string.blcok_confirm));

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,
                _context.getString(R.string.ok),

                new android.content.DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        blockFriend();
                    }
                });

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE,
                getString(R.string.cancel),

                new android.content.DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        alertDialog.show();
    }

    public void blockFriend() {

        showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_BLOCKFRIEND;
        String params = String.format("/%d/%d/%d", _user.get_idx(), _friend.get_idx(), 0);
        url += params;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {

                parseBlockResponse(json);
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                showAlertDialog(getString(R.string.error));
                closeProgress();
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        JCChatApplication.getInstance().addToRequestQueue(stringRequest, url);

    }

    public void parseBlockResponse(String json) {

        try {

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS) {
                _friend.set_blockStatus(0);
                _user.get_friendList().add(_friend);
                showAlertDialog(getString(R.string.blockfriend_success));
                Database.createBlock(_friend.get_idx());
            }

        } catch (JSONException e) {
            e.printStackTrace();
            showAlertDialog(getString(R.string.error));
        }

        closeProgress();
    }

    public void onClick(View view){

        switch (view.getId()){

            case R.id.imv_profile_close:
                finish();
                break;
            case R.id.txv_addFriend:
                showAddPopup();
                break;

            case R.id.txv_blockFriend:
                showBlockPopup();
                break;
        }
    }

}
