package com.funlab.jcchat.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.funlab.jcchat.Base.CommonTabActivity;
import com.funlab.jcchat.JCChatApplication;
import com.funlab.jcchat.R;
import com.funlab.jcchat.adapter.AddFriendAdapter;
import com.funlab.jcchat.commons.Constants;
import com.funlab.jcchat.commons.ReqConst;
import com.funlab.jcchat.model.FriendEntity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;

public class AddFriendActivity extends CommonTabActivity implements View.OnClickListener {

    private ImageView ui_imvFriend, ui_imvChatting, ui_imvAddFriend, ui_imvMyInfo;

    private ListView _addFriendList = null;
    private AddFriendAdapter _addFriendAdapter = null;

    private EditText ui_edtSearch;
    private ImageView ui_imvSearch;

    LinearLayout lyt_footer_friend, lyt_footer_chat, lyt_footer_addFriend, lyt_footer_info;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_friend);

        loadLayout();
    }

    public void loadLayout(){

        lyt_footer_friend = (LinearLayout)findViewById(R.id.lyt_footer_friend);
        lyt_footer_chat = (LinearLayout)findViewById(R.id.lyt_footer_chat);
        lyt_footer_addFriend = (LinearLayout)findViewById(R.id.lyt_footer_addFriend);
        lyt_footer_info = (LinearLayout)findViewById(R.id.lyt_footer_info);

        ui_imvFriend = (ImageView)findViewById(R.id.imv_friend);
        ui_imvFriend.setOnClickListener(this);

        ui_imvChatting = (ImageView)findViewById(R.id.imv_chatting);
        ui_imvChatting.setOnClickListener(this);

        ui_imvAddFriend = (ImageView)findViewById(R.id.imv_add_friend);
        ui_imvAddFriend.setOnClickListener(this);
        ui_imvAddFriend.setSelected(true);

        TextView txvAddFriend = (TextView) findViewById(R.id.txv_add_friend);
        txvAddFriend.setSelected(true);

        ui_imvMyInfo = (ImageView)findViewById(R.id.imv_my_info);
        ui_imvMyInfo.setOnClickListener(this);

        ui_edtSearch = (EditText)findViewById(R.id.edt_search);

        ui_edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                switch (actionId) {
                    case EditorInfo.IME_ACTION_SEARCH:
                        getSearchFriendData();
                        return true;

                    default:
                        return false;
                }
            }
        });

        ui_imvSearch = (ImageView) findViewById(R.id.imv_search);
        ui_imvSearch.setOnClickListener(this);

        ui_txvUnread = (TextView) findViewById(R.id.txv_unread);
        setUnRead();

        _addFriendList = (ListView)findViewById(R.id.liv_addFriend);
        _addFriendAdapter = new AddFriendAdapter(this);
        _addFriendList.setAdapter(_addFriendAdapter);

        _addFriendList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(AddFriendActivity.this, RecommendProfileActivity.class);
                intent.putExtra(Constants.KEY_FRIEND, (FriendEntity) _addFriendAdapter.getItem(position));
                startActivity(intent);
            }
        });

        setFooter(getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.blue), getResources().getColor(R.color.colorPrimary));
    }

    public void setFooter(int a, int b, int c, int d){

        lyt_footer_friend.setBackgroundColor(a);
        lyt_footer_chat.setBackgroundColor(b);
        lyt_footer_addFriend.setBackgroundColor(c);
        lyt_footer_info.setBackgroundColor(d);
    }

    public void addFriend(final FriendEntity friend) {

        String url = ReqConst.SERVER_URL + ReqConst.REQ_ADDFRIENDBYID;

        String params = String.format("/%d/%d", _user.get_idx(), friend.get_idx());
        url += params;

        showProgress();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                parseAddFriendResponse(json, friend);
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        JCChatApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    public void parseAddFriendResponse(String json, FriendEntity friend){

        closeProgress();

        try{

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){
                _addFriendAdapter.removeItem(friend);
                _user.get_friendList().add(friend);

                showAlertDialog(getString(R.string.addfriend_success));
            }

        } catch (JSONException e){
            e.printStackTrace();
        }

        _addFriendAdapter.notifyDataSetChanged();
    }

    public void getRecommendFriendData(){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_GETRECOMMENDFRIENDS;

        String params = String.format("/%d", _user.get_idx());
        url += params;

        showProgress();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                parseRecommendFriendResponse(json);
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        JCChatApplication.getInstance().addToRequestQueue(stringRequest, url);

    }

    public void parseRecommendFriendResponse(String json){

        _addFriendAdapter.clearFriendData();

        try{

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                JSONArray friends = response.getJSONArray(ReqConst.RES_USERLIST);

                for (int i = 0; i < friends.length(); i++) {

                    JSONObject friend = (JSONObject) friends.get(i);
                    FriendEntity entity = new FriendEntity();

                    entity.set_idx(friend.getInt(ReqConst.RES_ID));
                    entity.set_name(friend.getString(ReqConst.RES_NAME));
                    entity.set_label(friend.getString(ReqConst.RES_LABEL));
                    entity.set_photoUrl(friend.getString(ReqConst.RES_PHOTO_URL));
                    entity.set_bgUrl(friend.getString(ReqConst.RES_BG_URL));

                   _addFriendAdapter.addItem(entity);
                }
            }

        } catch (JSONException e){
            e.printStackTrace();
        }

        _addFriendAdapter.notifyDataSetChanged();

        closeProgress();
    }

    public void getSearchFriendData() {

        String words = ui_edtSearch.getText().toString().trim();

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(ui_edtSearch.getWindowToken(), 0);

        if (words.length() == 0) {
            getRecommendFriendData();
        } else {
            search(words);
        }
    }


    public void search(String words){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_SEARCHFRIEND;

        try {
            words = URLEncoder.encode(words, "utf-8");
        } catch (Exception ex) {

        }

        String params = String.format("/%s", words);

        url += params;

        showProgress();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {

                parseSearchResponse(json);
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        JCChatApplication.getInstance().addToRequestQueue(stringRequest, url);

    }

    public void parseSearchResponse(String json){

        closeProgress();

        _addFriendAdapter.clearFriendData();

        try{

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                JSONArray results = response.getJSONArray(ReqConst.RES_SEARCH_RESULT);

                for (int i = 0; i < results.length(); i++) {

                    JSONObject friend = (JSONObject) results.get(i);

                    FriendEntity entity = new FriendEntity();

                    entity.set_idx(friend.getInt(ReqConst.RES_ID));
                    entity.set_name(friend.getString(ReqConst.RES_NAME));
                    entity.set_label(friend.getString(ReqConst.RES_LABEL));
                    entity.set_photoUrl(friend.getString(ReqConst.RES_PHOTO_URL));
                    entity.set_bgUrl(friend.getString(ReqConst.RES_BG_URL));

                    if (entity.get_idx() !=  _user.get_idx() && !_user.get_friendList().contains(entity))
                        _addFriendAdapter.addItem(entity);

                }
            }

        } catch (JSONException e){
            e.printStackTrace();
        }

        _addFriendAdapter.notifyDataSetChanged();
    }

    public void onClick(View view){

        switch (view.getId()){
            case R.id.imv_friend:
                gotoFriendActivity();
                break;

            case R.id.imv_chatting:
                gotoChattListActivity();
                break;

            case R.id.imv_add_friend:
                break;
            case R.id.imv_my_info:
                gotoMyInfoActivity();
                break;
            case R.id.imv_search:
                getSearchFriendData();
                break;
        }
    }

    public void gotoFriendActivity(){

        Intent intent = new Intent(this, FriendActivity.class);
        startActivity(intent);
        overridePendingTransition(0,0);
        finish();
    }

    public void gotoChattListActivity(){

        Intent intent = new Intent(this, ChatListActivity.class);
        startActivity(intent);
        overridePendingTransition(0,0);
        finish();
    }

    public void gotoMyInfoActivity(){

        Intent intent = new Intent(this, MyInformationActivity.class);
        startActivity(intent);
        overridePendingTransition(0,0);
        finish();
    }

    @Override
    protected void onResume() {

        super.onResume();
        getRecommendFriendData();
    }

}
