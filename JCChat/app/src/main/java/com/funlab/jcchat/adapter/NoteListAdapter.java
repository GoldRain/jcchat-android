package com.funlab.jcchat.adapter;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.funlab.jcchat.R;
import com.funlab.jcchat.model.NoteEntity;

import java.util.ArrayList;

/**
 * Created by HGS on 12/14/2015.
 */
public class NoteListAdapter extends BaseAdapter {

    private Context _context;

    private ArrayList<NoteEntity> _noteDatas = new ArrayList<NoteEntity>();

    public NoteListAdapter(Context _context){

        super();
        this._context = _context;

    }

    public void add(NoteEntity note) {
        _noteDatas.add(note);
    }

    public void clear() {
        _noteDatas.clear();
    }

    @Override
    public int getCount(){
        return _noteDatas.size();
    }

    @Override
    public Object getItem(int position){
        return _noteDatas.get(position);
    }

    @Override
    public long getItemId(int position){
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){

        NoteListHoder noteHolder;

        if(convertView == null){
            noteHolder = new NoteListHoder();

            LayoutInflater inflater = (LayoutInflater)_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.note_list_item, null);

            noteHolder.txvNoteTitle = (TextView)convertView.findViewById(R.id.txv_note_title);
            noteHolder.imvTo = (ImageView)convertView.findViewById(R.id.imv_to);

            convertView.setTag(noteHolder);
        }else {
            noteHolder = (NoteListHoder)convertView.getTag();
        }

        final NoteEntity note = _noteDatas.get(position);
        noteHolder.txvNoteTitle.setText("");
        String fullString = note.get_title() + " " + note.get_date();
        SpannableStringBuilder builder = new SpannableStringBuilder(fullString);
        int color = _context.getResources().getColor(R.color.colorPrimary);
        builder.setSpan(new ForegroundColorSpan(color), note.get_title().length() + 1, note.get_title().length() + 1 + note.get_date().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.setSpan(new RelativeSizeSpan(0.8f), note.get_title().length() + 1, note.get_title().length() + 1 + note.get_date().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        noteHolder.txvNoteTitle.append(builder);

        return convertView;
    }

    public class NoteListHoder {

        public TextView txvNoteTitle;
        public ImageView imvTo;
    }
}
