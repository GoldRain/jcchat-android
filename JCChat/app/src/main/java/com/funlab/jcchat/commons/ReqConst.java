package com.funlab.jcchat.commons;

/**
 * Created by HGS on 12/11/2015.
 */
public class ReqConst {

//    public static final String SERVER_ADDR = "http://192.168.0.40/D-Chat";
//    public static final String CHATTING_SERVER = "192.168.0.40";

    public static final String SERVER_ADDR = "http://52.68.134.121";
    public static final String CHATTING_SERVER = "52.68.134.121";

//    public static final String SERVER_ADDR = "http://52.79.113.96";
//    public static final String CHATTING_SERVER = "52.79.113.96";

//    public static final String SERVER_ADDR = "http://121.88.250.136";
//    public static final String CHATTING_SERVER = "121.88.250.136";

    public static final String ROOM_SERVICE = "@conference.";

    public static final String SERVER_URL = SERVER_ADDR + "/index.php/api/";

    public static final String REQ_GETVERSIONINFO = "getVersionInfo";
    public static final String REQ_LOGIN = "login";
    public static final String REQ_LOGOUT = "logout";
    public static final String REQ_SINGUP = "signup";
    public static final String REQ_CREATEAUTHCODE = "createAuthCode";
    public static final String REQ_CONFIRMAUTHCODE = "confirmAuthCode";
    public static final String REQ_GETAUTHCODE = "getAuthCode";
    public static final String REQ_RESETNEWPASSWORD = "resetNewPassword";
    public static final String REQ_GETUSERINFO = "getUserInfo";
    public static final String REQ_GETALLUSERS = "getAllUsers";
    public static final String REQ_UPDATENAME = "updateName";
    public static final String REQ_UPDATELABEL = "updateLabel";
    public static final String REQ_UPLOADIMAGE = "uploadImage";
    public static final String REQ_UPLOADFILE = "uploadFile";
    public static final String REQ_ADDFRIENDALLOW = "addFriendAllow";
    public static final String REQ_SEARCHFRIEND = "searchFriend";
    public static final String REQ_ADDFRIENDBYID = "addFriendById";
    public static final String REQ_GETFRIENDLIST = "getFriendList";
    public static final String REQ_BLOCKFRIEND = "blockFriends";
    public static final String REQ_GETRECOMMENDFRIENDS = "getRecommendFriends";
    public static final String REQ_GETROOMINFO = "getRoomInfo";
    public static final String REQ_REMOVEPROFILE ="removeProfile";
    public static final String REQ_REMOVEBACKGROUND = "removeBackground";
    public static final String REQ_ADDFRIENDBYPHONENUMBER = "addFriendByPhoneNumber";
    public static final String REQ_SETALLOWFRIEND = "setAllowFriend";
    public static final String REQ_GETNOTELIST = "getNoteList";
    public static final String REQ_GETNOTECONTENT = "getNoteContent";
    public static final String REQ_REGISTERTOKEN = "registerToken";
    public static final String REQ_GETROOMINFOLIST = "getRoomInfoList";
    public static final String REQ_ADDFRIENDLIST = "addFriendList";
    public static final String REQ_BLOCKFRIENDLIST = "blockFriendList";

    //request params
    public static final String PARAM_EMAIL = "email";
    public static final String PARAM_PASSWORD = "password";
    public static final String PARAM_ID = "id";
    public static final String PARAM_TYPE = "type";
    public static final String PARAM_FILENAME = "filename";
    public static final String PARAM_NAME = "name";
    public static final String PARAM_PHONE_NUMBER = "phone_number";
    public static final String PARAM_PHOTO = "photo";
    public static final String PARAM_AUTH_CODE = "auth_code";
    public static final String PARAM_NEW_PASSWORD = "new_password";
    public static final String PARAM_LABEL = "label";
    public static final String PARAM_IMAGE_TYPE = "image_type";
    public static final String PARAM_FILE = "file";
    public static final String PARAM_SEARCH_STRING = "search_string";
    public static final String PARAM_PHONE_NUMBERS = "phone_numbers";
    public static final String PARAM_ROOMS = "rooms";
    public static final String PARAM_FRIENDLIST = "friend_list";
    public static final String PARAM_BLCOKSTATUS = "block_status";

    //response value
    public static final String RES_CODE = "result_code";
    public static final String RES_IDX = "idx";
    public static final String RES_ID = "id";
    public static final String RES_NAME = "name";
    public static final String RES_EMAIL = "email";
    public static final String RES_LABEL = "label";
    public static final String RES_BG_URL = "bg_url";
    public static final String RES_PHOTO_URL = "photo_url";
    public static final String RES_AUTH_CODE = "auth_code";
    public static final String RES_PHONE_NUMBER = "phone_number";
    public static final String RES_FILE_URL = "file_url";
    public static final String RES_SEARCH_RESULT = "search_result";
    public static final String RES_FRIENDLIST = "friend_list";
    public static final String RES_USERLIST = "user_list";
    public static final String RES_BLOCK_STATUS = "block_status";
    public static final String RES_FILENAME = "filename";
    public static final String RES_ALLOWFRIEND = "allow_friend";
    public static final String RES_NOTELIST = "note_list";
    public static final String RES_NOTEDATE = "note_date";
    public static final String RES_NOTETITLE = "note_title";
    public static final String RES_NOTECONTENT = "note_content";
    public static final String RES_VERSION = "version";

    public static final int CODE_SUCCESS = 0;
    public static final int CODE_UNREGUSER = 101;
    public static final int CODE_INVALIDPWD = 102;
    public static final int CODE_INVALIDEMAIL = 104;
    public static final int CODE_UNAUTHEMAIL = 105;
    public static final int CODE_AUTHFAIL = 106;
    public static final int CODE_INVALIDAUTH = 107;
    public static final int CODE_RESET_UNAUTHEMAIL = 108;
    public static final int CODE_OVERFLOW = 111;


}
