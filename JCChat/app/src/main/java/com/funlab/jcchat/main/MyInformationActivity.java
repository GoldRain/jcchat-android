package com.funlab.jcchat.main;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.funlab.jcchat.Base.CommonTabActivity;
import com.funlab.jcchat.JCChatApplication;
import com.funlab.jcchat.R;
import com.funlab.jcchat.Utils.BitmapUtils;
import com.funlab.jcchat.Utils.CirculaireNetworkImageView;
import com.funlab.jcchat.Utils.MultiPartRequest;
import com.funlab.jcchat.Utils.PhotoSelectDialog;
import com.funlab.jcchat.commons.Commons;
import com.funlab.jcchat.commons.Constants;
import com.funlab.jcchat.commons.ReqConst;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class MyInformationActivity extends CommonTabActivity implements View.OnClickListener {

    private Uri _imageCaptureUri;
    private String _photoPath = "";

    private ImageView ui_imvFriend, ui_imvChatting, ui_imvAddFriend, ui_imvMyInfo;
    private LinearLayout ui_layoutNameInput, ui_layoutStateInput;

    private TextView ui_txvName, ui_txvState;


    private PhotoSelectDialog _photoSelectDialog;
    private ImageLoader _imageLoader;
    CirculaireNetworkImageView ui_imvMyPhoto;
    LinearLayout lyt_footer_friend, lyt_footer_chat, lyt_footer_addFriend, lyt_footer_info;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_information);

        loadLayout();
    }

    public void loadLayout(){

        lyt_footer_friend = (LinearLayout)findViewById(R.id.lyt_footer_friend);
        lyt_footer_chat = (LinearLayout)findViewById(R.id.lyt_footer_chat);
        lyt_footer_addFriend = (LinearLayout)findViewById(R.id.lyt_footer_addFriend);
        lyt_footer_info = (LinearLayout)findViewById(R.id.lyt_footer_info);

        TextView txvEmail = (TextView)findViewById(R.id.info_email);
        txvEmail.setText(_user.get_email());

        ui_imvMyPhoto = (CirculaireNetworkImageView)findViewById(R.id.imv_info_photo);
        _imageLoader = JCChatApplication.getInstance().getImageLoader();
        ui_imvMyPhoto.setImageUrl(_user.get_photoUrl(), _imageLoader);
        ui_imvMyPhoto.setOnClickListener(this);

        ui_txvName = (TextView)findViewById(R.id.txv_inputName);
        ui_txvName.setText(_user.get_name());

        ui_txvState = (TextView)findViewById(R.id.txv_inputState);
        ui_txvState.setText(_user.get_label());

        TextView txvInfoSetting = (TextView)findViewById(R.id.txv_setting);
        txvInfoSetting.setOnClickListener(this);

        TextView txvInfoBlockFriend = (TextView)findViewById(R.id.txv_block_friend);
        txvInfoBlockFriend.setOnClickListener(this);

        TextView txvInfoPwdChange = (TextView)findViewById(R.id.txv_pwd_change);
        txvInfoPwdChange.setOnClickListener(this);

        TextView txvInfoLogout = (TextView)findViewById(R.id.txv_logout);
        txvInfoLogout.setOnClickListener(this);

        TextView txvNote = (TextView) findViewById(R.id.txv_note);
        txvNote.setOnClickListener(this);

        TextView txv_take_photo = (TextView)findViewById(R.id.txv_take_photo);
        txv_take_photo.setOnClickListener(this);

        TextView txvEula = (TextView) findViewById(R.id.txv_readuse);
        txvEula.setOnClickListener(this);

        TextView txvPersonal = (TextView) findViewById(R.id.txv_readpersonal);
        txvPersonal.setOnClickListener(this);

        ui_layoutNameInput = (LinearLayout)findViewById(R.id.layout_nameInput);
        ui_layoutNameInput.setOnClickListener(this);

        ui_layoutStateInput = (LinearLayout)findViewById(R.id.layout_stateInput);
        ui_layoutStateInput.setOnClickListener(this);

        ui_imvFriend = (ImageView)findViewById(R.id.imv_friend);
        ui_imvFriend.setOnClickListener(this);

        ui_imvChatting = (ImageView)findViewById(R.id.imv_chatting);
        ui_imvChatting.setOnClickListener(this);

        ui_imvAddFriend = (ImageView)findViewById(R.id.imv_add_friend);
        ui_imvAddFriend.setOnClickListener(this);

        ui_imvMyInfo = (ImageView)findViewById(R.id.imv_my_info);
        ui_imvMyInfo.setOnClickListener(this);
        ui_imvMyInfo.setSelected(true);

        TextView txvMyInfo = (TextView) findViewById(R.id.txv_my_info);
        txvMyInfo.setSelected(true);

        ui_txvUnread = (TextView) findViewById(R.id.txv_unread);
        setUnRead();

        setFooter(getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.blue));
    }

    public void setFooter(int a, int b, int c, int d){

        lyt_footer_friend.setBackgroundColor(a);
        lyt_footer_chat.setBackgroundColor(b);
        lyt_footer_addFriend.setBackgroundColor(c);
        lyt_footer_info.setBackgroundColor(d);
    }


    public void gotoNameInputActivity(){

        Intent intent = new Intent(this, NameInputActivity.class);
        startActivityForResult(intent, Constants.INPUT_NAME);
    }

    public void gotoStateMessageActivity(){

        Intent intent = new Intent(this, StateMessageActivity.class);
        startActivityForResult(intent, Constants.INPUT_STATE_MESSAGE);
    }

    public void gotoSettingActivity(){

        Intent intent = new Intent(this, SettingActivity.class);
        startActivity(intent);
    }

    public void gotoReleaseFriendActivity(){
        Intent intent = new Intent(this, ReleaseBlockFriendActivity.class);
        startActivity(intent);
    }

    public void gotoPwdResetActivity(){
        Intent intent = new Intent(this, PwdResetActivity.class);
        startActivity(intent);
    }

    public void logout(){

        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra(Constants.KEY_LOGOUT, true);
        startActivity(intent);

        Commons.g_xmppService.disconnect();

        unRegisterToken();
    }

    public void unRegisterToken() {

        String url = ReqConst.SERVER_URL + ReqConst.REQ_REGISTERTOKEN;

        String params = String.format("/%d/%s", _user.get_idx(), " ");
        url += params;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {

                parseTokenResponse(json);
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                finish();
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        JCChatApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    public void parseTokenResponse(String json){

        try{

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

            }

        }catch (JSONException e){
            e.printStackTrace();
        }

        finish();

    }

    public void gotoFriendActivity(){

        Intent intent = new Intent(this, FriendActivity.class);
        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();
    }

    public void gotoChatListActivity(){

        Intent intent = new Intent(this, ChatListActivity.class);
        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();
    }

    public void gotoAddFriendActivity(){

        Intent intent = new Intent(this, AddFriendActivity.class);
        startActivity(intent);
        overridePendingTransition(0,0);
        finish();
    }

    public void gotoNoteListActivity(){

        Intent intent = new Intent(this, NoteListActivity.class);
        startActivity(intent);
    }

    public void gotoEula(){

        Intent intent = new Intent(this, ReadUseActivity.class);
        startActivity(intent);
    }

    public void gotoPersonal() {

        Intent intent = new Intent(this, ReadPersonalActivity.class);
        startActivity(intent);
    }

    public void gotoTakePhoto(){

        startActivity(new Intent(this, TakePhotoActivity.class));
    }

    public void setMyPhoto(){

        View.OnClickListener cameraListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doTakePhotoAction();
                _photoSelectDialog.dismiss();
            }
        };
        View.OnClickListener albumListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doTakeGallery();
                _photoSelectDialog.dismiss();
            }
        };
        View.OnClickListener deleteListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteProfile();
                _photoSelectDialog.dismiss();
            }
        };

        View.OnClickListener cancelListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _photoSelectDialog.dismiss();
            }
        };

        if (_user.get_photoUrl().length() > 0)
            _photoSelectDialog = new PhotoSelectDialog(_context, cameraListener, albumListener, deleteListener, cancelListener);
        else
            _photoSelectDialog = new PhotoSelectDialog(_context, cameraListener, albumListener, cancelListener);

        _photoSelectDialog.show();
    }

    public void doTakePhotoAction(){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String picturePath = BitmapUtils.getTempFolderPath() + "photo_temp.jpg";
        _imageCaptureUri = Uri.fromFile(new File(picturePath));

        intent.putExtra(MediaStore.EXTRA_OUTPUT, _imageCaptureUri);
        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);

    }

    public void doTakeGallery(){
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, Constants.PICK_FROM_ALBUM);
    }

    public void deleteProfile() {

        String url = ReqConst.SERVER_URL + ReqConst.REQ_REMOVEPROFILE;
        String params = String.format("/%d", _user.get_idx());
        url += params;

        showProgress();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>(){

            @Override
            public void onResponse(String json) {
                parseDeleteResponse(json);
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        JCChatApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    public void parseDeleteResponse(String json){

        closeProgress();

        try{

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if(result_code == ReqConst.CODE_SUCCESS){
                ui_imvMyPhoto.setImageDrawable(null);
                _user.set_photoUrl("");
                _photoPath = "";
            } else {
                showAlertDialog(getString(R.string.error));
            }

        }catch (JSONException e){

            e.printStackTrace();
            showAlertDialog(getString(R.string.error));
        }
    }

    protected void onActivityResult(int requestCOde, int resultCode, Intent data){

        switch (requestCOde){

            case Constants.CROP_FROM_CAMERA: {

                if (resultCode == RESULT_OK){
                    try {

                        File saveFile = BitmapUtils.getOutputMediaFile(this);

                        InputStream in = getContentResolver().openInputStream(Uri.fromFile(saveFile));
                        BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);

                        in.close();

                        //set The bitmap data to image View
                        ui_imvMyPhoto.setImageBitmap(bitmap);
                        _photoPath = saveFile.getAbsolutePath();

                        uploadImage();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                break;
            }
            case Constants.PICK_FROM_ALBUM:

                if (resultCode == RESULT_OK){
                    _imageCaptureUri = data.getData();
                }

            case Constants.PICK_FROM_CAMERA:
            {
                try {
                    _photoPath = BitmapUtils.getRealPathFromURI(this, _imageCaptureUri);

                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(_imageCaptureUri, "image/*");

                    intent.putExtra("crop", true);
                    intent.putExtra("scale", true);
                    intent.putExtra("outputX", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("outputY", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("aspectX", 1);
                    intent.putExtra("aspectY", 1);
                    intent.putExtra("noFaceDetection", true);
                    //intent.putExtra("return-data", true);
                    intent.putExtra("output", Uri.fromFile(BitmapUtils.getOutputMediaFile(this)));

                    startActivityForResult(intent, Constants.CROP_FROM_CAMERA);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            }
            case Constants.INPUT_NAME:
                ui_txvName.setText(_user.get_name());
                break;

            case Constants.INPUT_STATE_MESSAGE:
                ui_txvState.setText(_user.get_label());
                break;
        }
    }

    public void uploadImage() {

        //if no profile photo
        if (_photoPath.length() == 0) {
            closeProgress();
            return;
        }

        try {

            File file = new File(_photoPath);

            Map<String, String> params = new HashMap<String, String>();
            params.put(ReqConst.PARAM_ID, String.valueOf(_user.get_idx()));
            params.put(ReqConst.PARAM_IMAGE_TYPE, String.valueOf(0));

            String url = ReqConst.SERVER_URL + ReqConst.REQ_UPLOADIMAGE;

            MultiPartRequest reqMultiPart = new MultiPartRequest(url, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    closeProgress();
                    showToast(getString(R.string.photo_upload_fail));

                }
            }, new Response.Listener<String>() {

                @Override
                public void onResponse(String json) {

                    ParseUploadImgResponse(json);
                }
            }, file, ReqConst.PARAM_FILE, params);

            reqMultiPart.setRetryPolicy(new DefaultRetryPolicy(
                    Constants.VOLLEY_TIME_OUT, 0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            JCChatApplication.getInstance().addToRequestQueue(reqMultiPart, url);

        } catch (Exception e) {

            e.printStackTrace();
            closeProgress();

            showToast(getString(R.string.photo_upload_fail));

        }
    }

    public void ParseUploadImgResponse(String json){

        closeProgress();

        try {

            JSONObject response = new JSONObject(json);
            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == 0){
                String file = response.getString(ReqConst.RES_FILE_URL);
                _user.set_photoUrl(file);
                ui_imvMyPhoto.setImageUrl(_user.get_photoUrl(), _imageLoader);
            }
            else if(result_code == 111){
                showToast(getString(R.string.photo_upload_fail));
            }
        } catch (JSONException e){
            e.printStackTrace();
        }

    }

    public void onClick(View view){

        switch (view.getId()){

            case R.id.txv_setting:
                gotoSettingActivity();
                break;
            case R.id.txv_block_friend:
                gotoReleaseFriendActivity();
                break;
            case R.id.txv_pwd_change:
                gotoPwdResetActivity();
                break;
            case R.id.txv_logout:
                logout();
                break;
            case R.id.imv_friend:
                gotoFriendActivity();
                break;
            case R.id.imv_chatting:
                gotoChatListActivity();
                break;
            case R.id.imv_add_friend:
                gotoAddFriendActivity();
                break;
            case R.id.imv_my_info:
                break;
            case R.id.layout_nameInput:
                gotoNameInputActivity();
                break;
            case R.id.layout_stateInput:
                gotoStateMessageActivity();
                break;
            case R.id.imv_info_photo:
                setMyPhoto();
                break;
            case R.id.txv_note:
                gotoNoteListActivity();
                break;
            case R.id.txv_readuse:
                gotoEula();
                break;
            case R.id.txv_readpersonal:
                gotoPersonal();
                break;

            case R.id.txv_take_photo:
                gotoTakePhoto();
                break;
        }
    }

}
