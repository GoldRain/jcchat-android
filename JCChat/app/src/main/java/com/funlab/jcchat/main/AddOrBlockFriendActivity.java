package com.funlab.jcchat.main;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.funlab.jcchat.Base.CommonActivity;
import com.funlab.jcchat.JCChatApplication;
import com.funlab.jcchat.R;
import com.funlab.jcchat.Utils.Database;
import com.funlab.jcchat.adapter.AddOrBlockFriendAdapter;
import com.funlab.jcchat.commons.Commons;
import com.funlab.jcchat.commons.Constants;
import com.funlab.jcchat.commons.ReqConst;
import com.funlab.jcchat.model.FriendEntity;
import com.funlab.jcchat.model.RoomEntity;
import com.funlab.jcchat.model.UserEntity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class AddOrBlockFriendActivity extends CommonActivity implements View.OnClickListener{

    private TextView ui_txvTitle;
    private Button ui_btnAddBlock;

    private ListView _friendList = null;
    private AddOrBlockFriendAdapter _addOrBlockFriendAdapter = null;

    private UserEntity _user;

    private String _addOrBlock = "";
    private RoomEntity _roomEntity = null;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addorblock_friend);

        _user = Commons.g_user;

        _addOrBlock = getIntent().getStringExtra(Constants.KEY_ADD_BLOCK);
        _roomEntity = (RoomEntity) getIntent().getSerializableExtra(Constants.KEY_ROOM);

        loadLayout();
    }

    public void loadLayout(){

        ui_txvTitle = (TextView)findViewById(R.id.txv_title);
        if (_addOrBlock.equals(Constants.KEY_ADD))
            ui_txvTitle.setText(getString(R.string.add_friend));
        else
            ui_txvTitle.setText(getString(R.string.block_friend));

        ui_btnAddBlock = (Button) findViewById(R.id.btn_add_block);
        ui_btnAddBlock.setText(ui_txvTitle.getText());
        ui_btnAddBlock.setOnClickListener(this);

        _friendList = (ListView) findViewById(R.id.lstFriend);
        _addOrBlockFriendAdapter = new AddOrBlockFriendAdapter(this);

        for (FriendEntity friendEntity : _roomEntity.get_participantList()) {
            friendEntity.set_isSelected(false);

            if (!_user.isFriend(friendEntity.get_idx()))
                _addOrBlockFriendAdapter.addItem(friendEntity);
        }

        _friendList.setAdapter(_addOrBlockFriendAdapter);

        TextView txvClose = (TextView) findViewById(R.id.txv_close);
        txvClose.setOnClickListener(this);

    }

    public void processAddBlock() {

        if (_addOrBlockFriendAdapter.getSelectedFriends().size() > 0) {

            if (_addOrBlock.equals(Constants.KEY_ADD))
                showAddPopup();
            else
                showBlockPopup();
        }
    }

    public void showAddPopup() {

        AlertDialog alertDialog = new AlertDialog.Builder(_context).create();

        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(getString(R.string.add_confirm));

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,
                _context.getString(R.string.ok),

                new android.content.DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        addFriends();
                    }
                });

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE,
                getString(R.string.cancel),

                new android.content.DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        alertDialog.show();
    }

    public void addFriends() {

        showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_ADDFRIENDLIST;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url , new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {

                parseAddFriendResponse(json);
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {
               showAlertDialog(getString(R.string.error));
            }
        }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();

                try {
                    params.put(ReqConst.PARAM_ID, String.valueOf(_user.get_idx()));

                    JSONArray friendIds = new JSONArray();

                    for (FriendEntity friendEntity : _addOrBlockFriendAdapter.getSelectedFriends()) {
                        friendIds.put(String.valueOf(friendEntity.get_idx()));
                    }

                    params.put(ReqConst.PARAM_FRIENDLIST, friendIds.toString());

                } catch (Exception e) {
                }

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        JCChatApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    public void parseAddFriendResponse(String json){

        try{

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                for (FriendEntity friendEntity : _addOrBlockFriendAdapter.getSelectedFriends()) {
                    _user.get_friendList().add(friendEntity);
                    _addOrBlockFriendAdapter.removeItem(friendEntity);
                }

                _addOrBlockFriendAdapter.notifyDataSetChanged();

                showAlertDialog(getString(R.string.addfriend_success));
            }

        } catch (JSONException e){
            e.printStackTrace();
            showAlertDialog(getString(R.string.error));
        }

        closeProgress();
    }

    public void showBlockPopup() {

        AlertDialog alertDialog = new AlertDialog.Builder(_context).create();

        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(getString(R.string.blcok_confirm));

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,
                _context.getString(R.string.ok),

                new android.content.DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        blockFriends();
                    }
                });

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE,
                getString(R.string.cancel),

                new android.content.DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        alertDialog.show();
    }

    public void blockFriends() {

        String url = ReqConst.SERVER_URL + ReqConst.REQ_BLOCKFRIENDLIST;

        showProgress();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url , new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {

                parseBlockFriendResponse(json);
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                showAlertDialog(getString(R.string.error));
            }
        }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();

                try {
                    params.put(ReqConst.PARAM_ID, String.valueOf(_user.get_idx()));

                    JSONArray friendIds = new JSONArray();

                    for (FriendEntity friendEntity : _addOrBlockFriendAdapter.getSelectedFriends()) {
                        friendIds.put(String.valueOf(friendEntity.get_idx()));
                    }

                    params.put(ReqConst.PARAM_FRIENDLIST, friendIds.toString());
                    params.put(ReqConst.PARAM_BLCOKSTATUS, "0");

                } catch (Exception e) {
                }

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        JCChatApplication.getInstance().addToRequestQueue(stringRequest, url);

    }

    public void parseBlockFriendResponse(String json) {

        try{

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                for (FriendEntity friendEntity : _addOrBlockFriendAdapter.getSelectedFriends()) {
                    friendEntity.set_blockStatus(0);
                    _user.get_friendList().add(friendEntity);
                    _addOrBlockFriendAdapter.removeItem(friendEntity);
                    Database.createBlock(friendEntity.get_idx());
                }

                _addOrBlockFriendAdapter.notifyDataSetChanged();

                showAlertDialog(getString(R.string.blockfriend_success));
            }

        } catch (JSONException e){
            e.printStackTrace();
            showAlertDialog(getString(R.string.error));
        }

        closeProgress();
    }


    public void onClick(View view){

        if (view.getId() == R.id.txv_close){
            finish();
        } else if (view.getId() == R.id.btn_add_block) {
            processAddBlock();
        }
    }

}
