package com.funlab.jcchat.main;

import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.funlab.jcchat.Base.CommonActivity;
import com.funlab.jcchat.JCChatApplication;
import com.funlab.jcchat.R;
import com.funlab.jcchat.commons.Constants;
import com.funlab.jcchat.commons.ReqConst;

import org.json.JSONException;
import org.json.JSONObject;

public class VerificationActivity extends CommonActivity implements View.OnClickListener {

    private TextView ui_txvClose;
    private TextView ui_txvEmail_transfer;
    private EditText ui_edtVerNum;
    private Button ui_btnConfirm;

    private String _email = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);

        Intent intent = getIntent();
        _email = intent.getStringExtra(Constants.KEY_EMAIL);

        loadLayout();
    }

    public void loadLayout(){

        ui_edtVerNum = (EditText)findViewById(R.id.edt_ver_num);

        /*Changing the letter color in one TextView*/
        ui_txvEmail_transfer = (TextView)findViewById(R.id.txv_email_transfer);
        int color = getResources().getColor(R.color.email);

        if (getLanguage().equals("ko") || getLanguage().equals("ja")) {
            SpannableStringBuilder builder = new SpannableStringBuilder(_email + getString(R.string.verify_email));
            builder.setSpan(new ForegroundColorSpan(color), 0, _email.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            ui_txvEmail_transfer.append(builder);
        } else {
            SpannableStringBuilder builder = new SpannableStringBuilder(getString(R.string.verify_email) + " " + _email);
            builder.setSpan(new ForegroundColorSpan(color), getString(R.string.verify_email).length() + 1, getString(R.string.verify_email).length() + 1 + _email.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            ui_txvEmail_transfer.append(builder);
        }

        ui_txvClose = (TextView)findViewById(R.id.txv_close);
        ui_txvClose.setOnClickListener(this);

        ui_btnConfirm = (Button)findViewById(R.id.btn_confirm);
        ui_btnConfirm.setOnClickListener(this);
    }

    public void onClick(View view){

        switch (view.getId()){

            case R.id.btn_confirm:

                if(isValid()){
                    confirmAuthCode();
                }
                break;
            case R.id.txv_close:
                finish();
                break;
        }
    }

    public void confirmAuthCode(){

        int auth_code = 0;

        try {
            auth_code = Integer.parseInt(ui_edtVerNum.getText().toString().trim());
        } catch (Exception ex) {}

        String url = ReqConst.SERVER_URL + ReqConst.REQ_CONFIRMAUTHCODE;
        String params = String.format("/%s/%d", _email, auth_code);

        url += params;

        showProgress();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>(){

            @Override
            public void onResponse(String json) {
                process(json);
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        JCChatApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    public void process(String json){

        closeProgress();

        try{
            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if(result_code == ReqConst.CODE_SUCCESS){

                showToast(getString(R.string.auth_success));
                gotoPwdRestConfirmActivity();

            }else {
                showToast(getString(R.string.auth_fail));
            }

        }catch (JSONException e){
            e.printStackTrace();
            showToast(getString(R.string.auth_fail));
        }
    }

    public void gotoPwdRestConfirmActivity(){

        Intent intent = new Intent(this, PwdResetConfirmActivity.class);
        intent.putExtra(Constants.KEY_EMAIL, _email);
        startActivity(intent);
        overridePendingTransition(0,0);
        finish();
    }

    public boolean isValid(){

        if(ui_edtVerNum.getText().length() == 0){

            showAlertDialog(getString(R.string.inputVerificationNum));
            return false;
        }

        return true;
    }

}
