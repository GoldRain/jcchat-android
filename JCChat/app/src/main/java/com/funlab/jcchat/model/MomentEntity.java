package com.funlab.jcchat.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by HugeRain on 2/15/2017.
 */

public class MomentEntity implements Serializable {

    int _id = 0;
    String _time = "";
    String _date = "";
    String _month = "";
    String _year = "";
    String _comment = "";
    String _videoUrl = "";
    String _thumb_video = "";
    ArrayList<String> _photos = new ArrayList<>();
    ArrayList<ImageDataEntity> _images_data = new ArrayList<>();

    public int get_id() {return _id;}
    public void set_id(int _id){ this._id = _id ;}

    public String get_time() {
        return _time;
    }

    public void set_time(String _time) {
        this._time = _time;
    }

    public String get_date() {
        return _date;
    }

    public void set_date(String _date) {
        this._date = _date;
    }

    public String get_month() {
        return _month;
    }

    public void set_month(String _month) {
        this._month = _month;
    }

    public String get_year() {
        return _year;
    }

    public void set_year(String _year) {
        this._year = _year;
    }

    public String get_comment(){ return _comment;}
    public void set_comment(String _comment){this._comment = _comment;}

    public String get_thumb_video(){return _thumb_video;}
    public void set_thumb_video(String _thumb_video){this._thumb_video = _thumb_video;}

    public String get_videoUrl() {
        return _videoUrl;
    }

    public void set_videoUrl(String _videoUrl) {
        this._videoUrl = _videoUrl;
    }

    public ArrayList<String> get_photos() {
        return _photos;
    }

    public void set_photos(ArrayList<String> _photos) {
        this._photos = _photos;
    }

    public ArrayList<ImageDataEntity> get_images_data() {
        return _images_data;
    }

    public void set_images_data(ArrayList<ImageDataEntity> _images_data) {
        this._images_data = _images_data;
    }
}
