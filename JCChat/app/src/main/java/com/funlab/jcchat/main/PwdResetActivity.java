package com.funlab.jcchat.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.funlab.jcchat.Base.CommonActivity;
import com.funlab.jcchat.JCChatApplication;
import com.funlab.jcchat.R;
import com.funlab.jcchat.commons.Constants;
import com.funlab.jcchat.commons.ReqConst;

import org.json.JSONException;
import org.json.JSONObject;

public class PwdResetActivity extends CommonActivity implements View.OnClickListener{

    private TextView ui_txvClose;
    private EditText ui_edtEmail;
    private Button ui_btnVerificationNum;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pwd_reset);

        loadLayout();
    }

    public void loadLayout(){

        ui_txvClose = (TextView)findViewById(R.id.txv_close);
        ui_txvClose.setOnClickListener(this);

        ui_edtEmail = (EditText)findViewById(R.id.edt_email);

        ui_btnVerificationNum = (Button)findViewById(R.id.btn_verificationNum);
        ui_btnVerificationNum.setOnClickListener(this);
    }

    public void onClick(View view){

        switch (view.getId()){

            case R.id.txv_close:
                finish();
                break;
            case R.id.btn_verificationNum:
                if(isValid()){

                    getAuthCode();
                }
                break;
        }
    }

    public void getAuthCode(){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_GETAUTHCODE;
        String params = String.format("/%s", ui_edtEmail.getText().toString().trim());
        url += params;

        showProgress();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>(){

            @Override
            public void onResponse(String json) {

                parseGetAuthCodeResponse(json);
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        JCChatApplication.getInstance().addToRequestQueue(stringRequest, url);

    }

    public void parseGetAuthCodeResponse(String json){

        closeProgress();

        try{

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if(result_code == ReqConst.CODE_SUCCESS){

                gotoVerificationActivity();

            } else if(result_code == ReqConst.CODE_UNREGUSER){

                showAlertDialog(getString(R.string.unregistered_user));
            }

        }catch (JSONException e){

            e.printStackTrace();
            showAlertDialog(getString(R.string.error));
        }
    }

    public void gotoVerificationActivity(){

        Intent intent = new Intent(this, VerificationActivity.class);
        intent.putExtra(Constants.KEY_EMAIL, ui_edtEmail.getText().toString().trim());
        startActivity(intent);
        overridePendingTransition(0,0);
        finish();
    }

    public boolean isValid(){

        if(ui_edtEmail.getText().length() == 0){

            showAlertDialog(getString(R.string.inputEmail));
            return false;
        }
        return true;
    }
}
