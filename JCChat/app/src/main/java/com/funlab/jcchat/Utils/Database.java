/* Copyright 2014 Sheldon Neilson www.neilson.co.za
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package com.funlab.jcchat.Utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.funlab.jcchat.chatting.GroupChatItem;
import com.funlab.jcchat.commons.Constants;
import com.funlab.jcchat.model.RoomEntity;

import java.util.ArrayList;

/* 
 * usage:  
 * DatabaseSetup.init(egActivityOrContext); 
 * DatabaseSetup.createEntry() or DatabaseSetup.getContactNames() or DatabaseSetup.getDb() 
 * DatabaseSetup.deactivate() then job done 
 */

public class Database extends SQLiteOpenHelper {

	private static Context _context = null;

	static Database instance = null;
	static SQLiteDatabase database = null;

	static final String DATABASE_NAME = "DB";
	static final int DATABASE_VERSION = 2;

	public static final String MESSAGE_TABLE = "tbl_message";

	public static final String COLUMN_MESSAGE_ID = "_id";
	public static final String COLUMN_MESSAGE_SENDER = "sender";
	public static final String COLUMN_MESSAGE_ROOMNAME = "room_name";
	public static final String COLUMN_MESSAGE_BODY = "message";
	public static final String COLUMN_MESSAGE_TYPE = "type";
	public static final String COLUMN_MESSAGE_TIME = "time";


	public static final String ROOM_TABLE = "tbl_room";

	public static final String COLUMN_ROOM_ID = "_id";
	public static final String COLUMN_ROOM_NAME = "name";
	public static final String COLUMN_ROOM_PARTICIPANTS = "participants";
	public static final String COLUMN_ROOM_RECENT_MESSAGE = "recent_message";
	public static final String COLUMN_ROOM_RECENT_TIME = "recent_time";
	public static final String COLUMN_ROOM_RECENT_COUNTER = "recent_counter";
	public static final String COLUMN_ROOM_OWNER = "room_owner";


	public static final String BLOCK_TABLE = "tbl_block";

	public static final String COLUMN_BLOCK_ID = "_id";
	public static final String COLUMN_BLOCK_FRINED_ID = "friend_idx";


	public static void init(Context context) {

		if (null == instance) {
			instance = new Database(context);
			_context = context;
		}
	}

	public static SQLiteDatabase getDatabase() {

		if (null == database) {
			database = instance.getWritableDatabase();
		}
		return database;
	}

	public static void deactivate() {

		if (null != database && database.isOpen()) {
			database.close();
		}
		database = null;
		instance = null;
	}

	Database(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		db.execSQL("CREATE TABLE IF NOT EXISTS " + MESSAGE_TABLE + " ( "
				+ COLUMN_MESSAGE_ID + " INTEGER primary key autoincrement, "
				+ COLUMN_MESSAGE_SENDER + " INTEGER NOT NULL, "
				+ COLUMN_MESSAGE_ROOMNAME + " TEXT NOT NULL, "
				+ COLUMN_MESSAGE_BODY + " TEXT NOT NULL, "
				+ COLUMN_MESSAGE_TYPE + " INTEGER NOT NULL, "
				+ COLUMN_MESSAGE_TIME + " TEXT NOT NULL)");


		db.execSQL("CREATE TABLE IF NOT EXISTS " + ROOM_TABLE + " ( "
				+ COLUMN_ROOM_ID + " INTEGER primary key autoincrement, "
				+ COLUMN_ROOM_NAME + " TEXT NOT NULL, "
				+ COLUMN_ROOM_PARTICIPANTS + " TEXT NOT NULL, "
				+ COLUMN_ROOM_RECENT_MESSAGE + " TEXT NOT NULL, "
				+ COLUMN_ROOM_RECENT_TIME + " TEXT NOT NULL, "
				+ COLUMN_ROOM_RECENT_COUNTER + " INTEGER NOT NULL, "
				+ COLUMN_ROOM_OWNER + " INTEGER NOT NULL)");

		db.execSQL("CREATE TABLE IF NOT EXISTS " + BLOCK_TABLE + " ( "
				+ COLUMN_BLOCK_ID + " INTEGER primary key autoincrement, "
				+ COLUMN_BLOCK_FRINED_ID + " INTEGER NOT NULL)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		if (newVersion == 2) {
			db.execSQL("CREATE TABLE IF NOT EXISTS " + BLOCK_TABLE + " ( "
					+ COLUMN_BLOCK_ID + " INTEGER primary key autoincrement, "
					+ COLUMN_BLOCK_FRINED_ID + " INTEGER NOT NULL)");
		}

	}

	public static void initDatabase() {

		deleteAllRoom();
		deleteAllMessage();
		deleteAllBlocks();
	}

	public static ContentValues getContentMessageValues(GroupChatItem chatItem) {

		ContentValues cv = new ContentValues();

		cv.put(COLUMN_MESSAGE_SENDER, chatItem.getSender());
		cv.put(COLUMN_MESSAGE_ROOMNAME, chatItem.getRoomName());
		cv.put(COLUMN_MESSAGE_BODY, chatItem.getMessage());
		cv.put(COLUMN_MESSAGE_TYPE, chatItem.getType().ordinal());
		cv.put(COLUMN_MESSAGE_TIME, chatItem.getTime());

		return cv;
	}

	public static long createMessage(GroupChatItem chatItem) {

		ContentValues cv = getContentMessageValues(chatItem);
		return getDatabase().insert(MESSAGE_TABLE, null, cv);
	}

	public static int deleteAllMessage() {
		return getDatabase().delete(MESSAGE_TABLE, "1", null);
	}

	public static int deleteRoomMessage(RoomEntity roomEntity) {

		return getDatabase().delete(MESSAGE_TABLE, COLUMN_MESSAGE_ROOMNAME + " = ?", new String[]{roomEntity.get_name()});
	}

	public static ArrayList<GroupChatItem> getAllMessage() {

		ArrayList<GroupChatItem> chatItems = new ArrayList<GroupChatItem>();

		String[] columns = new String[] {
				COLUMN_MESSAGE_ID, COLUMN_MESSAGE_SENDER, COLUMN_MESSAGE_ROOMNAME, COLUMN_MESSAGE_BODY, COLUMN_MESSAGE_TYPE, COLUMN_MESSAGE_TIME };

		Cursor cursor = getDatabase().query(MESSAGE_TABLE, columns, null, null, null,
				null, null);

		if (cursor.moveToFirst()) {

			do {
				GroupChatItem chat = new GroupChatItem(cursor.getInt(1), cursor.getString(2), cursor.getString(3), cursor.getInt(4), cursor.getString(5));
				chatItems.add(chat);

			} while (cursor.moveToNext());
		}

		cursor.close();

		return chatItems;
	}

	public static ArrayList<GroupChatItem> getRecentMessage(String roomname, int count) {

		ArrayList<GroupChatItem> chatItems = new ArrayList<GroupChatItem>();

		String[] columns = new String[] {
				COLUMN_MESSAGE_ID, COLUMN_MESSAGE_SENDER, COLUMN_MESSAGE_ROOMNAME, COLUMN_MESSAGE_BODY, COLUMN_MESSAGE_TYPE, COLUMN_MESSAGE_TIME };

//		Cursor cursor = getDatabase().query(MESSAGE_TABLE, columns, COLUMN_MESSAGE_ROOMNAME + " = ?", new String[]{roomname}, null,
//				null, COLUMN_MESSAGE_ID + " DESC", String.valueOf(Constants.RECENT_MESSAGE_COUNT));

		String queryString = "SELECT * from (SELECT * from " + MESSAGE_TABLE + " where " + COLUMN_MESSAGE_ROOMNAME + " = " + "'" + roomname +
				"' ORDER BY " +  COLUMN_MESSAGE_ID + " DESC LIMIT " + String.valueOf(Constants.RECENT_MESSAGE_COUNT * count) + ") AS table1 ORDER BY " +  COLUMN_MESSAGE_ID;

		Cursor cursor = getDatabase().rawQuery(queryString, null);

		if (cursor.moveToFirst()) {

			do {
				GroupChatItem chat = new GroupChatItem(cursor.getInt(1), cursor.getString(2), cursor.getString(3), cursor.getInt(4), cursor.getString(5));
				chatItems.add(chat);

			} while (cursor.moveToNext());
		}

		cursor.close();

		return chatItems;
	}


	public static ContentValues getContentRoomValues(RoomEntity roomEntity) {

		ContentValues cv = new ContentValues();

		cv.put(COLUMN_ROOM_NAME, roomEntity.get_name());
		cv.put(COLUMN_ROOM_PARTICIPANTS, roomEntity.get_participants());
		cv.put(COLUMN_ROOM_RECENT_MESSAGE, roomEntity.get_recentContent());
		cv.put(COLUMN_ROOM_RECENT_TIME, roomEntity.get_recentTime());
		cv.put(COLUMN_ROOM_RECENT_COUNTER, roomEntity.get_recentCounter());
		cv.put(COLUMN_ROOM_OWNER, roomEntity.get_ownerIdx());

		return cv;
	}

	public static long createRoom(RoomEntity roomEntity) {

		ContentValues cv = getContentRoomValues(roomEntity);

		return getDatabase().insert(ROOM_TABLE, null, cv);
	}

	public static int updateRoom(RoomEntity roomEntity) {

		ContentValues cv = getContentRoomValues(roomEntity);

		return getDatabase().update(ROOM_TABLE,
				cv, COLUMN_ROOM_NAME + "=" + "'" + roomEntity.get_name() + "'", null);
	}

	public static boolean isExistRoom(RoomEntity roomEntity) {

		String Query = "Select * from " + ROOM_TABLE + " where " + COLUMN_ROOM_NAME + " = " + "'" + roomEntity.get_name() + "'";

		Cursor cursor = getDatabase().rawQuery(Query, null);
		if(cursor.getCount() <= 0){
			cursor.close();
			return false;
		}
		cursor.close();
		return true;
	}

	public static int deleteRoom(RoomEntity roomEntity) {

		deleteRoomMessage(roomEntity);

		return getDatabase().delete(
				ROOM_TABLE,
				COLUMN_ROOM_NAME + "=" + "'" + roomEntity.get_name() + "'", null);
	}

	public static int deleteAllRoom() {

		return getDatabase().delete(ROOM_TABLE, "1", null);
	}

	public static ArrayList<RoomEntity> getAllRoom() {

		ArrayList<RoomEntity> roomEntities = new ArrayList<RoomEntity>();

		String[] columns = new String[] {
				COLUMN_ROOM_ID, COLUMN_ROOM_NAME, COLUMN_ROOM_PARTICIPANTS, COLUMN_ROOM_RECENT_MESSAGE,
				COLUMN_ROOM_RECENT_TIME, COLUMN_ROOM_RECENT_COUNTER, COLUMN_ROOM_OWNER };

		Cursor cursor = getDatabase().query(ROOM_TABLE, columns, null, null, null,
				null, null);

		if (cursor.moveToFirst()) {

			do {
				RoomEntity room = new RoomEntity(cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getInt(5), cursor.getInt(6));
				roomEntities.add(room);

			} while (cursor.moveToNext());
		}

		cursor.close();

		return roomEntities;
	}


	public static ContentValues getContentBlockValues(int idx) {

		ContentValues cv = new ContentValues();
		cv.put(COLUMN_BLOCK_FRINED_ID, String.valueOf(idx));

		return cv;
	}

	public static long createBlock(int idx) {

		ContentValues cv = getContentBlockValues(idx);

		return getDatabase().insert(BLOCK_TABLE, null, cv);
	}

	public static int deleteBlock(int idx) {

		return getDatabase().delete(
				BLOCK_TABLE,
				COLUMN_BLOCK_FRINED_ID + "=" + "'" + String.valueOf(idx) + "'", null);
	}

	public static int deleteAllBlocks() {

		return getDatabase().delete(BLOCK_TABLE, "1", null);
	}

	public static ArrayList<Integer> getAllBlocks() {

		ArrayList<Integer> idxs = new ArrayList<Integer>();

		String[] columns = new String[] {
				COLUMN_BLOCK_ID, COLUMN_BLOCK_FRINED_ID };

		Cursor cursor = getDatabase().query(BLOCK_TABLE, columns, null, null, null,
				null, null);

		if (cursor.moveToFirst()) {

			do {
				idxs.add(Integer.valueOf(cursor.getInt(1)));

			} while (cursor.moveToNext());
		}

		cursor.close();

		return idxs;
	}

	public static boolean isBlocked(int idx) {

		return getAllBlocks().contains(Integer.valueOf(idx));
	}
}