package com.funlab.jcchat.main;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.funlab.jcchat.Base.CommonActivity;
import com.funlab.jcchat.JCChatApplication;
import com.funlab.jcchat.R;
import com.funlab.jcchat.Utils.CustomUncaughtExceptionHandler;
import com.funlab.jcchat.commons.Commons;
import com.funlab.jcchat.commons.Constants;
import com.funlab.jcchat.commons.ReqConst;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;

public class IntroActivity extends CommonActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Commons.g_introActivity = this;

        setContentView(R.layout.activity_intro);

        Thread.UncaughtExceptionHandler handler = Thread
                .getDefaultUncaughtExceptionHandler();

        if (!(handler instanceof CustomUncaughtExceptionHandler)) {
            Thread.setDefaultUncaughtExceptionHandler(new CustomUncaughtExceptionHandler(
                    this));
        }

        getVersionInfo();

    }

    public void getVersionInfo() {

        String url = ReqConst.SERVER_URL + ReqConst.REQ_GETVERSIONINFO;

        String params = "/0";    // android

        url += params;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {

                parseVersionResponse(json);
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                showToast(getString(R.string.error));
                finish();
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        JCChatApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    public void parseVersionResponse(String json) {

        try {

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS) {

                String version = response.getString(ReqConst.RES_VERSION);
                checkVersion(version);
            }

        } catch (Exception ex) {

            showToast(getString(R.string.error));
            finish();
        }
    }

    public String getVersion() {

        String versionName = "1.0";

        try {
            versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return versionName;
    }

    public void checkVersion(String version) {

        try {
            String strServerVer = version.replace(".", "");
            int serverVer = Integer.valueOf(strServerVer);
            if (serverVer < 10)
                serverVer *= 100;
            else if (serverVer < 100)
                serverVer *= 10;

            String strAppVer = getVersion().replace(".", "");
            int appVer = Integer.valueOf(strAppVer);
            if (appVer < 10)
                appVer *= 100;
            else if (appVer < 100)
                appVer *= 10;

            if (appVer >= serverVer) {
                gotoLogin();
            } else {
                showUpdateDialog();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            finish();
        }
    }

    public void gotoLogin() {

        final String room = getIntent().getStringExtra(Constants.KEY_ROOM);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(IntroActivity.this, LoginActivity.class);

                if (room != null)
                    intent.putExtra(Constants.KEY_ROOM, room);

                startActivity(intent);
                finish();
            }
        }, Constants.SPLASH_TIME);
    }


    public void showUpdateDialog() {

        AlertDialog alertDialog = new AlertDialog.Builder(_context).create();

        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(getString(R.string.update_version));

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,
                _context.getString(R.string.ok),

                new android.content.DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        gotoStore();
                        finish();
                    }
                });

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE,
                getString(R.string.cancel),

                new android.content.DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });

        alertDialog.show();

    }

    public void gotoStore() {

        final String appPackageName = getPackageName();

        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }
}
