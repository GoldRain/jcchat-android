package com.funlab.jcchat.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.funlab.jcchat.JCChatApplication;
import com.funlab.jcchat.R;
import com.funlab.jcchat.Utils.CirculaireNetworkImageView;
import com.funlab.jcchat.model.FriendEntity;

import java.util.ArrayList;

/**
 * Created by HGS on 12/13/2015.
 */
public class BlockFriendAdapter extends BaseAdapter{

    private Context _context = null;
    private ArrayList<FriendEntity> _blockFriendDatas = new ArrayList<FriendEntity>();

    private ImageLoader _imageLoader;

    public BlockFriendAdapter(Context _context){

        super();
        this._context = _context;

        _imageLoader = JCChatApplication.getInstance().getImageLoader();
    }

    @Override
    public int getCount(){

        return _blockFriendDatas.size();
    }

    @Override
    public Object getItem(int position){

        return _blockFriendDatas.get(position);
    }

    @Override
    public long getItemId(int position){

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){

        BlockFriendHolder blockFriendHolder;

        if(convertView == null){
            blockFriendHolder = new BlockFriendHolder();

            LayoutInflater inflater = (LayoutInflater)_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.block_friend_list_item, null);

            blockFriendHolder.imvBlockFriendPhoto = (CirculaireNetworkImageView)convertView.findViewById(R.id.imv_blockFriendPhoto);
            blockFriendHolder.txvBlockFriendName = (TextView)convertView.findViewById(R.id.txv_blockFriendName);

            convertView.setTag(blockFriendHolder);
        }else {
            blockFriendHolder = (BlockFriendHolder)convertView.getTag();
        }

        FriendEntity friend = _blockFriendDatas.get(position);

        blockFriendHolder.txvBlockFriendName.setText(friend.get_name());

        if (friend.get_photoUrl().length() > 0)
            blockFriendHolder.imvBlockFriendPhoto.setImageUrl(friend.get_photoUrl(), _imageLoader);
        else
            blockFriendHolder.imvBlockFriendPhoto.setImageResource(R.drawable.bg_non_profile1);


        return convertView;
    }

    public void clearAll() {

        _blockFriendDatas.clear();
    }

    public void addItem(FriendEntity entity){

        _blockFriendDatas.add(entity);
    }


    public class BlockFriendHolder {

        public CirculaireNetworkImageView imvBlockFriendPhoto;
        public TextView txvBlockFriendName;
    }
}
