package com.funlab.jcchat.Utils;

/**
 * Created by HGS on 12/18/2015.
 */
import android.content.*;
import android.graphics.*;
import android.graphics.drawable.*;
import android.util.*;

import com.android.volley.toolbox.*;

public class RectNetImageView extends NetworkImageView {

    private static final int FADE_IN_TIME_MS = 250;

    public RectNetImageView(Context context) {
        super(context);
    }

    public RectNetImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RectNetImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void setImageBitmap(Bitmap bm) {
        TransitionDrawable td = new TransitionDrawable(new Drawable[] {
                new ColorDrawable(Color.TRANSPARENT),
                new BitmapDrawable(getContext().getResources(), bm) });

        setImageDrawable(td);
        td.setCrossFadeEnabled(true);
        td.startTransition(FADE_IN_TIME_MS);

    }
}
