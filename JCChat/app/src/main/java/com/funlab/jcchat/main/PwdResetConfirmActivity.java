package com.funlab.jcchat.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.funlab.jcchat.Base.CommonActivity;
import com.funlab.jcchat.JCChatApplication;
import com.funlab.jcchat.R;
import com.funlab.jcchat.commons.Constants;
import com.funlab.jcchat.commons.ReqConst;
import com.funlab.jcchat.preference.PrefConst;
import com.funlab.jcchat.preference.Preference;

import org.json.JSONException;
import org.json.JSONObject;

public class PwdResetConfirmActivity extends CommonActivity implements View.OnClickListener {

    private TextView ui_txvClose;
    private EditText ui_edtNewPwd, ui_edtConfirmPwd;
    private Button ui_btnConfirm;
    private String _email = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pwd_reset_confirm);

        Intent intent = getIntent();
        _email = intent.getStringExtra(Constants.KEY_EMAIL);

        loadLayout();
    }

    public void loadLayout(){

        ui_txvClose = (TextView)findViewById(R.id.txv_close);
        ui_txvClose.setOnClickListener(this);

        ui_edtNewPwd = (EditText)findViewById(R.id.edt_password);
        ui_edtConfirmPwd = (EditText)findViewById(R.id.edt_confirm);

        ui_btnConfirm = (Button)findViewById(R.id.btn_confirm);
        ui_btnConfirm.setOnClickListener(this);
    }

    public void onClick(View view){

        switch (view.getId()){

            case R.id.txv_close:
                onClose();
                break;

            case R.id.btn_confirm:

                if(isValid()){
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(ui_edtNewPwd.getWindowToken(), 0);
                    processConfirmPwd();
                }
                break;
        }
    }

    public void processConfirmPwd(){

        if (_email == null)
            return;

        String url = ReqConst.SERVER_URL + ReqConst.REQ_RESETNEWPASSWORD;
        String params = String.format("/%s/%s", _email.trim(), ui_edtNewPwd.getText().toString().trim());

        url += params;

        showProgress();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                parseConfirmPwd(json);
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        JCChatApplication.getInstance().addToRequestQueue(stringRequest, url);

    }

    public void parseConfirmPwd(String json){

        closeProgress();
        try{
            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                showAlertDialog(getString(R.string.successChangePwd)) ;
                Preference.getInstance().put(this,
                        PrefConst.PREFKEY_USERPWD, ui_edtNewPwd.getText().toString().trim());

            }else {
                showAlertDialog(getString(R.string.failChangePwd)) ;
            }
        }catch (JSONException e){
            e.printStackTrace();
            showAlertDialog(getString(R.string.failChangePwd)) ;
        }
    }

    public void onClose(){
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(ui_edtNewPwd.getWindowToken(), 0);
        finish();
    }

    public boolean isValid(){

        if(ui_edtNewPwd.getText().length() == 0){
            showAlertDialog(getString(R.string.inputPassword));
            return false;
        }else if(ui_edtNewPwd.getText().length()<6){
            showAlertDialog(getString(R.string.PwdLength));
            return false;
        }else if(ui_edtConfirmPwd.getText().length() == 0){

            showAlertDialog(getString(R.string.inputPwdConfirm));

            return false;
        }else if(!ui_edtNewPwd.getText().toString().equals(ui_edtConfirmPwd.getText().toString())){
            showAlertDialog(getString(R.string.checkPwd));
            return false;
        }
        return true;
    }
}
