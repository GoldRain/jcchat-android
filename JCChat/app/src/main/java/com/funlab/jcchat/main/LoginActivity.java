package com.funlab.jcchat.main;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.funlab.jcchat.Base.CommonActivity;
import com.funlab.jcchat.JCChatApplication;
import com.funlab.jcchat.R;
import com.funlab.jcchat.Utils.Database;
import com.funlab.jcchat.chatting.ConnectionMgrService;
import com.funlab.jcchat.chatting.LoggedInEvent;
import com.funlab.jcchat.commons.Commons;
import com.funlab.jcchat.commons.Constants;
import com.funlab.jcchat.commons.ReqConst;
import com.funlab.jcchat.logger.Logger;
import com.funlab.jcchat.model.FriendEntity;
import com.funlab.jcchat.model.RoomEntity;
import com.funlab.jcchat.model.UserEntity;
import com.funlab.jcchat.preference.PrefConst;
import com.funlab.jcchat.preference.Preference;

import org.jivesoftware.smack.SmackConfiguration;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.greenrobot.event.EventBus;

public class LoginActivity extends CommonActivity implements View.OnClickListener{

    public static final int REGISTER_CODE = 100;

    private EditText ui_edtEmail, ui_edtPassword;

    private UserEntity _user;

    private int _reqCounter = 0;

    private String _room = "";

    private ArrayList<String> _contacts = new ArrayList<>();

    String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE,  android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_SMS,
            android.Manifest.permission.CAMERA, Manifest.permission.CALL_PHONE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.RECORD_AUDIO, Manifest.permission.BIND_VOICE_INTERACTION, Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS};

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        _room = getIntent().getStringExtra(Constants.KEY_ROOM);

        Database.init(getApplicationContext());
        SmackConfiguration.DEBUG = true;

        initValues();
        checkAllPermission();
        loadLayout();
    }

       /*==================== Permission========================================*/
    public void checkAllPermission() {

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        if (hasPermissions(this, PERMISSIONS)){

        }else {
            ActivityCompat.requestPermissions(this, PERMISSIONS, 101);
        }
    }

    /*//////////////////////////////////////////////////////////////////////////////*/

    public boolean hasPermissions(Context context, String... permissions) {

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {

            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {

                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == Constants.MY_PEQUEST_CODE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        }


    }

    //==================================================================
/*Auto Login process*/
    boolean _isFromLogout = false;

    private void initValues(){

        Intent intent = getIntent();
        try{
            _isFromLogout = intent.getBooleanExtra(Constants.KEY_LOGOUT, false);
        } catch (Exception e){
        }
    }

    public void loadLayout(){

        ui_edtEmail = (EditText)findViewById(R.id.edt_email);
        ui_edtPassword = (EditText)findViewById(R.id.edt_password);

        TextView txvForget = (TextView)findViewById(R.id.txv_forget);
        txvForget.setOnClickListener(this);

        Button btnLogin = (Button)findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(this);

        Button btnRegister = (Button)findViewById(R.id.btn_register);
        btnRegister.setOnClickListener(this);

        LinearLayout linearLayout = (LinearLayout)findViewById(R.id.lyt_container);
        linearLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(ui_edtEmail.getWindowToken(), 0);

                return false;
            }
        });

        if(_isFromLogout){

            //save user to empty.
            Preference.getInstance().put(this,
                    PrefConst.PREFKEY_USEREMAIL, "");
            Preference.getInstance().put(this,
                    PrefConst.PREFKEY_USERPWD, "");

            ui_edtEmail.setText("");
            ui_edtPassword.setText("");

        } else {

            // load saved user
            String email = Preference.getInstance().getValue(this,
                    PrefConst.PREFKEY_USEREMAIL, "");
            String userpwd = Preference.getInstance().getValue(this,
                    PrefConst.PREFKEY_USERPWD, "");

            ui_edtEmail.setText(email);
            ui_edtPassword.setText(userpwd);

            if(email.length() > 0 && userpwd.length() > 0){

                processLogin();
            }
        }
    }

    public void onClick(View view){

        switch (view.getId()){
            case R.id.btn_login:
                if(isValid()){
                    processLogin();
                }
                break;

            case R.id.btn_register:
                gotoRegister();
                break;

            case R.id.txv_forget:
                gotoPwdReset();
                break;
        }
    }


    public void processLogin(){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_LOGIN;

        String params = String.format("/%s/%s", ui_edtEmail.getText().toString(), ui_edtPassword.getText().toString());

        url += params;

        showProgress();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {

                parseLoginResponse(json);
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        JCChatApplication.getInstance().addToRequestQueue(stringRequest, url);

    }

    public void parseLoginResponse(String json){

        try{

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if(result_code == ReqConst.CODE_SUCCESS){

                _user = new UserEntity();

                _user.set_idx(response.getInt(ReqConst.RES_IDX));
                _user.set_name(response.getString(ReqConst.RES_NAME));
                _user.set_email(response.getString(ReqConst.RES_EMAIL));
                _user.set_label(response.getString(ReqConst.RES_LABEL));
                _user.set_bgUrl(response.getString(ReqConst.RES_BG_URL));
                _user.set_photoUrl(response.getString(ReqConst.RES_PHOTO_URL));
                _user.set_password(ui_edtPassword.getText().toString().trim());
                _user.set_allowFriend(response.getInt(ReqConst.RES_ALLOWFRIEND));

                Preference.getInstance().put(this,
                        PrefConst.PREFKEY_USEREMAIL, ui_edtEmail.getText().toString().trim());
                Preference.getInstance().put(this,
                        PrefConst.PREFKEY_USERPWD, ui_edtPassword.getText().toString().trim());
                Preference.getInstance().put(this,
                        PrefConst.PREFKEY_XMPPID, String.valueOf(_user.get_idx()));

                String lastLoginEmail = Preference.getInstance().getValue(getApplicationContext(), PrefConst.PREFKEY_LASTLOGINID, "");

                // init database if new user login
                if (!lastLoginEmail.equals(_user.get_email())) {
                    Preference.getInstance().put(getApplicationContext(), PrefConst.PREFKEY_LASTLOGINID, _user.get_email());
                    Database.initDatabase();
                }

                Commons.g_user = _user;

                addContacts();

            }else if(result_code == ReqConst.CODE_UNREGUSER){

                closeProgress();
                showAlertDialog(getString(R.string.unregistered_user));

            }else if(result_code == ReqConst.CODE_INVALIDPWD){

                closeProgress();
                showAlertDialog(getString(R.string.checkPwd));
            }
        }catch (JSONException e){
            closeProgress();
            e.printStackTrace();

            showAlertDialog(getString(R.string.error));
        }
    }

    public void addContacts() {

        _contacts = getContactList();

        if (_contacts.size() > 0) {
            addFriendByPhonenumber();
        } else {
            registerToken();
        }
    }

    public void addFriendByPhonenumber() {

        String url = ReqConst.SERVER_URL + ReqConst.REQ_ADDFRIENDBYPHONENUMBER;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url , new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {

                parseAddFriendResponse(json);
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                registerToken();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();

                try {
                    params.put(ReqConst.PARAM_ID, String.valueOf(_user.get_idx()));

                    JSONArray phonenumbers = new JSONArray();

                    for (String contact : _contacts) {
                        phonenumbers.put(contact);
                    }

                    params.put(ReqConst.PARAM_PHONE_NUMBERS, phonenumbers.toString());

                } catch (Exception e) {
                }

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        JCChatApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    public void parseAddFriendResponse(String json){
        registerToken();
    }


    public ArrayList<String> getContactList() {

        ArrayList<String> contactList = new ArrayList<String>();

        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;

        String[] projection = new String[] {
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
                ContactsContract.CommonDataKinds.Phone.NUMBER,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME };

        String[] selectionArgs = null;

        String sortOrder = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME
                + " COLLATE LOCALIZED ASC";

        ContentResolver cr = getContentResolver();

        Cursor contactCursor = cr.query(uri, projection, null, selectionArgs,
                sortOrder);

        if (contactCursor.moveToFirst()) {

            do {
                String phonenumber = contactCursor.getString(1).replaceAll("-", "");
                phonenumber = phonenumber.replace("+", "0");

                if (phonenumber.length() > Constants.PHONENUMBER_LENGTH)
                    phonenumber = phonenumber.substring(phonenumber.length() - Constants.PHONENUMBER_LENGTH);

                contactList.add(phonenumber);

            } while (contactCursor.moveToNext());
        }

        contactCursor.close();

        return contactList;
    }

    public void registerToken() {

        String url = ReqConst.SERVER_URL + ReqConst.REQ_REGISTERTOKEN;

        String token = JCChatApplication.getInstance().getGcmToken();

        if (token == null || token.length() == 0) {
            getFriendList();
            return;
        }

        String params = String.format("/%d/%s", _user.get_idx(), token);
        url += params;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {

                parseTokenResponse(json);
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                getFriendList();
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        JCChatApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    public void parseTokenResponse(String json){

        try{

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

            }

        }catch (JSONException e){
            e.printStackTrace();
        }

        getFriendList();
    }

    public void getFriendList() {

        String url = ReqConst.SERVER_URL + ReqConst.REQ_GETFRIENDLIST;

        String params = String.format("/%s", _user.get_idx());
        url += params;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {

                parseFriendResponse(json);
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                loadRoomInfo();
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        JCChatApplication.getInstance().addToRequestQueue(stringRequest, url);
    }


    public void parseFriendResponse(String json){

        try{

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                JSONArray friends = response.getJSONArray(ReqConst.RES_FRIENDLIST);

                Database.deleteAllBlocks();

                for (int i = 0; i < friends.length(); i++) {

                    JSONObject friend = (JSONObject) friends.get(i);
                    FriendEntity entity = new FriendEntity();

                    entity.set_idx(friend.getInt(ReqConst.RES_ID));
                    entity.set_name(friend.getString(ReqConst.RES_NAME));
                    entity.set_label(friend.getString(ReqConst.RES_LABEL));
                    entity.set_photoUrl(friend.getString(ReqConst.RES_PHOTO_URL));
                    entity.set_bgUrl(friend.getString(ReqConst.RES_BG_URL));
                    entity.set_blockStatus(friend.getInt(ReqConst.RES_BLOCK_STATUS));

                    if (entity.get_blockStatus() == 0)
                        Database.createBlock(entity.get_idx());

                    _user.get_friendList().add(entity);
                }

            }

        }catch (JSONException e){
            e.printStackTrace();
        }

        loadRoomInfo();
    }

    public void loadRoomInfo() {

        ArrayList<RoomEntity> roomEntities =  Database.getAllRoom();

        _reqCounter = roomEntities.size();

        if (_reqCounter > 0) {

            for (RoomEntity room : roomEntities) {
                getRoomInfo(room);
            }
        } else {
            loginToChattingServer();
        }
    }

    public void getRoomInfo(final RoomEntity room){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_GETROOMINFO;

        String params = String.format("/%s", room.get_participants());
        url += params;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                parseRoomInfoResponse(json, room);
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                _reqCounter--;

                if (_reqCounter <= 0)
                    loginToChattingServer();
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        JCChatApplication.getInstance().addToRequestQueue(stringRequest, url);

    }

    public void parseRoomInfoResponse(String json, RoomEntity room){

        _reqCounter--;

        try {

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                JSONArray friends = response.getJSONArray(ReqConst.RES_USERLIST);

                ArrayList<FriendEntity> participants = new ArrayList<FriendEntity>();

                // get participants data
                for (int i = 0; i < friends.length(); i++) {

                    JSONObject friend = (JSONObject) friends.get(i);

                    int idx = friend.getInt(ReqConst.RES_ID);

                    if (idx == Commons.g_user.get_idx())
                        continue;

                    if (Commons.g_user.isFriend(idx)) {

                        participants.add(Commons.g_user.getFriend(idx));

                    } else {

                        FriendEntity entity = new FriendEntity();
                        entity.set_idx(friend.getInt(ReqConst.RES_ID));
                        entity.set_name(friend.getString(ReqConst.RES_NAME));
                        entity.set_label(friend.getString(ReqConst.RES_LABEL));
                        entity.set_photoUrl(friend.getString(ReqConst.RES_PHOTO_URL));
                        entity.set_bgUrl(friend.getString(ReqConst.RES_BG_URL));
                        participants.add(entity);
                    }
                }

                // make room
                room.set_participantList(participants);

                if (!Commons.g_user.get_roomList().contains(room))
                    Commons.g_user.get_roomList().add(room);
            }

        } catch (JSONException e){
            e.printStackTrace();
        }

        if (_reqCounter <= 0) {
            loginToChattingServer();
        }

    }

    public void loginToChattingServer() {

        if (ConnectionMgrService.mConnection == null || !ConnectionMgrService.mConnection.isConnected()) {

            Intent mServiceIntent = new Intent(LoginActivity.this, ConnectionMgrService.class);
            mServiceIntent.putExtra(Constants.XMPP_START, Constants.XMPP_FROMLOGIN);
            startService(mServiceIntent);

            Logger.d("XMPP", "not connected go to login from login activity");

        } else {
            closeProgress();
            gotoFriendActivity();

            Logger.d("XMPP", "already connected from broadcast");
        }
    }

    public void onEventMainThread(LoggedInEvent event) {

        closeProgress();

        if(event.isSuccessful()) {
            gotoFriendActivity();
        } else {
            showAlertDialog(getString(R.string.error));
        }
    }


    public void gotoFriendActivity(){

        Commons.g_isAppRunning = true;

        Intent intent = new Intent(this, FriendActivity.class);
        intent.putExtra(Constants.KEY_FROMLOGIN, true);

        if (_room != null) {
            intent.putExtra(Constants.KEY_ROOM, _room);
        }
        startActivity(intent);
        finish();
    }

    public void gotoRegister(){

        Intent intent = new Intent(this, RegisterActivity.class);
        startActivityForResult(intent, REGISTER_CODE);
    }

    public void gotoPwdReset(){

        Intent intent = new Intent(this, PwdResetActivity.class);
        startActivity(intent);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

            case REGISTER_CODE:

                if (resultCode == RESULT_OK) {

                    // load saved user
                    String email = Preference.getInstance().getValue(this,
                            PrefConst.PREFKEY_USEREMAIL, "");
                    String userpwd = Preference.getInstance().getValue(this,
                            PrefConst.PREFKEY_USERPWD, "");

                    ui_edtEmail.setText(email);
                    ui_edtPassword.setText(userpwd);

                    processLogin();
                }
                break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {

            onExit();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    /*check the empty string*/
    public boolean isValid(){

        if(ui_edtEmail.getText().length() == 0){
            showAlertDialog(getString(R.string.inputEmail));
            return false;
        }else if(ui_edtPassword.getText().length() == 0){
            showAlertDialog(getString(R.string.inputPassword));
            return false;
        }

        return true;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }
}


