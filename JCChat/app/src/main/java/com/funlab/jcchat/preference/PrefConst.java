package com.funlab.jcchat.preference;

/**
 * Created by HGS on 12/11/2015.
 */
public class PrefConst {

    public static final String PREFKEY_USEREMAIL = "email";
    public static final String PREFKEY_XMPPID = "XMPPID";
    public static final String PREFKEY_USERPWD = "password";

    public static final String PREFKEY_PUSH = "push_setting";
    public static final String PREFKEY_ALLOWFRIEND = "allow_friend";

    public static final String PREFKEY_LASTLOGINID = "lastlogin_id";

}
