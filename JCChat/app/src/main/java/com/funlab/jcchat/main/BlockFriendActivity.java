package com.funlab.jcchat.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.funlab.jcchat.Base.CommonActivity;
import com.funlab.jcchat.R;
import com.funlab.jcchat.adapter.BlockFriendAdapter;
import com.funlab.jcchat.commons.Commons;
import com.funlab.jcchat.model.FriendEntity;
import com.funlab.jcchat.model.UserEntity;

public class BlockFriendActivity extends CommonActivity implements View.OnClickListener{

    private TextView ui_txvEdit;

    private ListView _blockFriendList = null;
    private BlockFriendAdapter _blockFriendAdapter = null;

    private UserEntity _user;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_block_friend);

        _user = Commons.g_user;

        loadLayout();
    }

    public void loadLayout(){

        ui_txvEdit = (TextView)findViewById(R.id.tx_edit);
        ui_txvEdit.setOnClickListener(this);

        _blockFriendList = (ListView) findViewById(R.id.block_FriendLis);
        _blockFriendAdapter = new BlockFriendAdapter(this);
        _blockFriendList.setAdapter(_blockFriendAdapter);

    }

    public void getBlockFriendData(){

        _blockFriendAdapter.clearAll();

        for (int i = 0; i < _user.get_friendList().size(); i++) {

            FriendEntity friend = _user.get_friendList().get(i);

            if (friend.get_blockStatus() == 0)
                _blockFriendAdapter.addItem(_user.get_friendList().get(i));
        }

        _blockFriendAdapter.notifyDataSetChanged();

    }

    public void gotoReleaseBlockFriActivity(){

        Intent intent = new Intent(this, ReleaseBlockFriendActivity.class);
        overridePendingTransition(0, 0);
        startActivity(intent);
    }

    public void onClick(View view){

        if (view.getId() == R.id.tx_edit){

            gotoReleaseBlockFriActivity();
        }
    }

    @Override
    protected void onResume() {

        super.onResume();

        getBlockFriendData();
    }
}
