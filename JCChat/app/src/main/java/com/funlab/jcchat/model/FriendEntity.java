package com.funlab.jcchat.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by HGS on 12/16/2015.
 */
public class FriendEntity implements Serializable {

    private int _idx = 0;
    private String _name = "";
    private String _phoneNumber = "";
    private String _label = "";
    private String _bgUrl = "";
    private String _photoUrl = "";
    private int _blockStatus = 1;   // 1 unblock, 0 block
    private boolean _isSelected = false;

    ArrayList<MomentEntity> _friend_moment_list = new ArrayList<>();

    public int get_idx() {
        return _idx;
    }

    public void set_idx(int _idx) {
        this._idx = _idx;
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public String get_phoneNumber() {
        return _phoneNumber;
    }

    public void set_phoneNumber(String _phoneNumber) {
        this._phoneNumber = _phoneNumber;
    }

    public String get_label() {
        return _label;
    }

    public void set_label(String _label) {
        this._label = _label;
    }

    public String get_bgUrl() {
        return _bgUrl;
    }

    public void set_bgUrl(String _bgUrl) {
        this._bgUrl = _bgUrl;
    }

    public String get_photoUrl() {
        return _photoUrl;
    }

    public void set_photoUrl(String _photoUrl) {
        this._photoUrl = _photoUrl;
    }

    public int get_blockStatus() {
        return _blockStatus;
    }

    public void set_blockStatus(int _blockStatus) {
        this._blockStatus = _blockStatus;
    }

    public boolean is_isSelected() {
        return _isSelected;
    }

    public void set_isSelected(boolean _isSelected) {
        this._isSelected = _isSelected;
    }

    @Override
    public boolean equals(Object o) {

        FriendEntity other = (FriendEntity) o;

        if (get_idx() == other.get_idx())
            return true;

        return false;
    }

    public ArrayList<MomentEntity> get_friend_moment_list(){ return _friend_moment_list;}
    public void set_friend_moment_list(ArrayList<MomentEntity> _friend_moment_list){this._friend_moment_list = _friend_moment_list; }
}


