
D-CHAT – TERMS OF SERVICE

Last modified: 2016-01-27

INTRODUCTION

Welcome to D-Chat.
Your use of D-Chat is subject to these Terms of Service (these " Terms"). Thank you for reviewing these Terms – we hope you enjoy using D-Chat.
If you have any questions about, or if you wish to send us any notices in relation to, these Terms, please contact us at dchat@funlab.kr

Compliance with these Terms

Please review these Terms and our policies and instructions to understand how you can and cannot use D-Chat. You must comply with these Terms in your use of D-Chat and only use D-Chat as permitted by applicable laws and regulations, wherever you may be when you use them. If you do not agree to these Terms, you must not use D-Chat.

Contracting Entity

By using D-Chat, you are agreeing to be bound by these Terms between you and CC International Co.,Ltd., a Korean company located at 46 Dosan-daero 35-gil, Gangnam-gu Seoul, Seoul 06024 Korea, Republic of (" we", " our" and " us").

Other general terms in relation to these Terms

If you are using D-Chat on behalf of a company, partnership, association, government or other organization (your " Organization"), you warrant that you are authorised to do so and that you are authorised to bind your Organization to these Terms. In such circumstances, "you" will include your Organization.
We may translate these Terms into multiple languages, and in the event there is any difference between the Korean version and any other language version of these Terms, the Korean version will apply (to the extent permitted by applicable laws and regulations).

ADDITIONAL TERMS AND POLICIES

D-Chat policies

The following are additional policies that you must comply with in using D-Chat:
•	D-Chat Privacy Policy – which sets out how we collect, store and use your personal information, as well as our policy on the use of tracking technologies; and

TERMINATION

These Terms will apply to your use of D-Chat until your access to D-Chat is terminated by either you or us.
We may suspend or terminate your access to your account or any or all of D-Chat:
(i)	if we reasonably believe that you have breached these Terms; 
(ii)	if your use of D-Chat creates risk for us or for other users of D-Chat, gives rise to a threat of potential third party claims against us or is potentially damaging to our reputation; 
(iii)	if you fail to use D-Chat for a prolonged period; or 
(iv)	for any other reason in our sole and absolute discretion. Where reasonably practicable, we will give you notice of any suspension or termination.
